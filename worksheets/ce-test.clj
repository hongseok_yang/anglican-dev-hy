;; gorilla-repl.fileformat = 1

;; **
;;; # Test cases for the inference algorithm based on the cross-entropy method
;;; 
;;; We will describe a few tests for our ongoing implementation of the cross-entropy method. We will focus mostly on finding bugs in our implementation.
;; **

;; @@
(use 'nstools.ns)
(ns+ cetest
  (:like anglican-user.worksheet))
(use 'clojure.pprint)
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"},{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}],"value":"[nil,nil]"},{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}],"value":"[[nil,nil],nil]"}
;; <=

;; @@
(defquery test1
  (let [x (sample (normal 0.0 3.0))]
    (observe (normal x 1.0) 2.0)
    (predict :x x)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;cetest/test1</span>","value":"#'cetest/test1"}
;; <=

;; @@
(defquery test2
  (let [x (sample (bernoulli 0.5))]
    (observe (normal x 1) 1)
    (predict :x x)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;cetest/test2</span>","value":"#'cetest/test2"}
;; <=

;; @@
(defquery test3
  (let [x (sample (poisson 2))]
    (observe (normal x 1) 1)
    (predict :x x)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;cetest/test3</span>","value":"#'cetest/test3"}
;; <=

;; @@
(defquery test4
  (let [x (sample (exponential 3))]
    (observe (normal x 1) 1)
    (predict :x x)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;cetest/test4</span>","value":"#'cetest/test4"}
;; <=

;; @@
(defquery test5
  (let [x (sample (normal 0.0 1.0))
        y (sample (exponential 1.0))]
    (observe (normal x y) 2.0)
    (predict :xy [x y])))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;cetest/test5</span>","value":"#'cetest/test5"}
;; <=

;; @@
(use 'anglican.ml-dists :reload)
(use 'anglican.ce :reload)
(use 'anglican.diag :reload)
(def samples (doquery :ce test5 nil :number-of-samples 5 :max-epoch 10 :stripdown false :drop-invalid false))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"},{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}],"value":"[nil,nil]"},{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}],"value":"[[nil,nil],nil]"},{"type":"html","content":"<span class='clj-var'>#&#x27;cetest/samples</span>","value":"#'cetest/samples"}],"value":"[[[nil,nil],nil],#'cetest/samples]"}
;; <=

;; @@
(use '[anglican.diag :as diag] :reload)
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; @@
;;(diag/estimate-ml (:all-samples samples))
(diag/ess (:all-samples samples))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-double'>1.5562053561771916</span>","value":"1.5562053561771916"},{"type":"html","content":"<span class='clj-double'>1.200805017878778</span>","value":"1.200805017878778"},{"type":"html","content":"<span class='clj-double'>1.954027479015177</span>","value":"1.954027479015177"},{"type":"html","content":"<span class='clj-double'>1.8967014365159802</span>","value":"1.8967014365159802"},{"type":"html","content":"<span class='clj-double'>2.5112665375532446</span>","value":"2.5112665375532446"},{"type":"html","content":"<span class='clj-double'>4.217877436752329</span>","value":"4.217877436752329"},{"type":"html","content":"<span class='clj-double'>4.588824081855888</span>","value":"4.588824081855888"},{"type":"html","content":"<span class='clj-double'>2.5815127620432436</span>","value":"2.5815127620432436"},{"type":"html","content":"<span class='clj-double'>1.5323851687501624</span>","value":"1.5323851687501624"},{"type":"html","content":"<span class='clj-double'>1.0002337094760845</span>","value":"1.0002337094760845"}],"value":"(1.5562053561771916 1.200805017878778 1.954027479015177 1.8967014365159802 2.5112665375532446 4.217877436752329 4.588824081855888 2.5815127620432436 1.5323851687501624 1.0002337094760845)"}
;; <=

;; @@
(pprint (nth (:all-samples samples) 1))
;; @@
;; ->
;;; ({:anglican.state/mem {},
;;;   :predicts [[:xy [0.007970954119664059 0.10909997115372685]]],
;;;   :anglican.ce/sample-values
;;;   {[S27328 0 anglican.runtime.normal-distribution]
;;;    [[{:mean 0.174496625152768,
;;;       :sd 0.16980424358947183,
;;;       :dist25445
;;;       #object[org.apache.commons.math3.distribution.NormalDistribution 0x794641ef &quot;org.apache.commons.math3.distribution.NormalDistribution@794641ef&quot;]}
;;;      0.007970954119664059]],
;;;    [S27326 0 anglican.runtime.exponential-distribution]
;;;    [[{:rate 1.197320950311245,
;;;       :dist25339
;;;       #object[org.apache.commons.math3.distribution.ExponentialDistribution 0x75fac56f &quot;org.apache.commons.math3.distribution.ExponentialDistribution@75fac56f&quot;]}
;;;      0.10909997115372685]]},
;;;   :anglican.ce/choice-counts {S27328 1, S27326 1, O27324 1},
;;;   :anglican.ce/log-weight-map
;;;   {[O27324 0 anglican.runtime.normal-distribution] -165.3944148905403},
;;;   :log-importance-weight -1.4508211469844503,
;;;   :result nil,
;;;   :log-weight -165.3944148905403,
;;;   :anglican.state/store nil,
;;;   :anglican.ce/choice-last-id O27324,
;;;   :anglican.ce/proposal-map
;;;   {[S27328 0 anglican.runtime.normal-distribution]
;;;    {:mean 0.174496625152768,
;;;     :sd 0.16980424358947183,
;;;     :dist
;;;     {:mean 0.174496625152768,
;;;      :sd 0.16980424358947183,
;;;      :dist25445
;;;      #object[org.apache.commons.math3.distribution.NormalDistribution 0x794641ef &quot;org.apache.commons.math3.distribution.NormalDistribution@794641ef&quot;]}},
;;;    [S27326 0 anglican.runtime.exponential-distribution]
;;;    {:rate 1.197320950311245,
;;;     :dist
;;;     {:rate 1.197320950311245,
;;;      :dist25339
;;;      #object[org.apache.commons.math3.distribution.ExponentialDistribution 0x75fac56f &quot;org.apache.commons.math3.distribution.ExponentialDistribution@75fac56f&quot;]}}}}
;;;  {:anglican.state/mem {},
;;;   :predicts [[:xy [0.11453562299635217 0.08701982016836292]]],
;;;   :anglican.ce/sample-values
;;;   {[S27328 0 anglican.runtime.normal-distribution]
;;;    [[{:mean 0.174496625152768,
;;;       :sd 0.16980424358947183,
;;;       :dist25445
;;;       #object[org.apache.commons.math3.distribution.NormalDistribution 0x794641ef &quot;org.apache.commons.math3.distribution.NormalDistribution@794641ef&quot;]}
;;;      0.11453562299635217]],
;;;    [S27326 0 anglican.runtime.exponential-distribution]
;;;    [[{:rate 1.197320950311245,
;;;       :dist25339
;;;       #object[org.apache.commons.math3.distribution.ExponentialDistribution 0x75fac56f &quot;org.apache.commons.math3.distribution.ExponentialDistribution@75fac56f&quot;]}
;;;      0.08701982016836292]]},
;;;   :anglican.ce/choice-counts {S27328 1, S27326 1, O27324 1},
;;;   :anglican.ce/log-weight-map
;;;   {[O27324 0 anglican.runtime.normal-distribution] -233.2082410240309},
;;;   :log-importance-weight -1.8802376032798154,
;;;   :result nil,
;;;   :log-weight -233.2082410240309,
;;;   :anglican.state/store nil,
;;;   :anglican.ce/choice-last-id O27324,
;;;   :anglican.ce/proposal-map
;;;   {[S27328 0 anglican.runtime.normal-distribution]
;;;    {:mean 0.174496625152768,
;;;     :sd 0.16980424358947183,
;;;     :dist
;;;     {:mean 0.174496625152768,
;;;      :sd 0.16980424358947183,
;;;      :dist25445
;;;      #object[org.apache.commons.math3.distribution.NormalDistribution 0x794641ef &quot;org.apache.commons.math3.distribution.NormalDistribution@794641ef&quot;]}},
;;;    [S27326 0 anglican.runtime.exponential-distribution]
;;;    {:rate 1.197320950311245,
;;;     :dist
;;;     {:rate 1.197320950311245,
;;;      :dist25339
;;;      #object[org.apache.commons.math3.distribution.ExponentialDistribution 0x75fac56f &quot;org.apache.commons.math3.distribution.ExponentialDistribution@75fac56f&quot;]}}}}
;;;  {:anglican.state/mem {},
;;;   :predicts [[:xy [-0.01071146617670396 0.7189969147173974]]],
;;;   :anglican.ce/sample-values
;;;   {[S27328 0 anglican.runtime.normal-distribution]
;;;    [[{:mean 0.174496625152768,
;;;       :sd 0.16980424358947183,
;;;       :dist25445
;;;       #object[org.apache.commons.math3.distribution.NormalDistribution 0x794641ef &quot;org.apache.commons.math3.distribution.NormalDistribution@794641ef&quot;]}
;;;      -0.01071146617670396]],
;;;    [S27326 0 anglican.runtime.exponential-distribution]
;;;    [[{:rate 1.197320950311245,
;;;       :dist25339
;;;       #object[org.apache.commons.math3.distribution.ExponentialDistribution 0x75fac56f &quot;org.apache.commons.math3.distribution.ExponentialDistribution@75fac56f&quot;]}
;;;      0.7189969147173974]]},
;;;   :anglican.ce/choice-counts {S27328 1, S27326 1, O27324 1},
;;;   :anglican.ce/log-weight-map
;;;   {[O27324 0 anglican.runtime.normal-distribution]
;;;    -4.4993887782178215},
;;;   :log-importance-weight -1.216549782619968,
;;;   :result nil,
;;;   :log-weight -4.4993887782178215,
;;;   :anglican.state/store nil,
;;;   :anglican.ce/choice-last-id O27324,
;;;   :anglican.ce/proposal-map
;;;   {[S27328 0 anglican.runtime.normal-distribution]
;;;    {:mean 0.174496625152768,
;;;     :sd 0.16980424358947183,
;;;     :dist
;;;     {:mean 0.174496625152768,
;;;      :sd 0.16980424358947183,
;;;      :dist25445
;;;      #object[org.apache.commons.math3.distribution.NormalDistribution 0x794641ef &quot;org.apache.commons.math3.distribution.NormalDistribution@794641ef&quot;]}},
;;;    [S27326 0 anglican.runtime.exponential-distribution]
;;;    {:rate 1.197320950311245,
;;;     :dist
;;;     {:rate 1.197320950311245,
;;;      :dist25339
;;;      #object[org.apache.commons.math3.distribution.ExponentialDistribution 0x75fac56f &quot;org.apache.commons.math3.distribution.ExponentialDistribution@75fac56f&quot;]}}}}
;;;  {:anglican.state/mem {},
;;;   :predicts [[:xy [0.28119126748841805 1.668320192580503]]],
;;;   :anglican.ce/sample-values
;;;   {[S27328 0 anglican.runtime.normal-distribution]
;;;    [[{:mean 0.174496625152768,
;;;       :sd 0.16980424358947183,
;;;       :dist25445
;;;       #object[org.apache.commons.math3.distribution.NormalDistribution 0x794641ef &quot;org.apache.commons.math3.distribution.NormalDistribution@794641ef&quot;]}
;;;      0.28119126748841805]],
;;;    [S27326 0 anglican.runtime.exponential-distribution]
;;;    [[{:rate 1.197320950311245,
;;;       :dist25339
;;;       #object[org.apache.commons.math3.distribution.ExponentialDistribution 0x75fac56f &quot;org.apache.commons.math3.distribution.ExponentialDistribution@75fac56f&quot;]}
;;;      1.668320192580503]]},
;;;   :anglican.ce/choice-counts {S27328 1, S27326 1, O27324 1},
;;;   :anglican.ce/log-weight-map
;;;   {[O27324 0 anglican.runtime.normal-distribution]
;;;    -1.9614768077594082},
;;;   :log-importance-weight -1.4661302716670235,
;;;   :result nil,
;;;   :log-weight -1.9614768077594082,
;;;   :anglican.state/store nil,
;;;   :anglican.ce/choice-last-id O27324,
;;;   :anglican.ce/proposal-map
;;;   {[S27328 0 anglican.runtime.normal-distribution]
;;;    {:mean 0.174496625152768,
;;;     :sd 0.16980424358947183,
;;;     :dist
;;;     {:mean 0.174496625152768,
;;;      :sd 0.16980424358947183,
;;;      :dist25445
;;;      #object[org.apache.commons.math3.distribution.NormalDistribution 0x794641ef &quot;org.apache.commons.math3.distribution.NormalDistribution@794641ef&quot;]}},
;;;    [S27326 0 anglican.runtime.exponential-distribution]
;;;    {:rate 1.197320950311245,
;;;     :dist
;;;     {:rate 1.197320950311245,
;;;      :dist25339
;;;      #object[org.apache.commons.math3.distribution.ExponentialDistribution 0x75fac56f &quot;org.apache.commons.math3.distribution.ExponentialDistribution@75fac56f&quot;]}}}}
;;;  {:anglican.state/mem {},
;;;   :predicts [[:xy [0.23648249695011125 0.025930481798647396]]],
;;;   :anglican.ce/sample-values
;;;   {[S27328 0 anglican.runtime.normal-distribution]
;;;    [[{:mean 0.174496625152768,
;;;       :sd 0.16980424358947183,
;;;       :dist25445
;;;       #object[org.apache.commons.math3.distribution.NormalDistribution 0x794641ef &quot;org.apache.commons.math3.distribution.NormalDistribution@794641ef&quot;]}
;;;      0.23648249695011125]],
;;;    [S27326 0 anglican.runtime.exponential-distribution]
;;;    [[{:rate 1.197320950311245,
;;;       :dist25339
;;;       #object[org.apache.commons.math3.distribution.ExponentialDistribution 0x75fac56f &quot;org.apache.commons.math3.distribution.ExponentialDistribution@75fac56f&quot;]}
;;;      0.025930481798647396]]},
;;;   :anglican.ce/choice-counts {S27328 1, S27326 1, O27324 1},
;;;   :anglican.ce/log-weight-map
;;;   {[O27324 0 anglican.runtime.normal-distribution]
;;;    -2309.9084541958896},
;;;   :log-importance-weight -1.9094126499211057,
;;;   :result nil,
;;;   :log-weight -2309.9084541958896,
;;;   :anglican.state/store nil,
;;;   :anglican.ce/choice-last-id O27324,
;;;   :anglican.ce/proposal-map
;;;   {[S27328 0 anglican.runtime.normal-distribution]
;;;    {:mean 0.174496625152768,
;;;     :sd 0.16980424358947183,
;;;     :dist
;;;     {:mean 0.174496625152768,
;;;      :sd 0.16980424358947183,
;;;      :dist25445
;;;      #object[org.apache.commons.math3.distribution.NormalDistribution 0x794641ef &quot;org.apache.commons.math3.distribution.NormalDistribution@794641ef&quot;]}},
;;;    [S27326 0 anglican.runtime.exponential-distribution]
;;;    {:rate 1.197320950311245,
;;;     :dist
;;;     {:rate 1.197320950311245,
;;;      :dist25339
;;;      #object[org.apache.commons.math3.distribution.ExponentialDistribution 0x75fac56f &quot;org.apache.commons.math3.distribution.ExponentialDistribution@75fac56f&quot;]}}}})
;;; 
;; <-
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; @@

;; @@

;; @@

;; @@

import numpy as np
import matplotlib
import tensorflow as tf
from tensorflow.python import debug as tf_debug
import os
import time

import smc
import smc_opt
import util

import oracle_lookahead
import oracle_lookahead_bootstrap
import oracle_lookahead_bootstrap_new
import oracle_lookahead_bootstrap_new_denoise
import oracle_lookahead_bootstrap_new_denoise_no_t
import oracle_lookahead_bootstrap_new_halfnoise_no_t
import oracle_lookahead_no_h
import oracle_lookahead_no_h_no_t
import oracle_lookahead_no_lstm
import oracle_lstm_type3
import oracle_no_lstm_bootstrap
import oracle_prior
import oracle_nasmc
import oracle_lstm_type1


import exper_lgssm
import exper_lgssm_high
import exper_nlssm
import exper_nlssm_high
import exper_nlssm_high1

""" Script that performs experiments. """ 

exper_lgssm_lstm = exper_lgssm.ExperLSTM()
exper_lgssm_prior = exper_lgssm.ExperPrior()
exper_lgssm_lstm_high = exper_lgssm_high.ExperLSTM()
exper_lgssm_prior_high = exper_lgssm_high.ExperPrior()
#exper_lgssm_lstm_high = exper_nlssm_high1.ExperLSTM()
#exper_lgssm_prior_high = exper_nlssm_high1.ExperPrior()

exper_nlssm_lstm = exper_nlssm.ExperLSTM()
exper_nlssm_prior = exper_nlssm.ExperPrior()
exper_nlssm_lstm_high = exper_nlssm_high.ExperLSTM()
exper_nlssm_prior_high = exper_nlssm_high.ExperPrior()

write = False
is_linear = False
is_high_dimensional = False
have_fixed_data_stored = True
if is_linear:
    if is_high_dimensional:
        exper_prior = exper_lgssm_prior_high
        exper_lstm = exper_lgssm_lstm_high
    else:
        exper_prior = exper_lgssm_prior
        exper_lstm = exper_lgssm_lstm
else:
    if is_high_dimensional:
        exper_prior = exper_nlssm_prior_high 
        exper_lstm = exper_nlssm_lstm_high
    else:
        exper_prior = exper_nlssm_prior
        exper_lstm = exper_nlssm_lstm
exper = exper_lstm

file_name = ('nlssm_misspec_nasmc_d%d_m%d_T%d_varv%.1f_varw%.1f' %(exper.state_dim,exper.batch_size,exper.n_step,exper.var_v,exper.var_w[0]))
#file_name = ('lgssm_%dd_l_b_n_d_no_t' %exper.state_dim)
dir_name = '../../resource/pictures/' + file_name
#dir_name = '../../resource/pictures'
if not is_high_dimensional:
    os.system("mkdir -p " + dir_name) # make directory for storing plots


config = tf.ConfigProto(); config.gpu_options.visible_device_list = str(exper.gpu) # just use /gpu:0
#config = tf.ConfigProto(device_count = {'GPU': 0}) # only use cpu's
sess = tf.Session(config = config) 
# sess = tf_debug.LocalCLIDebugWrapperSession(sess) 
# sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)

oracle_prior = oracle_prior.Oracle(sess, exper_prior)
oracle_lkhd = oracle_lookahead.Oracle(sess, exper_lstm)
oracle_type3 = oracle_lstm_type3.Oracle(sess, exper_lstm)
oracle_lkhd_bootstrap = oracle_lookahead_bootstrap.Oracle(sess, exper_lstm)
oracle_lkhd_bootstrap_new = oracle_lookahead_bootstrap_new.Oracle(sess, exper_lstm)
oracle_lkhd_bootstrap_new_denoise = oracle_lookahead_bootstrap_new_denoise.Oracle(sess, exper_lstm)
oracle_lkhd_bootstrap_new_denoise_no_t = oracle_lookahead_bootstrap_new_denoise_no_t.Oracle(sess, exper_lstm)
oracle_lkhd_bootstrap_new_halfnoise_no_t = oracle_lookahead_bootstrap_new_halfnoise_no_t.Oracle(sess, exper_lstm)
oracle_lkhd_no_h = oracle_lookahead_no_h.Oracle(sess, exper_lstm)
oracle_lkhd_no_h_no_t = oracle_lookahead_no_h_no_t.Oracle(sess, exper_lstm)
oracle_nasmc = oracle_nasmc.Oracle(sess, exper_lstm)
oracle_type1 = oracle_lstm_type1.Oracle(sess, exper_lstm)
oracle_lkhd_no_h_no_t = oracle_lookahead_no_h_no_t.Oracle(sess, exper_lstm)
oracle_no_lstm1 = oracle_no_lstm_bootstrap.Oracle(sess, exper_lstm)
oracle_no_lstm2 = oracle_lookahead_no_lstm.Oracle(sess, exper_lstm)

""" Check this Line """
oracle = oracle_nasmc

algo = smc_opt.SMC(exper, oracle)


if write:
    util.open_write_output(file_name) # write output to file ../../resource/output/file_name.txt

print('##### start preprocessing steps #####')
if have_fixed_data_stored:
    var_v = exper.var_v
    var_w = exper.var_w
    T = int(exper.n_step)
    m = exper.batch_size
    if is_linear:
        if is_high_dimensional:
            filename = ('../../resource/data/lgssm_misspec_d%d_m%d_T%d_varv%.1f_varw%.1f_phiv%.1f_phiw%.1f_test_data.npz' %(exper.state_dim,m,T,var_v,var_w,exper.phi_v,exper.phi_w[0]))
        else:
            filename = ('../../resource/data/lgssm_misspec_d%d_m%d_T%d_varv%.1f_varw%.1f_phiv%.1f_phiw%.1f_test_data.npz' %(exper.state_dim,m,T,var_v,var_w,exper.phi_v[0],exper.phi_w))
    else:
        filename = ('../../resource/data/nlssm_misspec_d%d_m%d_T%d_varv%.1f_varw%.1f_test_data.npz' %(exper.state_dim,m,T,var_v,var_w[0]))
    if os.path.isfile(filename):
        print('Loading a test file from '+filename)
        test_data = np.load(filename)
        fixed_xs = test_data['x']
        fixed_ys = test_data['y']
    else:
        print('Generating and saving a test file: '+filename)
        fixed_xs, fixed_ys = exper.generate_minibatch()

        """ modify some observations for model misspecification """
        for t in range(T):
            if t % 10 == 9:
                fixed_ys[:,t,:] = 10
        
        np.savez(filename,x=fixed_xs,y=fixed_ys)
else:
    print('Generating a test file')
    fixed_xs, fixed_ys = exper.generate_minibatch()
    for t in range(T):
        if t % 10 == 9:
            fixed_ys[:,t,:] = 10
        
    np.savez(filename,x=fixed_xs,y=fixed_ys)

fixed_xs_ys = (fixed_xs, fixed_ys)

oracle.build_graph()
init = tf.global_variables_initializer()
sess.run(init)
matplotlib.rcParams.update({'figure.max_open_warning': 0}) # suppress warning of > 20 figures

if is_linear and not is_high_dimensional:
    _,_,_,log_marginal_lkhd = exper.future_lkhd(fixed_ys[:,:,0])
    mean_log_marginal = np.mean(log_marginal_lkhd)
    print("True mean log marginal likelihood across batch: %f" %mean_log_marginal)

print("##### batch_size=%d, n_step=%d, n_particle=%d, learning_rate=%f #####" % (exper.batch_size, exper.n_step, exper.n_particle, exper.learning_rate))
for epoch in xrange(exper.n_epoch):
    print "### Epoch: %d ###" % epoch
    _, array_ys = exper.generate_minibatch()
    T = int(exper.n_step)
    for t in range(T):
            if t % 10 == 9:
                array_ys[:,t,:] = 10
    for i in xrange(exper.n_iter):         
        print("### Iter %d of Epoch %d ###" % (i, epoch))
        oracle.feed_data(array_ys)  
        smc_result = algo.run(array_ys)
        (rollout, weight, _, _, ancestry) = smc_result
        oracle.train(i, rollout, weight, array_ys, ancestry)
        exper.plot(fixed_xs_ys, oracle, 5, epoch, i, dir_name)
        rollout, weight, marginal, marginal_mean_acc, ancestry = algo.run(fixed_ys)
        util.print_result(oracle, algo, rollout, weight, marginal, marginal_mean_acc, ancestry, fixed_xs)
sess.close()    

if write:
    print("##### Output_file_name = " + file_name + ".txt #####")
    

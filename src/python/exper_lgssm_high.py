import numpy as np
import tensorflow as tf
import scipy.stats

from exper import Exper
import util

def compute_normal_log_pdf(x, mean, var): 
    """ Input type:
        (1) x : array[a1,...,an,b]
        (2) mean : array[a1,...,an,b]
        (3) var : scalar
        Output type: array[a1,...,an]. """
    dim = x.shape[-1]
    distance = np.sum((x - mean) ** 2, axis=-1)
    return - distance / (2. * var) - 0.5 * dim * np.log(2. * np.pi * var)

class ExperLGSSM(Exper):
    """ Class for storing info about performing experiments with a simple 
    high-dimensional nonlinear ssm model. """
    def __init__(self):
        """ Parameters for our algorithm """
        self.batch_size = 10 # size of a minibatch
        self.n_epoch = 1000 # number of epochs
        self.n_particle = 100 # number of particles in smc 
        self.state_dim = 50 # dimension of state 
        self.n_iter = 1 # number of gradient updates 
        self.adapt_threshold = 0.5 # fraction of ESS below which we resample
        self.h_size = 1. # amplifier for h
        self.q_learning_rate = 0.0001
        self.h_learning_rate = 0.0001
        self.optimiser = 'Adam' # 'RMSProp' or 'Adam'
        self.init_q_std = 1. # initialisation of std of DNNQ output
        self.init_h = 10. # initialisation of std of h(y_(t+1:T-1)|x_t)

        self.lstm_size = 100 # num_units of h,c and output of lstm 
        self.num_layers = 3
        self.dnn_sizes = [100]*self.num_layers # list of layer sizes for dnn 
        self.nonlinearity = [tf.tanh]*self.num_layers # list of nonlinearities for each layer of dnn
        self.max_grad_norm = 5. 

        """ Parameters for a state-space model used in our experiment """ 
        self.n_step = 10  # number of steps in the model 
        self.var_v = 0.1 # variance for Gaussian transition 
        self.var_w = 1.*np.ones(self.n_step) # variance for Gaussian likelihood
        self.phi_v = np.sqrt(0.9) # linear coeff for mean of transition
        self.phi_w = np.ones([self.state_dim]) # linear coeff for mean of likelihood

        """ Parameters for objective """
        self.kl_weights = np.ones(self.n_step)

        """ Miscellaneous parameters """
        self.gpu = 2 # gpu number to use
        self.linear = False # used to determine whether to use gaussian parameterisation for h. Only relevant for l_b_n_d_no_t.
        self.use_dnnq = True # used for lstm1 to determine whether to use DNNQ or not - MUST be True for SSM with high dimensional x.
        self.use_layernorm = False # use layernorm for lstm
        self.init_bootstrap = False # add bootstrap mean and (log var - 2*log init_q_std) to DNNQ output mean and log var
        self.init_dnnq_output_scale = (0.1)**2 # var of output of DNNQ / var of input to DNNQ, used for uniform initialisation of wieghts. If None, Xavier init is used. Only relevent if self.init_bootstrap=True
        self.init_dnnh_output_scale = None # var of output of DNNH / var of input to DNNH, used for uniform initialisation of wieghts. If None, Xavier init is used.

        return

    def compute_next_mean(self, t, x): 
        """ Compute mean of p(x_t|x_{t-1}=x).
            Input type:
            (1) t : scalar
            (2) x : array[a1,...,an,state_dim]
            Output type: array[a1,...,an,state_dim]. """
        return self.phi_v * x

    def tf_compute_next_mean(self, t, x): 
        """ Input type:
            (1) t : scalar
            (2) x : tensorflow_array[a1,...,an,state_dim]
            Output type: tensorflow_array[a1,...,an,state_dim]. """
        return self.phi_v * x

    def log_lkhd(self, t, y, x): 
        """ Input type: 
            (1) t : scalar
            (2) y : scalar
            (3) x : array[state_dim]
            Output type: scalar. """
        mean = np.sum(self.phi_w * x)
        return compute_normal_log_pdf(np.array([y]), mean, self.var_w[t])

    def log_lkhd_particles(self, t, y, x): 
        """ Compute log likelihood of particles.  
            Input type: 
            (1) t : scalar
            (2) y : array of shape [batch_size]
            (3) x : array of shape [batch_size,n_particle,state_dim]
            Output type:
            (1) array of shape [batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        phi_w_expanded = np.expand_dims(np.expand_dims(self.phi_w, axis=0),axis=0) # [1,1,state_dim]
        phi_w_tiled = np.tile(phi_w_expanded, (batch_size, n_particle, 1)) # [batch_size,n_particle,state_dim]
        mean = np.sum(phi_w_tiled * x, axis=2) # [batch_size,n_particle]
        mean_expanded = np.expand_dims(mean, axis=2) # [batch_size,n_particle,1]
        y_tiled = np.tile(np.expand_dims(y, axis=1), (1, n_particle)) # [batch_size,n_particle]
        y_expanded = np.expand_dims(y_tiled, axis=2) # [batch_size,n_particle,1] 
        return compute_normal_log_pdf(y_expanded, mean_expanded, self.var_w[t])

    def _log_init_prob(self, x): 
        """ Input type: 
            (1) x : array[state_dim] 
            Output type: scalar. """
        state_dim = x.shape[0]
        mean = np.zeros(shape=[state_dim])
        return compute_normal_log_pdf(x, mean, self.var_v)

    def _log_init_prob_particles(self, x): 
        """ Input type: 
            (1) x : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.get_dim()
        mean = np.zeros(shape=[batch_size,n_particle,state_dim])
        return compute_normal_log_pdf(x, mean, self.var_v)

    def _log_trans_prob(self, t, x_prev, x_curr): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[state_dim]
            (3) x_curr : array[state_dim]
            Output type: scalar. """
        mean = self.compute_next_mean(t, x_prev)
        return compute_normal_log_pdf(x_curr, mean, self.var_v)

    def _log_trans_prob_particles(self, t, x_prev, x_curr): 
        """ Input type: 
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            (3) x_curr : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        mean = self.compute_next_mean(t, x_prev)
        return compute_normal_log_pdf(x_curr, mean, self.var_v)

    def _sample_init(self): 
        """ Output type:
            (1) x : array[batch_size,state_dim]
            (2) mean : array[batch_size,state_dim]
            (3) log_var : array[batch_size]. """
        batch_size, _, _, state_dim = self.get_dim()
        mean = np.zeros(shape=[batch_size,state_dim])
        log_var = np.zeros(shape=[batch_size]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var        

    def _sample_init_particles(self): 
        """ Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) mean : array[batch_size,n_particle,state_dim]
            (3) log_var : array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.get_dim()
        mean = np.zeros(shape=[batch_size,n_particle,state_dim])
        log_var = np.zeros(shape=[batch_size,n_particle]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var        

    def _sample_next(self, t, x_prev): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,state_dim]
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) mean : array[batch_size,state_dim]
            (3) log_var : array[batch_size]. """
        batch_size, _, _, _ = self.get_dim()
        mean = self.compute_next_mean(t, x_prev)
        log_var = np.zeros(shape=[batch_size]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var

    def _sample_next_particles(self, t, x_prev): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) mean : array[batch_size,n_particle,state_dim]
            (3) log_var : array[batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = self.compute_next_mean(t, x_prev)
        log_var = np.zeros(shape=[batch_size,n_particle]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var

    def _sample_obs(self, t, x):
        """ Sample y from p(y_t|x_t).
            Input type:
            (1) t : scalar
            (2) x : array[a1,...,an,state_dim]
            Output type: array[a1,...,an,1]. """
        num_rep = np.prod(x.shape[0:-1]) # a1*...*an
        phi_tiled = np.tile(self.phi_w,num_rep) # [state_dim*a1*...*an]
        phi_tiled_reshaped = phi_tiled.reshape(x.shape) # [a1,...,an,state_dim] (can also handle non-isotropic phi_w)
        mean = np.sum(x * phi_tiled_reshaped, axis=-1) # [a1,....,an]
        obs = np.random.normal(mean, np.sqrt(self.var_w[t])) # [a1,....,an]
        return np.expand_dims(obs, axis=-1) 

    def get_dim(self):
        return (self.batch_size, self.n_step, self.n_particle, self.state_dim)

    def plot(self, fixed_xs_ys, oracle, epoch_cur, iter_cur, dir_name):
        return

class ExperPrior(ExperLGSSM):
    def __init__(self):
        super(ExperPrior, self).__init__()
        return




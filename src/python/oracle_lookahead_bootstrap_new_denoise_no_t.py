import numpy as np
from scipy.misc import logsumexp
import tensorflow as tf

from util import tf_compute_neg_log_normal_pdf
from util import tf_stack_along_col

""" Utility functions. """

def data_type(): 
    return tf.float32

class Oracle(object):

    def __init__(self, sess, exper):
        self._sess = sess # tensorflow session
        self._lstm_size = exper.lstm_size
        self._learning_rate = exper.learning_rate
        self._nonlinearity = exper.nonlinearity
        self._max_grad_norm = exper.max_grad_norm
        self._dnn_sizes = exper.dnn_sizes
        self._batch_size = exper.batch_size
        self._n_iter = exper.n_iter
        self._n_particle = exper.n_particle
        self._n_step = exper.n_step
        self._h_size = exper.h_size
        self._state_dim = exper.state_dim
        self._compute_next_mean = exper.compute_next_mean
        self._init_mean = exper.init_mean
        self._linear = exper.linear
        self._init_q_std = exper.init_q_std
        self._init_h = exper.init_h
        self._kl_weights=exper.kl_weights
        return

    def __get_dim(self):
        return (self._batch_size, self._n_step, self._n_particle, self._state_dim)

    def __build_lstm(self):
        shape = [None, self._n_step, 1] # shape of tensor that stores observations reversed in time
        input_rev = tf.placeholder(shape=shape, dtype=data_type()) # minibatch of observations reversed in time
        lstm = tf.contrib.rnn.LSTMCell(num_units=self._lstm_size)
        output_rev, _ = tf.nn.dynamic_rnn(lstm, input_rev, dtype=data_type()) # [batch_size,n_step,lstm_size]

        self._input_rev = input_rev 
        self._output_rev = output_rev
        return

    def __build_dnn(self, input_list, num_outputs, base_name, reuse, bias_init_final):
        dnn_hidden = tf.concat(input_list, axis=1) 
        for d in xrange(len(self._dnn_sizes)): 
            scope_name = base_name + 'Layer' + str(d+1) 
            dnn_hidden = tf.contrib.layers.fully_connected(
                            inputs=dnn_hidden, 
                            num_outputs=self._dnn_sizes[d], 
                            activation_fn=self._nonlinearity,
                            reuse=reuse, 
                            scope=scope_name) 
        scope_name = base_name + 'FinalLayer'
        bias_init_value = np.array([0. if i < num_outputs-1 else bias_init_final 
                                    for i in range(num_outputs)]) # bias_init_final is the initial value of the bias of the last output unit.
        bias_init = tf.constant_initializer(bias_init_value)
        dnn_output = tf.contrib.layers.fully_connected(
                        inputs=dnn_hidden, 
                        num_outputs=num_outputs, 
                        activation_fn=None, 
                        biases_initializer = bias_init,
                        reuse=reuse, 
                        scope=scope_name)
        return dnn_output

    def __build_dnn_q_with_inputs(self, input_x_y_fx, input_lstm, reuse): 
        """        
        Inputs:
        (1) input_x_y_fx: x_(t-1),y_t,f(x_(t-1)) (latter is mean of state transition from x_(t-1) to x_t). array[batch_size,2*state_dim+1]             
        (2) input_lstm: v_(t+1) (output of lstm at time t+1). array[batch_size,lstm_size]
        (3) reuse: whether to reuse learned variables at test time. Boolean.
        Outputs:
        (1) sample: sample from gaussian q(x_t|x_(t-1)). array[batch_size,state_dim]
        (2) log_pdf: log pdf of sample. array[batch_size]
        (3) mean: mean of q(x_t|x_(t-1)). array[batch_size,state_dim] 
        (4) log_var: log_var of q(x_t|x_(t-1)). array[batch_size]
        """
        state_dim = self._state_dim
        num_outputs = state_dim + 1
        scope_name = 'DNNQ'
        init_log_q_var = 2*np.log(self._init_q_std)
        output = self.__build_dnn([input_x_y_fx, input_lstm], num_outputs, scope_name, reuse, init_log_q_var)
        mean = output[:,0:state_dim]
        log_var = output[:,state_dim]
        std = tf.sqrt(tf.exp(log_var))
        std = tf_stack_along_col(std, state_dim)
        dist = tf.contrib.distributions.MultivariateNormalDiag(mean, std)
        sample = dist.sample()
        log_pdf = dist.log_prob(sample)
        return [sample, log_pdf, mean, log_var]

    def __build_dnn_q(self):
        lstm_size = self._lstm_size 
        state_dim = self._state_dim
        input1 = tf.placeholder(shape=[None,2*state_dim+1], dtype=data_type())
        input2 = tf.placeholder(shape=[None,lstm_size], dtype=data_type())
        self._dnn_q_input1 = input1
        self._dnn_q_input2 = input2
        self._dnn_q_output = self.__build_dnn_q_with_inputs(input1, input2, None)
        return 

    def __build_dnn_h_with_inputs_gaussian(self, input_x, input_lstm, reuse):
        state_dim = self._state_dim
        h_size = self._h_size
        num_outputs = state_dim + 1
        scope_name = 'DNNH'
        init_log_h_var = 2*np.log(self._init_h)
        output = self.__build_dnn([input_x, input_lstm], num_outputs, scope_name, reuse, init_log_h_var)
        x = input_x
        mu = output[:, 0:state_dim]
        log_var = output[:, state_dim]
        h = - self._h_size * tf_compute_neg_log_normal_pdf(state_dim, x, mu, log_var)
        return h

    def __build_dnn_h_with_inputs_raw(self, input_x, input_lstm, reuse):
        num_outputs = 1
        scope_name = 'DNNH'
        output = self._h_size * self.__build_dnn([input_x, input_lstm], num_outputs, scope_name, reuse, self._init_h)
        return output[:,0]

    def __build_dnn_h_with_inputs(self, input_x, input_lstm, reuse):
        """
        Inputs:
        (1) input_x: x_t. array[batch_size,state_dim]
        (2) input_lstm: v_(t+1) (output of lstm at time t+1). array[batch_size,lstm_size]
        (3) reuse: whether to reuse learned variables at test time. Boolean.
        Outputs:
        (1) h: value of h_t(x_t). array[batch_size]
        """
        if self._linear:        
            return self.__build_dnn_h_with_inputs_gaussian(input_x, input_lstm, reuse)
        else:
            return self.__build_dnn_h_with_inputs_raw(input_x, input_lstm,reuse) 

    def __build_dnn_h(self):
        lstm_size = self._lstm_size 
        state_dim = self._state_dim
        input1 = tf.placeholder(shape=[None,state_dim], dtype=data_type())
        input2 = tf.placeholder(shape=[None,lstm_size], dtype=data_type())
        self._dnn_h_input1 = input1
        self._dnn_h_input2 = input2
        self._dnn_h_output = self.__build_dnn_h_with_inputs(input1, input2, None)
        return

    def __build_loss(self):
        batch_size, n_step, n_particle, state_dim = self.__get_dim()
        lstm_size = self._lstm_size
        n_summands_long = batch_size * n_step * n_particle
        n_summands_short = batch_size * (n_step - 1) * n_particle
        n_summands_init = batch_size 

        """ The input_rev_smc0 placeholder stores information about a particular 
            smc run over a minibatch. For t=0,...,T-1 and i=0,...,n_particle-1,
            input_rev_smc0[b,T-1-t,i,:] is an array containing the following six elements:
            (1) x^i_t -- state of particle i at step t. [:,:,:,0:state_dim]
            (2) t -- step t. [:,:,:,state_dim]
            (3) x^(a_(t-1)^i)_(t-1) -- state of ancestor of x^i_t. [:,:,:,(state_dim+1):(2*state_dim+1)]
            (4) y^b_t -- observation at step t in the b-th batch. [:,:,:,2*state_dim+1]
            (5) log (w^i_t) -- log weight of the i-th particle at step t. [:,:,:,2*state_dim+2]
            (6) log (sum_i (- w^i_t * log w^i_t)) -- log of the entropy-like sum. [:,:,:,2*state_dim+3]
            (7) f(x^(a_(t-1)^i)_(t-1)) -- mean of transition p(.|x^(a_(t-1)^i)_(t-1)). [:,:,:,(2*state_dim+4):(3*state_dim+4)] """

        n_elem = 3 * state_dim + 4
        input_rev_smc0 = tf.placeholder(shape=[batch_size,n_step,n_particle,n_elem], dtype=data_type())
        input_rev_smc1 = tf.reshape(input_rev_smc0, [n_summands_long,n_elem])

        input_rev_smc2 = input_rev_smc0[:,1:n_step,:,:] # values for t=T-2,...,0 
        input_rev_smc3 = tf.reshape(input_rev_smc2, [n_summands_short,n_elem])

        output_rev_lstm0 = self._output_rev
        output_rev_lstm1 = tf.expand_dims(output_rev_lstm0, axis=2)
        output_rev_lstm2 = tf.tile(output_rev_lstm1, [1,1,n_particle,1])
        output_rev_lstm3 = tf.reshape(output_rev_lstm2, [n_summands_long,lstm_size])

        output_rev_lstm4 = output_rev_lstm2[:,0:(n_step-1),:,:] # values for t=T-1,...,1
        output_rev_lstm5 = tf.reshape(output_rev_lstm4, [n_summands_short,lstm_size])

        zero_padding = tf.zeros(shape=[batch_size,1,n_particle,lstm_size], dtype=data_type())
        output_rev_lstm6 = tf.concat([zero_padding, output_rev_lstm4], axis=1) # values for t=T,...,1
        output_rev_lstm7 = tf.reshape(output_rev_lstm6, [n_summands_long,lstm_size])

        output_rev_lstm8 = tf.reshape(output_rev_lstm0[:,n_step-1,:], [n_summands_init,lstm_size]) # value for t=0

        x_long = input_rev_smc1[:,0:state_dim] # (1) for t=T-1,...,0
        log_w_long = input_rev_smc1[:,(2*state_dim+2)] # (5) for t=T-1,...,0

        log_w_short = input_rev_smc3[:,(2*state_dim+2)] # (5) for t=T-2,...,0
        log_neg_w_sum_short = input_rev_smc3[:,(2*state_dim+3)] # (6) for t=T-2,...,0

        input_rev_dnn_q = tf.concat([input_rev_smc1[:,(state_dim+1):(2*state_dim+2)],input_rev_smc1[:,(2*state_dim+4):(3*state_dim+4)]], axis = 1)
        _, _, q_mu, q_log_var = self.__build_dnn_q_with_inputs(input_rev_dnn_q, output_rev_lstm7, True)  # mean, log_var of q(x_t|x_(t-1)) for t=T-1,...,0

        input_rev_dnn_h_long = input_rev_smc1[:,(state_dim+1):(2*state_dim+1)]
        h_term_long = self.__build_dnn_h_with_inputs(input_rev_dnn_h_long, output_rev_lstm3, True) # h_(t-1)(x^t_t-1) for t=T-1,...,0.

        input_rev_dnn_h_short = input_rev_smc3[:,0:state_dim]
        h_term_short = self.__build_dnn_h_with_inputs(input_rev_dnn_h_short, output_rev_lstm5, True) # h_t(x_t) for t=T-2,...,0

        input_rev_dnn_h_init = tf.zeros(shape=[n_summands_init, state_dim], dtype=data_type())
        h_term_init = self.__build_dnn_h_with_inputs(input_rev_dnn_h_init, output_rev_lstm8, True) # h_{-1}(0)

        log_q_term = tf.exp(log_w_long) * tf_compute_neg_log_normal_pdf(state_dim, x_long, q_mu, q_log_var) # w_t * log q_t(x_t|x_(t-1)) for t= T-1,...,0

        gamma_t_term_long = - tf.exp(log_w_long) * h_term_long # - w_t * h_(t-1) for t=T-1,...,0
        gamma_t_term_short = tf.exp(log_w_short) * h_term_short # w_t * h_t for t=T-2,...,0
        gamma_t_term_init = h_term_init # h_{-1}. cancels the h_{-1} term in gamma_t_term_long
        #gamma_t_next_term = tf.exp(log_w_short) * (log_w_short + tf.exp(log_neg_w_sum_short)) * h_term_short 
        
        # Reweigh the terms by self.kl_weights
        # want all weights for 'long' terms
        kl_weights_long = self._kl_weights
        kl_weights_long = tf.convert_to_tensor(np.flip(kl_weights_long, axis=0), dtype=tf.float32)
        kl_weights_long = tf.expand_dims(kl_weights_long, axis=0) # shape [1,T]
        kl_weights_long = tf.expand_dims(kl_weights_long, axis=-1) # shape [1,T,1]
        kl_weights_long = tf.tile(kl_weights_long, [batch_size,1,n_particle]) # shape [batch_size,T,n_step]
        kl_weights_long = tf.reshape(kl_weights_long,[-1]) # shape [n_summands_long]

        # want weights for t=1,...,T-1 for 'short' terms
        kl_weights_short = self._kl_weights[1:n_step]
        kl_weights_short = tf.convert_to_tensor(np.flip(kl_weights_short, axis=0), dtype=tf.float32)
        kl_weights_short = tf.expand_dims(kl_weights_short, axis=0) # shape [1,T-1]
        kl_weights_short = tf.expand_dims(kl_weights_short, axis=-1) # shape [1,T-1,1]
        kl_weights_short = tf.tile(kl_weights_short, [batch_size,1,n_particle]) # shape [batch_size,T-1,n_step]
        kl_weights_short = tf.reshape(kl_weights_short,[-1]) # shape [n_summands_short]

        # want weight for t=0 for 'init' term
        kl_weights_init = tf.convert_to_tensor(self._kl_weights[0], dtype=tf.float32)

        self._loss_input_rev = input_rev_smc0
        self._loss_output_rev = (tf.reduce_sum(kl_weights_long * log_q_term) 
                + tf.reduce_sum(kl_weights_long * gamma_t_term_long) 
                + tf.reduce_sum(kl_weights_short * gamma_t_term_short)
                + tf.reduce_sum(kl_weights_init * gamma_t_term_init)
                )#+ tf.reduce_sum(gamma_t_next_term))
        return

    def __build_optimizer(self):
        minimize = tf.contrib.layers.optimize_loss(
                    loss = self._loss_output_rev,
                    global_step = tf.contrib.framework.get_global_step(),
                    learning_rate = self._learning_rate,
                    optimizer = 'Adam',
                    clip_gradients = self._max_grad_norm)
        self._minimize = minimize
        return

    def build_graph(self):
        self.__build_lstm()
        self.__build_dnn_q()
        self.__build_dnn_h()
        self.__build_loss()
        self.__build_optimizer()
        return

    def feed_data(self, input_data):
        """ Run the lstm on a given input data, and store the output of the lstm 
            in the internal state of this object. 
            Input type:
            (1) input_data : array[batch_size,n_step,1]
            Output type: unit
            Side effect: The result of the lstm is stored in the _lstm_output_val field. """
        input_rev = self._input_rev 
        output_rev = self._output_rev 
        input_data_rev = np.flip(input_data, axis=1)
        output_data_rev = self._sess.run(output_rev, feed_dict={input_rev: input_data_rev})
        output_data = np.flip(output_data_rev, axis=1)
        self._input_data = input_data
        self._lstm_output_val = output_data
        return 

    def __combine_x_and_yt_and_fx(self, x, t, yt):
        """ Input type:
            (1) x : array[batch_size,state_dim] 
            (2) t : scalar
            (3) yt : array[batch_size,1]
            Output type: array[batch_size,2*state_dim+1]. """
        batch_size, _, _, _ = self.__get_dim()
        if t > 0:
            fx = self._compute_next_mean(t, x)
        else:
            fx = self._init_mean(x)
        return np.concatenate([x, yt[:,:], fx], axis=1)

    def __combine_x_and_yt_and_fx_particles(self, x, t, yt):
        """ Input type:
            (1) x : array[batch_size,n_particle,state_dim] 
            (2) t : scalar
            (3) yt : array[batch_size,1]
            Output type: array[batch_size,n_particle,2*state_dim+1]. """
        batch_size, _, n_particle, _ = self.__get_dim()
        array_yt = np.tile(np.expand_dims(yt, axis=1), (1,n_particle,1))
        if t > 0:
            fx = self._compute_next_mean(t,x)
        else:
            fx = self._init_mean(x)
        return np.concatenate([x, array_yt, fx], axis=2)

    def __sample(self, t, x_prev_and_yt_and_fx):
        """ Input type:
            (1) t : scalar
            (2) x_prev_and_yt_and_fx : array[batch_size,2*state_dim+1] 
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size]. """
        batch_size, _, _, state_dim = self.__get_dim()

        # Run the dnn for q to get parameters of the proposal distribution.
        node_input1 = self._dnn_q_input1
        node_input2 = self._dnn_q_input2
        node_output = self._dnn_q_output
        lstm_output = self._lstm_output_val[:,t,:]
        feed_dict = {node_input1: x_prev_and_yt_and_fx, node_input2: lstm_output}
        return self._sess.run(node_output, feed_dict=feed_dict)

    def __sample_particles(self, t, x_prev_and_yt_and_fx):
        """ Input type:
            (1) t : scalar
            (2) x_prev_and_yt_and_fx : array[batch_size,n_particle,2*state_dim+1] 
            Output type: 
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.__get_dim()
        lstm_size = self._lstm_size 
        num_entries = batch_size * n_particle

        lstm_output = self._lstm_output_val[:,t,:]
        lstm_output_expanded = np.expand_dims(lstm_output, axis=1)
        lstm_output_tiled = np.tile(lstm_output_expanded, (1,n_particle,1))

        lstm_output_reshaped = np.reshape(lstm_output_tiled, (num_entries,lstm_size))
        x_prev_and_yt_and_fx_reshaped = np.reshape(x_prev_and_yt_and_fx, (num_entries,2*state_dim+1))

        node_input1 = self._dnn_q_input1
        node_input2 = self._dnn_q_input2
        node_output = self._dnn_q_output
        feed_dict = {node_input1: x_prev_and_yt_and_fx_reshaped, node_input2: lstm_output_reshaped}
        x, x_log_pdf, _, _ = self._sess.run(node_output, feed_dict=feed_dict)

        x_unreshaped = np.reshape(x, (batch_size,n_particle,state_dim))
        x_log_pdf_unreshaped = np.reshape(x_log_pdf, (batch_size,n_particle))
        output_unreshaped = [x_unreshaped, x_log_pdf_unreshaped]
        return output_unreshaped

    def sample_next(self, t, x_prev):
        """ Sample from a learnt proposal distribution. 
            Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,state_dim]
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size]. """
        x_prev_and_yt_and_fx = self.__combine_x_and_yt_and_fx(x_prev, t, self._input_data[:,t,:])
        return self.__sample(t, x_prev_and_yt_and_fx)

    def sample_next_particles(self, t, x_prev):
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]. """
        x_prev_and_yt_and_fx = self.__combine_x_and_yt_and_fx_particles(x_prev, t, self._input_data[:,t,:])
        return self.__sample_particles(t, x_prev_and_yt_and_fx)
        
    def sample_init(self):
        """ Sample for the 0-th step using a learnt proposal distribution.
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size]. """
        x_prev = np.zeros(shape=[self._batch_size, self._state_dim])
        x_prev_and_yt_and_fx_prev = self.__combine_x_and_yt_and_fx(x_prev, 0, self._input_data[:,0,:])
        return self.__sample(0, x_prev_and_yt_and_fx_prev)

    def sample_init_particles(self):
        """ Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]. """
        x_prev = np.zeros(shape=[self._batch_size,self._n_particle,self._state_dim])
        x_prev_and_yt_and_fx_prev = self.__combine_x_and_yt_and_fx_particles(x_prev, 0, self._input_data[:,0,:])
        return self.__sample_particles(0, x_prev_and_yt_and_fx_prev)

    def __apply_h(self, x, lstm_output):
        node_input1 = self._dnn_h_input1
        node_input2 = self._dnn_h_input2
        node_output = self._dnn_h_output
        feed_dict = {node_input1: x, node_input2: lstm_output}
        h = self._sess.run(node_output, feed_dict=feed_dict)
        return h

    def _compute_h(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,state_dim]
            (2) t : scalar
            Output type: array[batch_size]. """
        lstm_output = self._lstm_output_val[:,t+1,:]
        h = self.__apply_h(x, lstm_output)
        return h

    def _compute_h_particles(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) t : scalar
            Output type: array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.__get_dim()
        lstm_size = self._lstm_size 
        num_entries = batch_size * n_particle

        lstm_output = self._lstm_output_val[:,t+1,:]
        lstm_output_expanded = np.expand_dims(lstm_output, axis=1)
        lstm_output_tiled = np.tile(lstm_output_expanded, (1,n_particle,1))
        lstm_output_reshaped = np.reshape(lstm_output_tiled, (num_entries,lstm_size))

        x_reshaped = np.reshape(x, (num_entries,state_dim))

        output = self.__apply_h(x_reshaped, lstm_output_reshaped)
        output_unreshaped = np.reshape(output, (batch_size,n_particle))
        return output_unreshaped

    def _compute_h_batch(self, x, t, b):
        """ Compute h_t(x) for arbitrary x and input from batch b. 
            Input type:
            (1) x : array[a,state_dim]
            (2) t : scalar
            (3) b : scalar
            Output type: array[a]. """
        node_input1 = self._dnn_h_input1
        node_input2 = self._dnn_h_input2
        node_output = self._dnn_h_output
        num_x = x.shape[0]
        lstm_output = self._lstm_output_val[b,t+1,:]
        lstm_output_rep = np.tile(lstm_output,(num_x,1))
        feed_dict = {node_input1: x, node_input2: lstm_output_rep}
        h = self._sess.run(node_output, feed_dict=feed_dict)
        return h

    def compute_h(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,state_dim]
            (2) t : scalar
            Output type: array[batch_size]. """
        if (t == -1):
            batch_size, _, _, _ = self.__get_dim()
            return np.zeros(shape=[batch_size]) 
        else:
            return self._compute_h(x, t)

    def compute_h_particles(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) t : scalar
            Output type: array[batch_size,n_particle]. """
        if (t == -1):
            batch_size, _, n_particle, _ = self.__get_dim()
            return np.zeros(shape=[batch_size,n_particle])
        else:
            return self._compute_h_particles(x, t)

    def compute_h_batch(self, x, t, b):
        """ Compute h_t(x) for arbitrary x and input from batch b. 
            Input type:
            (1) x : array[a,state_dim]
            (2) t : scalar
            (3) b : scalar
            Output type: array[a]. """
        if (t == -1):
            num_x = x.shape[0]
            return np.zeros(shape=[num_x])
        else:
            return self._compute_h_batch(x, t, b)

    def compute_h_diff_next(self, x_prev, x, t):
        """ Compute h_t(x) - h_(t-1)(x_prev). 
            Input type:
            (1) x_prev : array[batch_size,state_dim]
            (2) x : array[batch_size,state_dim]
            (3) t : scalar
            Output type: array[batch_size]. """
        h1 = self.compute_h(x_prev, t-1)
        if (t < self._n_step-1):
            h2 = self.compute_h(x, t)
            return h2-h1
        else:
            return -h1

    def compute_h_diff_next_particles(self, x_prev, x, t):
        """ Compute h_t(x) - h_(t-1)(x_prev). 
            Input type:
            (1) x_prev : array[batch_size,n_particle,state_dim]
            (2) x : array[batch_size,n_particle,state_dim]
            (3) t : scalar
            Output type: array[batch_size,n_particle]. """
        h1 = self.compute_h_particles(x_prev, t-1)
        if (t < self._n_step-1):
            h2 = self.compute_h_particles(x, t)
            return h2-h1
        else:
            return -h1

    def compute_h_diff_init(self, x):
        """ Compute h_0(x). 
            Input type:
            (1) x : array[batch_size,state_dim]
            Output type: array[batch_size]. """
        x_prev = np.zeros(shape=[self._batch_size, self._state_dim])
        return self.compute_h_diff_next(x_prev, x, 0)

    def compute_h_diff_init_particles(self, x):
        """ Compute h_0(x). 
            Input type:
            (1) x : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        x_prev = np.zeros(shape=[self._batch_size, self._n_particle, self._state_dim])
        return self.compute_h_diff_next_particles(x_prev, x, 0)

    def __generate_input_smc_rev(self, rollout, weight, ancestry):
        batch_size, n_step, n_particle, state_dim = rollout.shape
        input_data = self._input_data
        shape = [batch_size, n_step, n_particle, 3*state_dim+4]

        input_smc = np.zeros(shape=shape)
        input_smc[:,:,:,0:state_dim] = rollout[:,:,:,:]
        for b in xrange(batch_size):
            for t in xrange(1,n_step):
                for i in xrange(n_particle): 
                    j = ancestry[b,t-1,i]
                    ancestor = rollout[b,t-1,j,:]
                    input_smc[b,t,i,(state_dim+1):(2*state_dim+1)] = ancestor
                    input_smc[b,t,i,(2*state_dim+4):(3*state_dim+4)] = self._compute_next_mean(t,ancestor)
        for t in xrange(n_step): 
            for i in xrange(n_particle):
                input_smc[:,t,i,state_dim] = t
                input_smc[:,t,i,2*state_dim+1] = input_data[:,t,0]
        input_smc[:,:,:,2*state_dim+2] = weight[:,:,:,0]

        weight_entropy = np.exp(weight)*weight
        weight_entropy_sum = np.zeros(shape=[batch_size,n_step])
        for b in xrange(batch_size):
            for t in xrange(n_step):
                weight_entropy_sum[b,t] = np.log(-np.sum(weight_entropy[b,t,:,0]))
        for i in xrange(n_particle): 
            input_smc[:,:,i,2*state_dim+3] = weight_entropy_sum[:,:]
        return np.flip(input_smc, axis=1)

    def __generate_input_lstm_rev(self, input_data):
        input_data_rev = np.flip(input_data, axis=1)
        return input_data_rev

    def train(self, iter_num, rollout, weight, input_data, ancestry):
        weight_expanded = np.expand_dims(weight, axis=3)
        input_smc_rev = self.__generate_input_smc_rev(rollout,weight_expanded,ancestry)
        input_data_rev = self.__generate_input_lstm_rev(input_data)

        loss_input_rev = self._loss_input_rev
        input_rev = self._input_rev

        feed_dict = {input_rev: input_data_rev, loss_input_rev: input_smc_rev}
        minimize = self._minimize 
        self._sess.run(minimize, feed_dict=feed_dict) 
        #loss_val = self._sess.run(self._loss_output_rev, feed_dict=feed_dict) 
        #print "(Iter, ProxyLoss): (%d, %.3f)" % (iter_num, loss_val)
        return

    def estimate_objective(self, weight):
        batch_size, _, n_particle = weight.shape
        weight_entropy = np.exp(weight)*weight
        weight_entropy_mean = np.sum(weight_entropy,axis=2) / n_particle # array[batch_size,n_step]
        weight_entropy_mean_t = np.sum(weight_entropy_mean, axis=0) / batch_size # array[n_step]
        weight_entropy_mean_t += np.log(n_particle)/n_particle
        return weight_entropy_mean_t

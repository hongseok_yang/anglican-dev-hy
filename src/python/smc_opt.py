import numpy as np
from scipy.misc import logsumexp
import time

""" Implementation of Sequential Monte Carlo with Heuristics """

class SMC(object):

    def __init__(self, exper, oracle):
        """ Initialisation.
            Assumption:
            (1) The exper object has fields, batch_size, n_step, n_particle and state_dim.
            (2) The exper object implements the following four functions:
                    compute_log_pi_ratio_init(), compute_log_pi_ratio_next(),
                    compute_log_pi_ratio_init_particles(), compute_log_pi_ratio_next_particles(). 
            (3) The oracle object implements eleven functions:
                    sample_init(), sample_next(),
                    sample_init_particles(), sample_next_particles(),
                    compute_h(), 
                    compute_h_diff_init(), compute_h_diff_next(),
                    compute_h_diff_init_particles(), compute_h_diff_next_particles().  """ 

        self._oracle = oracle
        self._exper = exper
        batch_size, n_step, n_particle, state_dim = exper.get_dim()
        self._batch_size = batch_size
        self._n_step = n_step
        self._n_particle = n_particle
        self._state_dim = state_dim
        self._log_threshold = np.log(exper.adapt_threshold * n_particle)
        return

    def __init_log(self):
        self._time_resampling = 0.
        self._time_sample = 0.
        self.h_diff_max = float('-inf')
        self.h_diff_min = float('inf')
        self.log_pi_ratio_max = float('-inf')
        self.log_pi_ratio_min = float('inf')
        self.log_q_max = float('-inf')
        self.log_q_min = float('inf')
        self._n_resampling = 0
        return

    def __update_log_sample(self, h_diff, log_pi_ratio, log_q):
        self.h_diff_max = max(self.h_diff_max, np.max(h_diff)) 
        self.h_diff_min = min(self.h_diff_min, np.min(h_diff)) 
        self.log_pi_ratio_max = max(self.log_pi_ratio_max, np.max(log_pi_ratio))
        self.log_pi_ratio_min = min(self.log_pi_ratio_min, np.min(log_pi_ratio))
        self.log_q_max = max(self.log_q_max, np.max(log_q))
        self.log_q_min = min(self.log_q_min, np.min(log_q))
        return

    def __update_log_resampling(self, is_resampled):
        if is_resampled:
            self._n_resampling += 1
        return

    def __get_dim(self):
        batch_size = self._batch_size
        n_step = self._n_step
        n_particle = self._n_particle
        state_dim = self._state_dim
        return batch_size, n_step, n_particle, state_dim

    def __sample_init(self, rollout, weight, marginal, ys):
        time_start = time.time()
        batch_size, _, n_particle, state_dim = self.__get_dim()
        oracle = self._oracle
        exper = self._exper

        x, x_log_pdf = oracle.sample_init_particles()
        rollout[:,0,:,:] = x

        h_diff = oracle.compute_h_diff_init_particles(x) 
        log_pi_ratio = exper.compute_log_pi_ratio_init_particles(x, ys)
        self.__update_log_sample(h_diff, log_pi_ratio, x_log_pdf)
        weight[:,0,:] += (log_pi_ratio + h_diff - x_log_pdf - np.log(n_particle))
	    # print(ys[0,0,:])
        # print(x[0,0,:])
	    # print(np.exp(weight[0,0,:]))

        marginal[:,0] = logsumexp(weight[:,0,:], axis=1) 
        weight[:,0,:] -= np.expand_dims(marginal[:,0], axis=1)
        time_end = time.time()
        self._time_sample += (time_end - time_start) 
        return

    def __sample_next(self, rollout, weight, marginal, ancestry, t, ys):
        assert (t > 0)
        time_start = time.time()
        batch_size, _, n_particle, state_dim = self.__get_dim()
        oracle = self._oracle
        exper = self._exper

        x_prev = np.zeros(shape=[batch_size, n_particle, state_dim])
        for b in xrange(batch_size): 
            for i in xrange(n_particle): 
                j = ancestry[b,t-1,i]
                x_prev[b,i,:] = rollout[b,t-1,j,:]

        x, x_log_pdf = oracle.sample_next_particles(t, x_prev)
        rollout[:,t,:,:] = x

        h_diff = oracle.compute_h_diff_next_particles(x_prev, x, t)
        log_pi_ratio = exper.compute_log_pi_ratio_next_particles(t, x_prev, x, ys)
        self.__update_log_sample(h_diff, log_pi_ratio, x_log_pdf)
        weight[:,t,:] += (log_pi_ratio + h_diff - x_log_pdf)

        marginal[:,t] = logsumexp(weight[:,t,:], axis=1) 
        weight[:,t,:] -= np.expand_dims(marginal[:,t], axis=1)
        time_end = time.time()
        self._time_sample += (time_end - time_start) 
        return

    def __should_resample(self, weight_step): 
        log_ess = -logsumexp(weight_step * 2.) 
        return log_ess < self._log_threshold

    def __resample_one_batch(self, weight_step):
        n_particle = self._n_particle
        assert (n_particle > 0)
        ancestry_step = np.zeros(shape=[n_particle])
        i = 0; log_weight_sum = weight_step[0]
        j = 0; log_U_delta = np.log(np.random.uniform(0.,1./n_particle)); log_U_curr = log_U_delta
        # Loop invariant: 
        #   (1) sum_{k <= i} exp(weight_step[k]) = exp(log_weight_sum) 
        #   (2) sum_{k <= j} exp(log_U_delta) = exp(log_U_curr)
        #   (3) i < n_particle
        #   (4) j <= n_particle
        #   (5) sum_{k < j} exp(log_U_delta) <= exp(log_weight_sum)
        while (j < n_particle):
            while (log_U_curr > log_weight_sum):
                i += 1 
                log_weight_sum = logsumexp([log_weight_sum, weight_step[i]]) 
            ancestry_step[j] = i 
            log_U_curr = logsumexp([log_U_curr, log_U_delta])
            j += 1
        return ancestry_step 
                
    def __resample(self, weight, marginal, ancestry, t):
        batch_size, n_step, n_particle, _ = self.__get_dim()
        time_start = time.time() 
        for b in xrange(batch_size):
            if self.__should_resample(weight[b,t,:]):
                ancestry[b,t,:] = self.__resample_one_batch(weight[b,t,:]) 
                if (t < n_step-1): 
                    weight[b,t+1,:] = -np.log(n_particle)
                self.__update_log_resampling(True)
            else:
                ancestry[b,t,:] = range(n_particle)
                if (t < n_step-1): 
                    weight[b,t+1,:] = weight[b,t,:] 
                self.__update_log_resampling(False)
        time_end = time.time() 
        self._time_resampling += (time_end - time_start)
        return

    def __create_rollout(self):
        batch_size, n_step, n_particle, state_dim = self.__get_dim()
        shape = [batch_size, n_step, n_particle, state_dim]
        return np.zeros(shape=shape)

    def __create_weight(self):
        batch_size, n_step, n_particle, _ = self.__get_dim()
        return np.zeros(shape=[batch_size, n_step, n_particle])

    def __create_marginal(self):
        batch_size, n_step, _, _ = self.__get_dim()
        return np.zeros(shape=[batch_size, n_step])

    def __create_marginal_mean_acc(self):
        batch_size, _, _, _ = self.__get_dim()
        return np.zeros(shape=[batch_size])

    def __create_ancestry(self):
        batch_size, n_step, n_particle, _ = self.__get_dim()
        return np.zeros(shape=[batch_size, n_step, n_particle], dtype=int)

    def run(self, ys):
        """ Run the SMC algorithm for the observation ys. """
        batch_size, n_step, n_particle, _ = self.__get_dim()

        # rollout : numpy array of shape [batch_size,n_step,n_particle,state_dim].
        # rollout stores all samples.
        rollout = self.__create_rollout() 

        # weight : numpy array of shape [batch_size,n_step,n_particle].
        # weight stores the log of normalised weights of all samples. Initialised to zero.
        weight = self.__create_weight() 

        # marginal : numpy array of shape [batch_size,n_step].
        # marginal stores the log of the sum of unnormalised weights. Initialized to zero.
        marginal = self.__create_marginal() 

        # marginal_mean_acc : numpy array of shape [batch_size].
        # marginal_mean_acc stores the log of the product of the mean of unnormalised weights. 
        # Initialized to zero.
        marginal_mean_acc = self.__create_marginal_mean_acc() 
        x_init = np.zeros(shape=[self._batch_size, self._state_dim])
        marginal_mean_acc = self._oracle.compute_h(x_init, -1)

        # ancestry : numpy array of shape [batch_size,n_step,n_particle].
        # ancestry stores the results of resampling.
        ancestry = self.__create_ancestry() 

        self.__init_log()
        for t in xrange(n_step):
            if (t == 0): 
                self.__sample_init(rollout, weight, marginal, ys) 
            else:
                self.__sample_next(rollout, weight, marginal, ancestry, t, ys)
            self.__resample(weight, marginal, ancestry, t) 
        marginal_mean_acc += np.sum(marginal, axis=1)

        # print(marginal_mean_acc)
        print("Time spent for resampling during SMC: %f" % self._time_resampling)
        print("Time spent for sampling during SMC: %f" % self._time_sample)
        return rollout, weight, marginal, marginal_mean_acc, ancestry

    def get_average_n_resampling(self):
        return (float(self._n_resampling) / self._batch_size)

    def get_n_step(self):
        return self._n_step 

    def get_n_particle(self):
        return self._n_particle

from abc import ABCMeta, abstractmethod

""" Base class for a state space model. """

class Model(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def get_dim(self):
        """ Output:
            (1) state_dim : integer - dimension of a state
            (2) obs_dim : integer - dimension of an observation """
        pass

    @abstractmethod
    def log_lkhd(self, t, y, x): 
        """ Input type: 
            (1) t : scalar
            (2) y : array[d1,...,dn,obs_dim]
            (3) x : array[d1,...,dn,d(n+1),...,d(n+m),state_dim] 
            Output type: array[d1,...,dn,d(n+1),...,d(n+m)] """
        pass

    @abstractmethod
    def log_init_prob(self, x): 
        """ Input type: 
            (1) x : array[d1,...,dn,state_dim] 
            Output type: array[d1,...,dn] """
        pass

    @abstractmethod
    def log_trans_prob(self, t, x_prev, x_curr): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[d1,...,dn,state_dim]
            (3) x_curr : array[d1,...,dn,state_dim]
            Output type: array[d1,...,dn] """
        pass

    @abstractmethod
    def sample_init(self, shape): 
        """ Input type:
            (1) shape = (d1,...,dn) for some n and di : integer
            Output type:
            (1) x : array[d1,...,dn,state_dim] """
        pass

    @abstractmethod
    def sample_next(self, t, x_prev):
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[d1,...,dn,state_dim]
            Output type:
            (1) x : array[d1,...,dn,state_dim] """
        pass

    @abstractmethod
    def sample_obs(self, t, x):
        """ Input type:
            (1) t : scalar
            (2) x : array[d1,...,dn,state_dim]
            Output type:
            (1) y : array[d1,...,dn,obs_dim] """
        pass

from abc import ABCMeta, abstractmethod
from model.base import Model

import numpy as np

""" Linear Gaussian State Space Model from Naesseth et al.'s 2017 VSMC paper """

def expand_by_repetition(y, x): 
    """ Input type: 
        (1) y : array[d1,...,dn,d]
        (2) x : array[d1,...,dn,d(n+1),...,d(n+m),d']
        Output type: array[d1,...,dn,d(n+1),...,d(n+m),d] """ 
    shape_y = y.shape; dim_y = len(shape_y) 
    shape_x = x.shape; dim_x = len(shape_x)
    cur_y = y 
    for i in xrange(dim_y-1, dim_x-1): 
        cur_y = np.expand_dims(cur_y, axis=i) 
    repeat_info = (1,) * (dim_y-1) + (dim_x[(dim_y-1):(dim_x-1)]) + (1,) 
    new_y = np.tile(cur_y, repeat_info)
    return new_y

def compute_log_pdf_mv_standard_normal(x):
    """ Input type:
        (1) x : array[d1,...,dn,d]
        Output type: array[d1,...,dn] """
    n = len(x.shape) - 1
    log_pdf_pre = - np.square(x) / (2 * np.pi) - 0.5 * np.log(2 * np.pi)
    log_pdf = np.sum(log_pdf_pre, axis=n)
    return log_pdf

def matmul_vectorised(A, x):
    """ Input type:
        (1) A : array[r,c]
        (2) B : array[d1,...,dn,c]
        Output type: array[d1,...,dn,r] """ 
    return np.einsum('ij,...j->...i', A, x)

class ModelLinearGaussianHigh(Model):

    def __init__(self):
        self.state_dim = 10
        self.obs_dim = self.state_dim

        self.__C = np.identity(self.state_dim) 
        self.__Q = np.identity(self.state_dim)
        self.__R1 = 0.1 * np.identity(self.state_dim)
        self.__R2 = np.identity(self.state_dim)
        self.__I = np.identity(self.state_dim)

        alpha = 0.42
        self.__A = np.zeros(shape=(self.state_dim,self.state_dim))
        for i in xrange(self.state_dim):
            for j in xrange(self.state_dim):
                self.__A[i,j] = alpha ** (abs(i-j) + 1)

        self.__zero = np.zeros(shape=(self.state_dim))
        return

    def get_dim(self):
        """ Output:
            (1) state_dim : Integer - dimension of a state
            (2) obs_dim : Integer - dimension of an observation """
        return (self.state_dim, self.obs_dim)


    def log_lkhd(self, t, y, x): 
        """ Input type: 
            (1) t : scalar
            (2) y : array[d1,...,dn,obs_dim]
            (3) x : array[d1,...,dn,d(n+1),...,d(n+m),state_dim] 
            Output type: array[d1,...,dn,d(n+1),...,d(n+m)] """
        expanded_y = expand_by_repetition(y, x) 
        return compute_log_pdf_mv_standard_normal(expanded_y - x)

    def log_init_prob(self, x): 
        """ Input type: 
            (1) x : array[d1,...,dn,state_dim] 
            Output type: array[d1,...,dn] """
        return compute_log_pdf_mv_standard_normal(x)

    def log_trans_prob(self, t, x_prev, x_curr): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[d1,...,dn,state_dim]
            (3) x_curr : array[d1,...,dn,state_dim]
            Output type: array[d1,...,dn] """
        x_mean = matmul_vectorised(self.__A, x_prev)
        log_pdf = compute_log_pdf_mv_standard_normal(x_curr - x_mean)
        return log_pdf

    def sample_init(self, shape): 
        """ Input type:
            (1) shape = (d1,...,dn) for some n and di : Integer
            Output type:
            (1) x : array[d1,...,dn,state_dim] """
        x = np.random.multivariate_normal(self.__zero, self.__I, shape)
        return x 

    def sample_next(self, t, x_prev):
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[d1,...,dn,state_dim]
            Output type:
            (1) x : array[d1,...,dn,state_dim] """
        shape = x_prev.shape[0:-1]
        noise = np.random.multivariate_normal(self.__zero, self.__Q, shape)  
        x_mean = matmul_vectorised(self.__A, x_prev)
        x_new = x_mean + noise
        return x_new

    def sample_obs(self, t, x):
        """ Input type:
            (1) t : scalar
            (2) x : array[d1,...,dn,state_dim]
            Output type:
            (1) y : array[d1,...,dn,obs_dim] """
        shape = x.shape[0:-1]
        noise = np.random.multivariate_normal(self.__zero, self.__R, shape)
        y = noise + x
        return y

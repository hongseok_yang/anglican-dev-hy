import numpy as np
import tensorflow as tf
import scipy.stats

from exper import Exper
import util

def compute_normal_log_pdf(x, mean, var): 
    """ Input type:
        (A1) x : scalar
        (A2) mean : scalar 
        (A3) var : scalar
        or
        (B1) x : array[batch_size,n_particle]
        (B2) mean : array[batch_size,n_particle]
        (B3) var : scalar
        Output type: (A) scalar or (B) array[batch_size,n_particle]. """
    return - ((x - mean) ** 2) / (2. * var) - 0.5 * np.log(2. * np.pi * var)


class ExperJUMP(Exper):
    """ Class for storing info about performing experiments with the jumpy ssm model
        where p(x_t|x_(t-1)) = N(x_(t-1), var_v), p(y_t|x_t) = I(x_t > threshold)"""

    def __init__(self):
        """ Parameters for our algorithm """
        self.batch_size = 10 # size of a minibatch
        self.n_epoch = 5000 # number of epochs
        self.n_particle = 100 # number of particles in smc 
        self.state_dim = 1 # dimension of state 
        self.n_iter = 1 # number of gradient updates 
        self.adapt_threshold = 0.5 # fraction of ESS below which we resample
        self.h_size = 1. # amplifier for h
        self.q_learning_rate = 0.0001
        self.h_learning_rate = 0.0001
        self.optimiser = 'Adam' # 'RMSProp' or 'Adam'
        self.init_q_std = 1. # initialisation of std of q(x_t|x_(t-1))
        self.init_h = 10. # initialisation of h(y_(t+1:T-1)|x_t) or std of h(y_(t+1:T-1)|x_t) if normal

        self.lstm_size = 500 # num_units of h,c and output of lstm 
        self.num_layers = 4
        self.dnn_sizes = [500]*self.num_layers # list of layer sizes for dnn 
        self.nonlinearity = [tf.tanh]*self.num_layers # list of nonlinearities for each layer of dnn 
        self.max_grad_norm = 5.

        """ Parameters for a state-space model used in our experiment """ 
        self.n_step = 20  # number of steps in the model 
        self.var_v = 1. # variance for Gaussian transition 
        self.var_w = ((0.1)**2)*np.ones(self.n_step) # variance for Gaussian likelihood - should be << var_v
        self.threshold = 2.5 # threshold for a jump in the ssm. Used compute_threshold(p, var_v, T) to estimate to 1.d.p.
        """ Parameters for objective """
        self.kl_weights = np.ones(self.n_step)

        """ Miscellaneous parameters """
        self.gpu = 0 # gpu number to use
        self.plot_every = 100 # number of epochs per plot
        self.n_times_wanted = 5 # number of time steps(subfigures) to include in one plot
        self.linear = False # used to determine whether to use gaussian parameterisation for h
        self.use_dnnq = True # used for lstm1 to determine whether to use DNNQ or not- always true for this ssm.
        self.use_layernorm = False # use layernorm for lstm
        self.init_bootstrap = True # add bootstrap mean and (log var - 2*log init_q_std) to DNNQ output mean and log var
        self.init_dnnq_output_scale = (0.01)**2 # var of output of DNNQ / var of input to DNNQ, used for uniform initialisation of weights. If None, Xavier init is used. Only relevent if self.init_bootstrap=True
        self.init_dnnh_output_scale = None # var of output of DNNH / var of input to DNNH, used for uniform initialisation of wieghts. If None, Xavier init is used.

        return

    def compute_next_mean(self, t, x): 
        """ Input type:
            (A1) t : scalar
            (A2) x : scalar
            or
            (B1) t : scalar
            (B2) x : array[a1,...,an]
            Output type: (A) scalar or (B) array[a1,...,an]. """
        return x

    def tf_compute_next_mean(self, t, x): 
        return x

    def log_lkhd(self, t, y, x): 
        """ Input type: 
            (1) t : scalar
            (2) y : scalar 
            (3) x : array[1]
            Output type: scalar. """
        if np.absolute(x[0]) >= self.threshold:
            mean = 1.
        else:
            mean = 0.
        return compute_normal_log_pdf(np.array([y]), np.array([mean]), self.var_w[t])

    def log_lkhd_particles(self, t, y, x): 
        """ Compute log likelihood of particles.  
            Input type: 
            (1) t : scalar
            (2) y : array of shape [batch_size]
            (3) x : array of shape [batch_size,n_particle,1]
            Output type:
            (1) array of shape [batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        boolean_mean = (np.absolute(x[:,:,0]) >= self.threshold)
        mean = boolean_mean.astype(np.float32)
        y_expanded = np.tile(np.expand_dims(y,axis=1), (1,n_particle))
        return compute_normal_log_pdf(y_expanded, mean, self.var_w[t])

    def _log_init_prob(self, x): 
        """ Input type: 
            (1) x : array[1] 
            Output type: scalar. """
        return compute_normal_log_pdf(x[0], 0., self.var_v)

    def _log_init_prob_particles(self, x): 
        """ Input type: 
            (1) x : array[batch_size,n_particle,1]
            Output type: array[batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = np.zeros(shape=[batch_size,n_particle])
        return compute_normal_log_pdf(x[:,:,0], mean, self.var_v)

    def _log_trans_prob(self, t, x_prev, x_curr): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[1]
            (3) x_curr : array[1]
            Output type: scalar. """
        mean = self.compute_next_mean(t, x_prev[0])
        return compute_normal_log_pdf(x_curr[0], mean, self.var_v)

    def _log_trans_prob_particles(self, t, x_prev, x_curr): 
        """ Input type: 
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,1]
            (3) x_curr : array[batch_size,n_particle,1]
            Output type: array[batch_size,n_particle]. """
        mean = self.compute_next_mean(t, x_prev[:,:,0])
        return compute_normal_log_pdf(x_curr[:,:,0], mean, self.var_v)

    def _sample_init(self): 
        """ Output type:
            (1) x : array[batch_size,1]
            (2) mean : array[batch_size,1]
            (3) log_var : array[batch_size]. """
        batch_size, _, _, _ = self.get_dim()
        mean = np.zeros(shape=[batch_size,1])
        log_var = np.zeros(shape=[batch_size]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var        

    def _sample_init_particles(self): 
        """ Output type:
            (1) x : array[batch_size,n_particle,1]
            (2) mean : array[batch_size,n_particle,1]
            (3) log_var : array[batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = np.zeros(shape=[batch_size,n_particle,1])
        log_var = np.zeros(shape=[batch_size,n_particle]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var        

    def _sample_next(self, t, x_prev): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,1]
            Output type:
            (1) x : array[batch_size,1]
            (2) mean : array[batch_size,1]
            (3) log_var : array[batch_size]. """
        batch_size, _, _, _ = self.get_dim()
        mean = self.compute_next_mean(t, x_prev)
        log_var = np.zeros(shape=[batch_size]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var

    def _sample_next_particles(self, t, x_prev): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,1]
            Output type:
            (1) x : array[batch_size,n_particle,1]
            (2) mean : array[batch_size,n_particle,1]
            (3) log_var : array[batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = self.compute_next_mean(t, x_prev)
        log_var = np.zeros(shape=[batch_size,n_particle]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var

    def _sample_obs(self, t, x):
        """ Sample y from p(y_t|x_t).
            Input type:
            (1) t : scalar
            (2) x : array[batch_size,1]
            Output type: array[batch_size,1] """        
        boolean_mean = (np.absolute(x) > self.threshold)
        mean = boolean_mean.astype(np.float32)
        return np.random.normal(mean, np.sqrt(self.var_w[t])) 

    def get_dim(self):
        return self.batch_size, self.n_step, self.n_particle, self.state_dim

    def plot(self, fixed_xs_ys, oracle, epoch_cur, iter_cur, dir_name):
        return

class ExperPrior(ExperJUMP):
    def __init__(self):
        super(ExperPrior, self).__init__()
        return


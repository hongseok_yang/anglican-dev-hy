from abc import ABCMeta, abstractmethod

import numpy as np

""" Base class for storing info about performing experiments """

class Exper(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def log_lkhd(self, t, y, x): 
        """ Input type: 
            (1) t : scalar
            (2) y : scalar 
            (3) x : array[state_dim] 
            Output type: scalar. """
        pass

    @abstractmethod
    def log_lkhd_particles(self, t, y, x):
        """ Input type: 
            (1) t : scalar
            (2) y : array[batch_size] 
            (3) x : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        pass

    def log_lkhd_lifted(self, t, y, x): 
        """ Input type: 
            (1) y : scalar 
            (2) x : array[a,state_dim] 
            Output type: array[a]. """
        size = x.shape[0]
        result = np.zeros(shape=[size])
        for i in xrange(size):
            result[i] = self.log_lkhd(t, y, x[i,:])
        return result

    @abstractmethod
    def _log_init_prob(self, x): 
        """ Input type: 
            (1) x : array[state_dim] 
            Output type: scalar. """
        pass

    @abstractmethod
    def _log_init_prob_particles(self, x): 
        """ Input type: 
            (1) x : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        pass

    @abstractmethod
    def _log_trans_prob(self, t, x_prev, x_curr): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[state_dim]
            (3) x_curr : array[state_dim]
            Output type: scalar. """
        pass

    @abstractmethod
    def _log_trans_prob_particles(self, t, x_prev, x_curr): 
        """ Input type: 
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            (3) x_curr : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        pass

    @abstractmethod
    def _sample_init(self): 
        """ Output type:
            (1) x : array[batch_size,state_dim]
            (2) mean : array[batch_size,state_dim]
            (3) log_var : array[batch_size]. """
        pass

    @abstractmethod
    def _sample_init_particles(self): 
        """ Sample initial states from a prior.
            Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) mean : array[batch_size,n_particle,state_dim]
            (3) log_var : array[batch_size,n_particle]. """
        pass

    @abstractmethod
    def _sample_next(self, t, x_prev):
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,state_dim]
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) mean : array[batch_size,state_dim]
            (3) log_var : array[batch_size]. """
        pass

    @abstractmethod
    def _sample_next_particles(self, t, x_prev): 
        """ Sample states from a transition kernel p_t(.|x) for t = 1, ..., T-1.  
            Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) mean : array[batch_size,n_particle,state_dim]
            (3) log_var : array[batch_size,n_particle]. """
        pass

    @abstractmethod
    def _sample_obs(self, x):
        pass

    def init_mean(self, x):
        """ Give the mean of prior p(x_0) with same shape as x.
            Input type: array[batch_size,state_dim] or [batch_size,n_particle,state_dim]
            Output type: array[batch_size,state_dim] or [batch_size,n_particle,state_dim]. """
        _, mean, _ = self._sample_init() # array[batch_size,state_dim]
        assert x.shape[0] == mean.shape[0]
        if len(x.shape) == 2:
            return mean
        elif len(x.shape) == 3:
            return np.expand_dims(mean, axis=1) * np.ones_like(x) # uses broadcasting
        else:
            raise Exception('Error: x does not have shape [B,D] or [B,N,D]')

    def init_log_var(self):
        """ Give the log variance of prior p(x_0).
            Output type: scalar. """
        _, _, log_var = self._sample_init() # array[batch_size]
        return log_var[0]

    @abstractmethod
    def get_dim(self):
        pass

    def sample_prior_init(self): 
        """ Sample initial states from a prior.
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size]. """
        batch_size, _, _, _ = self.get_dim()
        x, mean, log_var = self._sample_init()
        x_log_pdf = np.zeros(shape=[batch_size])
        for b in xrange(batch_size):
            x_log_pdf[b] = self._log_init_prob(x[b,:])
        return x, x_log_pdf, mean, log_var

    def sample_prior_init_particles(self): 
        """ Sample initial states from a prior.
            Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]
            (3) mean : array[batch_size,n_particle,state_dim]
            (4) log_var : array[batch_size,n_particle]. """
        x, mean, log_var = self._sample_init_particles()
        x_log_pdf = self._log_init_prob_particles(x)
        return x, x_log_pdf, mean, log_var

    def sample_prior_next(self, t, x_prev):
        """ Sample states from a transition kernel p_t(.|x) for t = 1, ..., T-1.  
            Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,state_dim]
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size]. """
        batch_size, _, _, _ = self.get_dim()
        x, mean, log_var = self._sample_next(t, x_prev)
        x_log_pdf = np.zeros(shape=[batch_size])
        for b in xrange(batch_size):
            x_log_pdf[b] = self._log_trans_prob(t, x_prev[b,:], x[b,:])
        return x, x_log_pdf, mean, log_var

    def sample_prior_next_particles(self, t, x_prev): 
        """ Sample states from a transition kernel p_t(.|x) for t = 1, ..., T-1.  
            Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]
            (3) mean : array[batch_size,n_particle,state_dim]
            (4) log_var : array[batch_size,n_particle]. """
        x, mean, log_var = self._sample_next_particles(t, x_prev)
        x_log_pdf = self._log_trans_prob_particles(t, x_prev, x)
        return x, x_log_pdf, mean, log_var

    def generate_minibatch(self):
        """ Generate a minibatch from a joint model.The minibatch includes
            both sequences of hiden states and sequences of observations. 
            Output type:
            (1) state: [batch_size,n_step,state_dim]
            (2) obs: [batch_size,n_step,1]. """
        batch_size, n_step, _, state_dim = self.get_dim()
        state = np.zeros(shape=[batch_size, n_step, state_dim]) 
        obs = np.zeros(shape=[batch_size, n_step, 1])

        state[:,0,:], _, _ = self._sample_init()
        obs[:,0,:] = self._sample_obs(0, state[:,0,:])
        for t in xrange(1, n_step):
            state[:,t,:], _, _ = self._sample_next(t, state[:,t-1,:]) 
            obs[:,t,:] = self._sample_obs(t, state[:,t,:])
        return state, obs   

    def _compute_log_pi_ratio(self, t, x, ys, compute_log_trans):
        """ Compute log pi_t(x_{1:t}) / pi_(t-1)(x_{1:t-1}) = log p(x_t|x_(t-1) + log p(y_t|x_t).
            Input type:
            (1) t: scalar
            (2) x: array[batch_size,state_dim]
            (3) ys: array[batch_size,n_step,1]
            (4) compute_log_trans: function mapping b,t,x_t to scalar p(x_t|x_(t-1))
            Output type: array[batch_size]. """ 
        batch_size, _, _, _ = self.get_dim() 
        log_ratio = np.zeros(shape=[batch_size]) 
        for b in xrange(batch_size): 
            l = self.log_lkhd(t, ys[b,t,0], x[b,:])
            k = compute_log_trans(b, t, x[b,:])
            log_ratio[b] = l + k 
        return log_ratio

    def compute_log_pi_ratio_init(self, x, ys): 
        """ Return log(pi_0(x) / 1) = log p(x_0) + log p(y_0|x_0).
            Input type:
            (1) x: array[batch_size,state_dim]
            (2) ys: array[batch_size,n_step,1]
            Output type: array[batch_size]. """ 
        compute_log_trans = lambda b, t, x_cur: self._log_init_prob(x[b,:])
        log_ratio = self._compute_log_pi_ratio(0, x, ys, compute_log_trans)
        return log_ratio

    def compute_log_pi_ratio_next(self, t, x_prev, x, ys): 
        """ Compute log pi_t(x_{1:t}) / pi_(t-1)(x_{1:t-1}) = log p(x_t|x_(t-1) + log p(y_t|x_t).
            Input type:
            (1) t: scalar
            (2) x_prev: array[batch_size,state_dim]
            (3) x: array[batch_size,state_dim]
            (4) ys: array[batch_size,n_step,1]
            Output type: array[batch_size]. """ 
        assert (t > 0) 
        compute_log_trans = lambda b, t, x_cur: self._log_trans_prob(t, x_prev[b,:], x_cur)
        log_ratio = self._compute_log_pi_ratio(t, x, ys, compute_log_trans)
        return log_ratio

    def _compute_log_pi_ratio_particles(self, t, x, ys, compute_log_trans):
        """ Input type:
            (1) t: scalar
            (2) x: array[batch_size,n_particle,state_dim]
            (3) ys: array[batch_size,n_step,1]
            (4) compute_log_trans: function mapping t,x_t to p(x_t|x_(t-1)) array[batch_size,n_particle]
            Output type: array[batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        log_ratio = np.zeros(shape=[batch_size,n_particle])
        l = self.log_lkhd_particles(t, ys[:,t,0], x) # array[batch_size,n_particle]
        k = compute_log_trans(t, x) # array[batch_size,n_particle]
        log_ratio = l + k
        return log_ratio
    
    def compute_log_pi_ratio_init_particles(self, x, ys): 
        """ Compute log(pi_0(x) / 1) = log p(x_0) + log p(y_0|x_0).
            Input type:
            (1) x: array[batch_size,n_particle,state_dim]
            (2) ys: array[batch_size,n_step,1]
            Output type: array[batch_size,n_particle]. """
        compute_log_trans = lambda t, x_cur: self._log_init_prob_particles(x_cur)
        log_ratio = self._compute_log_pi_ratio_particles(0, x, ys, compute_log_trans)
        return log_ratio

    def compute_log_pi_ratio_next_particles(self, t, x_prev, x, ys): 
        """ Compute log pi_t(x_{1:t}) / pi_(t-1)(x_{1:t-1}) = log p(x_t|x_(t-1) + log p(y_t|x_t).
            Input type:
            (1) t: scalar
            (2) x_prev: array[batch_size,n_particle,state_dim]
            (3) x: array[batch_size,n_particle,state_dim]
            (4) ys: array[batch_size,n_step,1]
            Output type: array[batch_size,n_particle]. """ 
        assert (t > 0) 
        compute_log_trans = lambda t, x_cur: self._log_trans_prob_particles(t, x_prev, x_cur)
        log_ratio = self._compute_log_pi_ratio_particles(t, x, ys, compute_log_trans)
        return log_ratio

    def should_plot(self, epoch_cur, iter_cur):
        return True

    @abstractmethod
    def plot(self, ys, fixed_xs_ys, smc_result, oracle, epoch_cur, iter_cur, dir_name):
        pass

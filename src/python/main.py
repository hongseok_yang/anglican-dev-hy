import numpy as np
import matplotlib
import tensorflow as tf
from tensorflow.python import debug as tf_debug
import os
import time

import smc
import util

import oracle_lookahead
import oracle_lookahead_bootstrap
import oracle_lookahead_bootstrap_new
import oracle_lookahead_no_h
import oracle_lookahead_no_lstm
import oracle_lstm_type3
import oracle_no_lstm_bootstrap
import oracle_prior
import oracle_nasmc

import exper_lgssm
import exper_nlssm
import exper_nlssm_high

""" Script that performs experiments. """ 

exper_nlssm_lstm = exper_nlssm.ExperLSTM()
exper_nlssm_prior = exper_nlssm.ExperPrior()
exper_lgssm_lstm = exper_lgssm.ExperLSTM()
exper_lgssm_prior = exper_lgssm.ExperPrior()

file_name = 'nlssm_nasmc_m10' 
dir_name = '../../resource/pictures/' + file_name
#dir_name = '../../resource/pictures'
os.system("mkdir " + dir_name) # make directory for storing plots

write = True
is_linear = False
is_high_dimensional = False
have_fixed_data_stored = True
if is_linear:
    exper_prior = exper_lgssm_prior
    exper_lstm = exper_lgssm_lstm
else:
    if is_high_dimensional:
        exper_prior = exper_nlssm_prior_high 
        exper_lstm = exper_nlssm_lstm_high 
    else: 
        exper_prior = exper_nlssm_prior 
        exper_lstm = exper_nlssm_lstm
exper = exper_lstm

config = tf.ConfigProto(); config.gpu_options.visible_device_list = str(exper.gpu) # just use /gpu:0
#config = tf.ConfigProto(device_count = {'GPU': 0}) # only use cpu's
sess = tf.Session(config = config) 
# sess = tf_debug.LocalCLIDebugWrapperSession(sess) 
# sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)

oracle_prior = oracle_prior.Oracle(sess, exper_prior)
oracle_lstm1 = oracle_lookahead.Oracle(sess, exper_lstm)
oracle_lstm2 = oracle_lstm_type3.Oracle(sess, exper_lstm)
oracle_lstm3 = oracle_lookahead_bootstrap.Oracle(sess, exper_lstm)
oracle_lstm4 = oracle_lookahead_bootstrap_new.Oracle(sess, exper_lstm)
oracle_lstm5 = oracle_lookahead_no_h.Oracle(sess, exper_lstm)
oracle_lstm6 = oracle_nasmc.Oracle(sess, exper_lstm)
oracle_no_lstm1 = oracle_no_lstm_bootstrap.Oracle(sess, exper_lstm)
oracle_no_lstm2 = oracle_lookahead_no_lstm.Oracle(sess, exper_lstm)

""" Check this Line """
oracle = oracle_lstm6

algo = smc.SMC(exper, oracle)

if have_fixed_data_stored:
    if is_linear:
        var_v = int(exper.var_v)
        var_w = int(exper.var_w)
        T = int(exper.n_step)
        test_data = np.load('../../resource/data/lgssm_m10T%d_varv%dvarw%d_test_data.npz' %(T,var_v,var_w))
        fixed_xs = test_data['x']
        fixed_ys = test_data['y']
    elif exper.n_step == 100 and exper.batch_size == 10 and exper.state_dim == 1:
        test_data = np.load('../../resource/data/nlssm_m10T100_test_data.npz') 
        fixed_xs = test_data['x']
        fixed_ys = test_data['y']
    else :
        fixed_xs, fixed_ys = exper.generate_minibatch()
fixed_xs_ys = (fixed_xs, fixed_ys)

oracle.build_graph()
init = tf.global_variables_initializer()
sess.run(init)
matplotlib.rcParams.update({'figure.max_open_warning': 0}) # suppress warning of > 20 figures

if write:
    util.open_write_output(file_name) # write output to file ../../resource/output/file_name.txt

if is_linear:
    _,_,_,marginal_lkhd = exper.future_lkhd(fixed_ys[:,:,0])
    mean_marginal = np.mean(marginal_lkhd)
    print("True log mean marginal likelihood across batch: %f" % np.log(mean_marginal))

print("##### batch_size=%d, n_step=%d, n_particle=%d #####" % (exper.batch_size, exper.n_step, exper.n_particle))
for epoch in xrange(exper.n_epoch):
    print "### Epoch: %d ###" % epoch
    _, array_ys = exper.generate_minibatch()
    for i in xrange(exper.n_iter):         
        print("### Iter %d of Epoch %d ###" % (i, epoch))
        oracle.feed_data(array_ys)  
        smc_result = algo.run(array_ys)
        (rollout, weight, _, _, ancestry) = smc_result
        oracle.train(i, rollout, weight, array_ys, ancestry)
        exper.plot(fixed_xs_ys, oracle, 5, epoch, i, dir_name)
        rollout, weight, marginal, marginal_mean_acc, ancestry = algo.run(fixed_ys)
        util.print_result(oracle, algo, rollout, weight, marginal, marginal_mean_acc, ancestry, fixed_xs)
sess.close()    

if write:
    print("##### Output_file_name = " + file_name + ".txt #####")
    

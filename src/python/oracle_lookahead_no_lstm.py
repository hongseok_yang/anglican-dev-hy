import numpy as np
from scipy.misc import logsumexp
import tensorflow as tf

from util import np_compute_neg_log_normal_pdf
from util import tf_compute_neg_log_normal_pdf
from util import tf_stack_along_col

""" Utility functions. """

def data_type(): 
    return tf.float32

class Oracle(object):

    def __init__(self, sess, exper):
        self._sess = sess # tensorflow session
        self._lstm_size = exper.lstm_size
        self._learning_rate = exper.learning_rate
        self._nonlinearity = exper.nonlinearity
        self._max_grad_norm = exper.max_grad_norm
        self._dnn_sizes = exper.dnn_sizes
        self._batch_size = exper.batch_size
        self._n_iter = exper.n_iter
        self._n_particle = exper.n_particle
        self._n_step = exper.n_step
        self._h_size = exper.h_size
        self._state_dim = exper.state_dim
        return

    def __build_lstm(self):
        shape = [None, self._n_step, 1] # shape of tensor that stores observations
        input_rev = tf.placeholder(shape=shape, dtype=data_type()) # minibatch of observations
        lstm = tf.contrib.rnn.LSTMCell(num_units=self._lstm_size)
        output_rev, _ = tf.nn.dynamic_rnn(lstm, input_rev, dtype=data_type())

        self._input_rev = input_rev
        self._output_rev = 0.*output_rev
        return

    def __build_dnn(self, input_list, num_outputs, base_name):
        dnn_hidden = tf.concat(input_list, axis=1) 
        for d in xrange(len(self._dnn_sizes)): 
            scope_name = base_name + 'Layer' + str(d+1) 
            dnn_hidden = tf.contrib.layers.fully_connected(
                            inputs=dnn_hidden, 
                            num_outputs=self._dnn_sizes[d], 
                            activation_fn=self._nonlinearity,
                            reuse=True, 
                            scope=scope_name) 
        scope_name = base_name + 'FinalLayer'
        dnn_output = tf.contrib.layers.fully_connected(
                        inputs=dnn_hidden, 
                        num_outputs=num_outputs, 
                        activation_fn=None, 
                        reuse=True, 
                        scope=scope_name)
        return dnn_output

    def __build_dnn_q_with_inputs(self, input_t_x_y, input_lstm):
        state_dim = self._state_dim
        num_outputs = state_dim + 1
        scope_name = 'DNNQ'
        output = self.__build_dnn([input_t_x_y, input_lstm], num_outputs, scope_name)
        # output = self.__build_dnn([input1], num_outputs, scope_name)
        mean = output[:,0:state_dim]
        log_var = output[:,state_dim]
        std = tf.sqrt(tf.exp(log_var))
        std = tf_stack_along_col(std, state_dim)
        dist = tf.contrib.distributions.MultivariateNormalDiag(mean, std)
        sample = dist.sample()
        log_pdf = dist.log_pdf(sample)
        return [sample, log_pdf, mean, log_var]

    def __build_dnn_q(self):
        lstm_size = self._lstm_size 
        state_dim = self._state_dim
        input1 = tf.placeholder(shape=[None,state_dim+2], dtype=data_type())
        input2 = tf.placeholder(shape=[None,lstm_size], dtype=data_type())
        self._dnn_q_input1 = input1
        self._dnn_q_input2 = input2
        self._dnn_q_output = self.__build_dnn_q_with_inputs(input1, input2)
        return 

    def __build_dnn_h_with_inputs_gaussian(self, input_x_and_t, input_lstm):
        state_dim = self._state_dim
        h_size = self._h_size
        num_outputs = state_dim + 1
        scope_name = 'DNNH'
        output = self.__build_dnn([input_x_and_t, input_lstm], num_outputs, scope_name)
        x = input_x_and_t[:, 0:state_dim]
        mu = output[:, 0:state_dim]
        log_var = output[:, state_dim]
        h = h_size * tf.exp(- tf_compute_neg_log_normal_pdf(state_dim, x, mu, log_var))
        return h

    def __build_dnn_h_with_inputs_raw(self, input_x_and_t, input_lstm):
        num_outputs = 1
        scope_name = 'DNNH'
        output = self.__build_dnn([input_x_and_t, input_lstm], num_outputs, scope_name)
        return output[:,0]

    def __build_dnn_h_with_inputs(self, input_x_and_t, input_lstm):
        #return self.__build_dnn_h_with_inputs_gaussian(input_x_and_t, input_lstm)
        return self.__build_dnn_h_with_inputs_raw(input_x_and_t, input_lstm) 

    def __build_dnn_h(self):
        lstm_size = self._lstm_size 
        state_dim = self._state_dim
        input1 = tf.placeholder(shape=[None,state_dim+1], dtype=data_type())
        input2 = tf.placeholder(shape=[None,lstm_size], dtype=data_type())
        self._dnn_h_input1 = input1
        self._dnn_h_input2 = input2
        self._dnn_h_output = self.__build_dnn_h_with_inputs(input1, input2)
        return

    def __build_loss(self):
        batch_size = self._batch_size
        lstm_size = self._lstm_size
        state_dim = self._state_dim
        n_step = self._n_step
        n_particle = self._n_particle
        n_summands_long = batch_size * n_step * n_particle
        n_summands_short = batch_size * (n_step - 1) * n_particle
        n_summands_init = batch_size 

        """ The input_rev_smc0 placeholder stores information about a particular 
            smc run over a minibatch. For all 0 <= t < n_step and 0 <= i < n_particle,
            input_rev_smc0[b,T-t,i,:] is an array containing the following six elements:
            (1) x^i_t -- state of particle i at step t
            (2) t -- step t
            (3) x^i_(t-1) -- state of particle i at step t-1 
            (4) y^b_t -- observation at step t in the b-th batch
            (5) log (w^i_t) -- log weight of the i-th particle at step t
            (6) log (sum_i (- w^i_t * log w^i_t)) -- log of the entropy-like sum. """

        n_elem = 2 * state_dim + 4
        input_rev_smc0 = tf.placeholder(shape=[batch_size,n_step,n_particle,n_elem], dtype=data_type())
        input_rev_smc1 = tf.reshape(input_rev_smc0, [n_summands_long,n_elem])

        input_rev_smc2 = input_rev_smc0[:,1:n_step,:,:]
        input_rev_smc3 = tf.reshape(input_rev_smc2, [n_summands_short,n_elem])

        output_rev_lstm0 = self._output_rev
        output_rev_lstm1 = tf.expand_dims(output_rev_lstm0, axis=2)
        output_rev_lstm2 = tf.tile(output_rev_lstm1, [1,1,n_particle,1])
        output_rev_lstm3 = tf.reshape(output_rev_lstm2, [n_summands_long,lstm_size])

        output_rev_lstm4 = output_rev_lstm2[:,0:(n_step-1),:,:]
        output_rev_lstm5 = tf.reshape(output_rev_lstm4, [n_summands_short,lstm_size])

        zero_padding = tf.zeros(shape=[batch_size,1,n_particle,lstm_size], dtype=data_type())
        output_rev_lstm6 = tf.concat([zero_padding, output_rev_lstm4], axis=1)
        output_rev_lstm7 = tf.reshape(output_rev_lstm6, [n_summands_long,lstm_size])

        output_rev_lstm8 = tf.reshape(output_rev_lstm0[:,n_step-1,:], [n_summands_init,lstm_size])

        x_long = input_rev_smc1[:,0:state_dim]
        log_w_long = input_rev_smc1[:,(2*state_dim+2)]

        log_w_short = input_rev_smc3[:,(2*state_dim+2)]
        log_neg_w_sum_short = input_rev_smc3[:,(2*state_dim+3)]

        input_rev_dnn_q = input_rev_smc1[:,state_dim:(2*state_dim+2)]
        _, _, q_mu, q_log_var = self.__build_dnn_q_with_inputs(input_rev_dnn_q, output_rev_lstm7)

        x_prev_and_t = [input_rev_smc1[:,(state_dim+1):(2*state_dim+1)], input_rev_smc1[:,state_dim:(state_dim+1)]]
        input_rev_dnn_h_long = tf.concat(x_prev_and_t, axis=1)
        h_term_long = self.__build_dnn_h_with_inputs(input_rev_dnn_h_long, output_rev_lstm3)

        x_short = input_rev_smc3[:,0:state_dim]
        t_short = input_rev_smc3[:,state_dim:(state_dim+1)] + 1
        input_rev_dnn_h_short = tf.concat([x_short, t_short], axis=1)
        h_term_short = self.__build_dnn_h_with_inputs(input_rev_dnn_h_short, output_rev_lstm5)

        input_rev_dnn_h_init = tf.zeros(shape=[n_summands_init, state_dim+1], dtype=data_type())
        h_term_init = self.__build_dnn_h_with_inputs(input_rev_dnn_h_init, output_rev_lstm8)

        log_q_term = tf.exp(log_w_long) * tf_compute_neg_log_normal_pdf(state_dim, x_long, q_mu, q_log_var)

        gamma_t_term_long = - tf.exp(log_w_long) * h_term_long 
        gamma_t_term_short = tf.exp(log_w_short) * h_term_short 
        gamma_t_term_init = h_term_init
        gamma_t_next_term = tf.exp(log_w_short) * (log_w_short + tf.exp(log_neg_w_sum_short)) * h_term_short 

        self._loss_input_rev = input_rev_smc0
        self._loss_output_rev = (tf.reduce_sum(log_q_term) 
                + tf.reduce_sum(gamma_t_term_long) 
                + tf.reduce_sum(gamma_t_term_short)
                + tf.reduce_sum(gamma_t_term_init)
                + tf.reduce_sum(gamma_t_next_term))
        return

    def __build_optimizer(self):
        minimize = tf.contrib.layers.optimize_loss(
                    loss = self._loss_output_rev,
                    global_step = tf.contrib.framework.get_global_step(),
                    learning_rate = self._learning_rate,
                    optimizer = 'Adam',
                    clip_gradients = self._max_grad_norm)
        self._minimize = minimize
        return

    def build_graph(self):
        self.__build_lstm()
        self.__build_dnn_q()
        self.__build_dnn_h()
        self.__build_loss()
        self.__build_optimizer()
        return

    def feed_data(self, input_data):
        """ Run the lstm on a given input data, and store the output of the lstm in the
            internal state of this object. input_data is a numpy array of shape 
            [batch_size,n_step,1], and the output is stored in the _lstm_output_val field
            of this object. """
        input_rev = self._input_rev 
        output_rev = self._output_rev 
        input_data_rev = np.flip(input_data, axis=1)
        output_data_rev = self._sess.run(output_rev, feed_dict={input_rev: input_data_rev})
        output_data = np.flip(output_data_rev, axis=1)
        self._input_data = input_data
        self._lstm_output_val = output_data
        return 

    def __combine_x_and_t(self, x, t):
        """ Extend a numpy array x of size [?,state_dim] with a scalar t.
            The result is an array of size [?,state_dim+1]."""
        num_x = x.shape[0]
        return np.concatenate((x, np.ones([num_x,1]) * t), axis=1)

    def __combine_t_and_x_and_yt(self, x, t, yt):
        """ Extend a numpy array x of shape [batch_size,state_dim] with an observation yt, 
            which is a numpy array of shape [batch_size,1]. The result is an array of 
            size [batch_size,state_dim+1]."""
        batch_size = x.shape[0]
        return np.concatenate([np.ones([batch_size,1]) * t, x, yt[:,t,:]], axis=1)

    def __sample(self, t, t_and_x_prev_and_yt):
        batch_size = self._batch_size
        state_dim = self._state_dim

        # Run the dnn for q to get parameters of the proposal distribution.
        node_input1 = self._dnn_q_input1
        node_input2 = self._dnn_q_input2
        node_output = self._dnn_q_output
        lstm_output = self._lstm_output_val[:,t,:]
        feed_dict = {node_input1: t_and_x_prev_and_yt, node_input2: lstm_output}
        return self._sess.run(node_output, feed_dict=feed_dict)

    def sample_next(self, t, x_prev):
        """ Sample from a learnt proposal distribution. 
            x_prev is a numpy array of shape [batch_size,state_dim], and stores the 
            states in the previous step. t is the current step. The 
            results are four numpy arrays, one of shape [batch_size,state_dim] 
            and the other three of shape [batch_size,1]. """
        t_and_x_prev_and_yt = self.__combine_t_and_x_and_yt(x_prev, t, self._input_data)
        return self.__sample(t, t_and_x_prev_and_yt)
        
    def sample_init(self):
        """ Sample for the 0-th step using a learnt proposal distribution. 
            The results are four numpy arrays, one of shape [batch_size,state_dim] 
            and the other three of shape [batch_size,1]. """
        x_prev = np.zeros(shape=[self._batch_size, self._state_dim])
        t_and_x_prev_and_yt = self.__combine_t_and_x_and_yt(x_prev, 0, self._input_data)
        return self.__sample(0, t_and_x_prev_and_yt)

    def _compute_h(self, x, t):
        """ Compute h_t(x). x is a numpy array of shape [?,state_dim]
            and stores the states in the current steps. t is the current step. 
            The result is a numpy array of shape [?]. """
        node_input1 = self._dnn_h_input1
        node_input2 = self._dnn_h_input2
        node_output = self._dnn_h_output

        lstm_output = self._lstm_output_val[:,t+1,:]
        x_and_t = self.__combine_x_and_t(x, t+1)
        feed_dict = {node_input1: x_and_t, node_input2: lstm_output}
        h = self._sess.run(node_output, feed_dict=feed_dict)
        return h

    def _compute_h_batch(self, x, t, b):
        """ Compute h_t(x) for arbitrary x and input from batch b. 
            x is a numpy array of shape [?,state_dim]
            and stores the states for which we wish to evaluate h_t(.).
            y is a numpy array of shape [n_step] used to generate output of lstm.
            t is the current step. The result is a numpy array of shape [?]. """
        node_input1 = self._dnn_h_input1
        node_input2 = self._dnn_h_input2
        node_output = self._dnn_h_output
        num_x = x.shape[0]
        lstm_output = self._lstm_output_val[b,t+1,:]
        lstm_output_rep = np.tile(lstm_output,(num_x,1))
        x_and_t = self.__combine_x_and_t(x, t+1)
        feed_dict = {node_input1: x_and_t, node_input2: lstm_output_rep}
        h = self._sess.run(node_output, feed_dict=feed_dict)
        return h

    def compute_h(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,state_dim]
            (2) t : scalar
            Output type: array[batch_size]. """
        if (t == -1):
            batch_size, _, _, _ = self.__get_dim()
            return np.zeros(shape=[batch_size]) 
        else:
            return self._compute_h(x, t)

    def compute_h_batch(self, x, t, b):
        """ Compute h_t(x) for arbitrary x and input from batch b. 
            Input type:
            (1) x : array[a,state_dim]
            (2) t : scalar
            (3) b : scalar
            Output type: array[a]. """
        if (t == -1):
            num_x = x.shape[0]
            return np.zeros(shape=[num_x])
        else:
            return self._compute_h_batch(x, t, b)

    def compute_h_diff_next(self, x_prev, x, t):
        """ Compute h_t(x) - h_(t-1)(x_prev). x_prev and x are numpy arrays 
            of shape [batch_size,state_dim], and stores the states in the previous 
            and the current steps. t is the current step. The 
            result is a numpy array of shape [batch_size]. """
        h1 = self.compute_h(x_prev, t-1)
        if (t < self._n_step-1):
            h2 = self.compute_h(x, t)
            return h2-h1
        else:
            return -h1

    def compute_h_diff_init(self, x):
        x_prev = np.zeros(shape=[self._batch_size, self._state_dim])
        return self.compute_h_diff_next(x_prev, x, 0)

    def __generate_input_smc_rev(self, rollout, weight, ancestry):
        batch_size, n_step, n_particle, state_dim = rollout.shape
        input_data = self._input_data
        shape = [batch_size, n_step, n_particle, 2*state_dim+4]

        input_smc = np.zeros(shape=shape)
        input_smc[:,:,:,0:state_dim] = rollout[:,:,:,:]
        for b in xrange(batch_size):
            for t in xrange(1,n_step):
                for i in xrange(n_particle): 
                    j = ancestry[b,t-1,i]
                    input_smc[b,t,i,(state_dim+1):(2*state_dim+1)] = rollout[b,t-1,j,:]
        for t in xrange(n_step): 
            for i in xrange(n_particle):
                input_smc[:,t,i,state_dim] = t
                input_smc[:,t,i,2*state_dim+1] = input_data[:,t,0]
        input_smc[:,:,:,2*state_dim+2] = weight[:,:,:,0]

        f = np.vectorize(lambda w: w + np.log(-w))
        weight_entropy = f(weight)
        weight_entropy_sum = np.zeros(shape=[batch_size,n_step])
        for b in xrange(batch_size):
            for t in xrange(n_step):
                weight_entropy_sum[b,t] = logsumexp(weight_entropy[b,t,:,0])
        for i in xrange(n_particle): 
            input_smc[:,:,i,2*state_dim+3] = weight_entropy_sum[:,:]
        return np.flip(input_smc, axis=1)

    def __generate_input_lstm_rev(self, input_data):
        input_data_rev = np.flip(input_data, axis=1)
        return input_data_rev

    def train(self, iter_num, rollout, weight, input_data, ancestry):
        weight_expanded = np.expand_dims(weight, axis=3)
        input_smc_rev = self.__generate_input_smc_rev(rollout,weight_expanded,ancestry)
        input_data_rev = self.__generate_input_lstm_rev(input_data)

        loss_input_rev = self._loss_input_rev
        input_rev = self._input_rev
        feed_dict = {input_rev: input_data_rev, loss_input_rev: input_smc_rev}

        minimize = self._minimize 
        self._sess.run(minimize, feed_dict=feed_dict) 
        loss_val = self._sess.run(self._loss_output_rev, feed_dict=feed_dict) 
        print "(Iter, ProxyLoss): (%d, %.3f)" % (iter_num, loss_val)
        return

    def estimate_objective(self, weight):
        batch_size, _, n_particle = weight.shape

        logsumexp_neg = logsumexp(weight[:,:,:] + np.log(- weight[:,:,:])) 
        logsumexp_neg_normalised = logsumexp_neg - np.log(n_particle) - np.log(batch_size)

        return -np.exp(logsumexp_neg_normalised)


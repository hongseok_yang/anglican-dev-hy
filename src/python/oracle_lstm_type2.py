import numpy as np
from scipy.misc import logsumexp
import tensorflow as tf

from util import np_compute_neg_log_normal_pdf
from util import tf_compute_neg_log_normal_pdf
from util import tf_stack_along_col
from util import multiply_gaussian
from util import tf_multiply_gaussian
from util import tf_denormalise

""" Utility functions. """

def data_type(): 
    return tf.float32

class Oracle(object):

    def __init__(self, sess, exper):
        self._sess = sess # tensorflow session
        self._lstm_size = exper.lstm_size
        self._learning_rate = exper.q_learning_rate
        self._hq_learnig_rate_ratio = exper.h_learning_rate/exper.q_learning_rate
        self._nonlinearity = exper.nonlinearity
        self._max_grad_norm = exper.max_grad_norm
        self._dnn_sizes = exper.dnn_sizes
        self._batch_size = exper.batch_size
        self._n_iter = exper.n_iter
        self._n_particle = exper.n_particle
        self._n_step = exper.n_step
        self._h_size = exper.h_size
        self._state_dim = exper.state_dim
        self._compute_next_mean = exper.compute_next_mean
        self._tf_compute_next_mean = exper.tf_compute_next_mean
        self._init_mean = exper.init_mean
        self._init_log_var = exper.init_log_var
        self._var_v = exper.var_v
        self._var_w = exper.var_w
        self._use_dnnq = exper.use_dnnq
        self._init_q_std = exper.init_q_std
        self._init_h = exper.init_h
        self._use_layernorm = exper.use_layernorm
        if not self._use_dnnq:
            self._phi_w = exper.phi_w
        return

    def __get_dim(self):
        return (self._batch_size, self._n_step, self._n_particle, self._state_dim)

    def __build_lstm(self):
        shape = [None, self._n_step, 1] # shape of tensor that stores observations
        input_rev = tf.placeholder(shape=shape, dtype=data_type()) # minibatch of observations
        if self._use_layernorm:
            lstm = tf.contrib.rnn.LayerNormBasicLSTMCell(num_units=self._lstm_size)
        else:
            lstm = tf.contrib.rnn.LSTMCell(num_units=self._lstm_size)
        output_rev, _ = tf.nn.dynamic_rnn(lstm, input_rev, dtype=data_type())

        self._input_rev = input_rev
        self._output_rev = output_rev
        return

    def __build_dnn(self, input_list, num_outputs, base_name, reuse, bias_init_final):
        dnn_hidden = tf.concat(input_list, axis=1) 
        for d in xrange(len(self._dnn_sizes)): 
            scope_name = base_name + 'Layer' + str(d+1) 
            dnn_hidden = tf.contrib.layers.fully_connected(
                            inputs=dnn_hidden, 
                            num_outputs=self._dnn_sizes[d], 
                            activation_fn=self._nonlinearity[d],
                            reuse=reuse, 
                            scope=scope_name) 
        scope_name = base_name + 'FinalLayer'
        bias_init_value = np.array([0. if i < num_outputs-1 else bias_init_final 
                                    for i in range(num_outputs)]) # bias_init_final is the initial value of the bias of the last output unit.
        bias_init = tf.constant_initializer(bias_init_value)
        dnn_output = tf.contrib.layers.fully_connected(
                        inputs=dnn_hidden, 
                        num_outputs=num_outputs, 
                        activation_fn=None, 
                        biases_initializer = bias_init,
                        reuse=reuse, 
                        scope=scope_name)
        return dnn_output

    def __build_dnn_q_with_inputs(self, input_y, reuse):
        """        
        Inputs:
        (1) input_y: y_t. array[batch_size,1]             
        (2) reuse: whether to reuse learned variables at test time. Boolean.
        Outputs:
        (1) sample: sample from gaussian q(x_t|x_(t-1)). array[batch_size,state_dim]
        (2) log_pdf: log pdf of sample. array[batch_size]
        (3) mean: mean of q(x_t|x_(t-1)). array[batch_size,state_dim] 
        (4) log_var: log_var of q(x_t|x_(t-1)). array[batch_size]
        """
        state_dim = self._state_dim
        num_outputs = state_dim + 1
        scope_name = 'DNNQ'
        init_log_q_var = 2*np.log(self._init_q_std)
        output = self.__build_dnn([input_y], num_outputs, scope_name, reuse, init_log_q_var)
        mean = output[:,0:state_dim]
        log_var = output[:,state_dim]
        std = tf.sqrt(tf.exp(log_var))
        std = tf_stack_along_col(std, state_dim)
        dist = tf.contrib.distributions.MultivariateNormalDiag(mean, std)
        sample = dist.sample()
        log_pdf = dist.log_prob(sample)
        return [sample, log_pdf, mean, log_var]
    
    def __build_dnn_q(self):
        batch_size = self._batch_size 
        state_dim = self._state_dim
        input = tf.placeholder(shape=[None,1], dtype=data_type())
        self._dnn_q_input = input
        self._dnn_q_output = self.__build_dnn_q_with_inputs(input, None)
        return

    def __build_dnn_h_with_inputs_gaussian(self, input_x, input_lstm, reuse):
        state_dim = self._state_dim
        h_size = self._h_size
        num_outputs = state_dim + 1
        scope_name = 'DNNH'
        init_log_h_var = 2*np.log(self._init_h)
        output = self.__build_dnn([input_lstm], num_outputs, scope_name, reuse, init_log_h_var)
        mu = output[:, 0:state_dim]
        log_var = output[:, state_dim]
        h = - self._h_size * tf_compute_neg_log_normal_pdf(state_dim, input_x, mu, log_var)
        return h, mu, log_var

    def __build_dnn_h_with_inputs(self, input_x, input_lstm, reuse):
        """
        Inputs:
        (1) input_x: x_t. array[batch_size,state_dim]
        (2) input_lstm: v_(t+1) (output of lstm at time t+1). array[batch_size,lstm_size]
        (3) reuse: whether to reuse learned variables at test time. Boolean.
        Outputs:
        (3) h: value of h_t(x_t). array[batch_size]
        """
        h, _, _ = self.__build_dnn_h_with_inputs_gaussian(input_x, input_lstm, reuse)      
        return h

    def __build_dnn_h(self): 
        lstm_size = self._lstm_size 
        state_dim = self._state_dim
        input1 = tf.placeholder(shape=[None,state_dim], dtype=data_type())
        input2 = tf.placeholder(shape=[None,lstm_size], dtype=data_type())
        self._dnn_h_input1 = input1
        self._dnn_h_input2 = input2
        self._dnn_h_output = self.__build_dnn_h_with_inputs_gaussian(input1, input2, None)
        return

    def __build_loss(self):
        batch_size, n_step, n_particle, state_dim = self.__get_dim()
        lstm_size = self._lstm_size
        h_size = self._h_size
        n_summands_long = batch_size * n_step * n_particle
        n_summands_short = batch_size * (n_step - 1) * n_particle
        n_summands_init = batch_size

        """ The input_rev_smc0 placeholder stores information about a particular 
            smc run over a minibatch. For t=0,...,T-1 and i=0,...,n_particle-1,
            input_rev_smc0[b,T-1-t,i,:] is an array containing the following six elements:
            (1) x^i_t -- state of particle i at step t. [:,:,:,0:state_dim]
            (2) t -- step t. [:,:,:,state_dim]
            (3) x^(a_(t-1)^i)_(t-1) -- state of ancestor of x^i_t. [:,:,:,(state_dim+1):(2*state_dim+1)]
            (4) y^b_t -- observation at step t in the b-th batch. [:,:,:,2*state_dim+1]
            (5) log (w^i_t) -- log weight of the i-th particle at step t. [:,:,:,2*state_dim+2]
            (6) log (sum_i (- w^i_t * log w^i_t)) -- log of the entropy-like sum. [:,:,:,2*state_dim+3]
            (7) f(x^(a_(t-1)^i)_(t-1)) -- mean of transition p(.|x^(a_(t-1)^i)_(t-1)). [:,:,:,(2*state_dim+4):(3*state_dim+4)] """

        n_elem = 3 * state_dim + 4
        input_rev_smc0 = tf.placeholder(shape=[batch_size,n_step,n_particle,n_elem], dtype=data_type())
        input_rev_smc1 = tf.reshape(input_rev_smc0, [n_summands_long,n_elem])

        input_rev_smc2 = input_rev_smc0[:,1:n_step,:,:] # values for t=T-2,...,0 
        input_rev_smc3 = tf.reshape(input_rev_smc2, [n_summands_short,n_elem]) 
        input_rev_smc4 = input_rev_smc0[:,0:n_step-1,:,:] # values for t=T-1,...,1
        input_rev_smc5 = tf.reshape(input_rev_smc4, [n_summands_short,n_elem]) 

        output_rev_lstm0 = self._output_rev
        output_rev_lstm1 = tf.expand_dims(output_rev_lstm0, axis=2)
        output_rev_lstm2 = tf.tile(output_rev_lstm1, [1,1,n_particle,1])
        output_rev_lstm3 = tf.reshape(output_rev_lstm2, [n_summands_long,lstm_size]) # values for t=T-1,...,0

        output_rev_lstm4 = output_rev_lstm2[:,0:(n_step-1),:,:] # values for t=T-1,...,1
        output_rev_lstm5 = tf.reshape(output_rev_lstm4, [n_summands_short,lstm_size])
        
        zero_padding = tf.zeros(shape=[batch_size,1,n_particle,lstm_size], dtype=data_type())
        output_rev_lstm6 = tf.concat([zero_padding, output_rev_lstm4], axis=1) # values for t=T,...,1
        output_rev_lstm7 = tf.reshape(output_rev_lstm6, [n_summands_long,lstm_size])

        output_rev_lstm8 = tf.reshape(output_rev_lstm0[:,n_step-1,:], [n_summands_init,lstm_size]) # value for t=0

        x_long = input_rev_smc1[:,0:state_dim] # (1) for t=T-1,...,0
        x_short = input_rev_smc3[:,0:state_dim] # values for t=T-2,...,0
        y = input_rev_smc1[:,2*state_dim+1] # (4) for t=T-1,...,0

        log_w_long = input_rev_smc1[:,(2*state_dim+2)] # (5) for t=T-1,...,0        
        log_w_short = input_rev_smc3[:,(2*state_dim+2)] # (5) for t=T-2,...,0
        log_neg_w_sum_short = input_rev_smc3[:,(2*state_dim+3)] # (6) for t=T-2,...,0

        input_rev_dnn_h_long = input_rev_smc1[:,(state_dim+1):(2*state_dim+1)]
        h_term_long = self.__build_dnn_h_with_inputs(input_rev_dnn_h_long, output_rev_lstm3, True) # h_(t-1)(x^t_(t-1)) for t=T-1,...,0.
        
        h_term_short, h_mean_short, h_log_var_short = self.__build_dnn_h_with_inputs_gaussian(x_short, output_rev_lstm5, True) # h_t(x_t), its mean and log_var for t=T-2,...,0
        
        input_rev_dnn_h_init = tf.zeros(shape=[n_summands_init, state_dim], dtype=data_type())
        h_term_init = self.__build_dnn_h_with_inputs(input_rev_dnn_h_init, output_rev_lstm8, True) # h_{-1}(0)
        
        # add on h_t(x_t) for t=T-1 with mean=0, log_var=10 (something big) for sake of implementation
        h_mean_short_unreshaped = tf.reshape(h_mean_short,[batch_size,n_step-1,n_particle,state_dim])
        h_mean_unreshaped = tf.concat([tf.zeros(shape=[batch_size,1,n_particle,state_dim]),h_mean_short_unreshaped], axis = 1)
        h_mean = tf.reshape(h_mean_unreshaped, shape = [n_summands_long, state_dim])
        h_log_var_short_unreshaped = tf.reshape(h_log_var_short,[batch_size,n_step-1,n_particle])
        h_log_var_unreshaped = tf.concat([10*tf.ones(shape=[batch_size,1,n_particle]),h_log_var_short_unreshaped], axis = 1)
        h_log_var = tf.reshape(h_log_var_unreshaped, shape = [n_summands_long])        

        # compute bootstrap info
        x_prev = input_rev_smc1[:,(state_dim+1):(2*state_dim+1)]
        ts0 = input_rev_smc1[:,state_dim]
        ts = tf_stack_along_col(ts0,state_dim)
        fx_prev = self._tf_compute_next_mean(ts,x_prev)

        if not self._use_dnnq: # only applicable for SSM with one-dimensional x
            # q(.|x_(t-1)) is the product of:
            #   1. t=0,..,T-2: three gaussian pdfs: p(.|x^t_(t-1)),p(y_t|.),h_t(.)
            #   2. t=T-1: two gaussian pdfs: p(.|x^t_(t-1)),p(y_t|.)
            # compute this in one go by having h_mean = 0, h_log_var = 10 (something big) for t=T-1
            bootstrap_mean = fx_prev # mean of p(.|x^t_(t-1)) t=T-1,...,0
            bootstrap_log_var_short_unreshaped = tf.log(self._var_v) * tf.ones(shape=[batch_size,n_step-1,n_particle])
            bootstrap_log_var_init_unreshaped = self._init_log_var() * tf.ones(shape=[batch_size,1,n_particle])
            bootstrap_log_var_unreshaped = tf.concat([bootstrap_log_var_short_unreshaped,bootstrap_log_var_init_unreshaped], axis = 1)
            bootstrap_log_var = tf.reshape(bootstrap_log_var_unreshaped, shape = [n_summands_long]) # log_var of p(.|x^t_(t-1)) t=T-1,...,0. [n_summands_long]

            # denormalise y's
            y_reshaped = tf.reshape(y, [batch_size,n_step,n_particle])
            y_reshaped_flipped = tf.reverse(y_reshaped, axis=[2]) # t=0,...,T-1
            y_mean_tiled = tf.tile(self.mean_y, [1,n_particle]) # [n_step,n_particle]
            y_mean_tiled = tf.cast(y_mean_tiled, tf.float32)
            y_std_tiled = tf.tile(self.std_y, [1,n_particle]) # [n_step,n_particle]
            y_std_tiled = tf.cast(y_std_tiled, tf.float32)
            y_reshaped_flipped_denorm = tf_denormalise(y_reshaped_flipped, y_mean_tiled, y_std_tiled)
            y_reshaped_denorm = tf.reverse(y_reshaped_flipped_denorm, axis=[2]) # t=T-1,...,0
            y_denorm = tf.reshape(y, [-1]) # return to be one-dimensional

            var_w = np.flip(self._var_w, axis=0) # var_w for t=T-1,...,0
            var_w = np.expand_dims(np.expand_dims(var_w, axis=0), axis=-1) # [1,n_step,1]
            var_w = np.tile(var_w, (batch_size,1,n_particle)) # [batch_size,n_step,n_particle]
            var_w = np.reshape(var_w, [-1]) # [n_summands_long]

            obs_mean = y_denorm / self._phi_w # mean of p(y_t|.) t=T-1,...,0
            obs_log_var = tf.log(var_w/(self._phi_w)**2) * tf.ones_like(h_log_var, dtype=tf.float32) # log_var of p(y_t|.) t=T-1,...,0
            temp_mean, temp_log_var = tf_multiply_gaussian(bootstrap_mean, bootstrap_log_var, obs_mean, obs_log_var)
        else:
            # q(.|x_(t-1)) is the product of:
            #   1. t=0,..,T-2: three gaussian pdfs: p(.|x^t_(t-1)),DNNQ(y_t),h_t(.)
            #   2. t=T-1: two gaussian pdfs: p(.|x^t_(t-1)),DNNQ(y_t)
            # compute this in one go by having h_mean = 0, h_log_var = 10 (something big) for t=T-1
            bootstrap_mean = fx_prev # mean of p(.|x^t_(t-1)) t=T-1,...,0
            bootstrap_log_var = tf.log(self._var_v)*tf.ones_like(h_log_var, dtype=tf.float32) # log_var of p(.|x^t_(t-1)) t=T-1,...,0
            
            input_rev_dnn_q = input_rev_smc1[:,(2*state_dim+1):(2*state_dim+2)]
            _, _, output_mean, output_log_var = self.__build_dnn_q_with_inputs(input_rev_dnn_q, True)

            temp_mean, temp_log_var = tf_multiply_gaussian(bootstrap_mean, bootstrap_log_var, output_mean, output_log_var)

        q_mean, q_log_var = tf_multiply_gaussian(temp_mean, temp_log_var, h_mean, h_log_var)

        log_q_term = tf.exp(log_w_long) * tf_compute_neg_log_normal_pdf(state_dim, x_long, q_mean, q_log_var)
        
        gamma_t_term_long = - tf.exp(log_w_long) * h_term_long 
        gamma_t_term_short = tf.exp(log_w_short) * h_term_short 
        gamma_t_term_init = h_term_init
        #gamma_t_next_term = tf.exp(log_w_short) * (log_w_short + tf.exp(log_neg_w_sum_short)) * h_term_short 

        self._loss_input_rev = input_rev_smc0
        self._loss_output_rev = (tf.reduce_sum(log_q_term) 
                + self._hq_learnig_rate_ratio * (tf.reduce_sum(gamma_t_term_long) 
                                                 + tf.reduce_sum(gamma_t_term_short)
                                                 + tf.reduce_sum(gamma_t_term_init)))
                #+ tf.reduce_sum(gamma_t_next_term))
        return

    def __build_optimizer(self):
        minimize = tf.contrib.layers.optimize_loss(
                    loss = self._loss_output_rev,
                    global_step = tf.contrib.framework.get_global_step(),
                    learning_rate = self._learning_rate,
                    optimizer = 'Adam',
                    clip_gradients = self._max_grad_norm)
        self._minimize = minimize
        return

    def build_graph(self):
        self.__build_lstm()
        self.__build_dnn_q()
        self.__build_dnn_h()
        self.__build_loss()
        self.__build_optimizer()
        return

    def feed_data(self, input_data):
        """ Run the lstm on a given input data, and store the output of the lstm in the
            internal state of this object. input_data is a numpy array of shape 
            [batch_size,n_step,1], and the output is stored in the _lstm_output_val field
            of this object. """
        input_rev = self._input_rev 
        output_rev = self._output_rev 
        input_data_rev = np.flip(input_data, axis=1)
        output_data_rev = self._sess.run(output_rev, feed_dict={input_rev: input_data_rev})
        output_data = np.flip(output_data_rev, axis=1)
        self._input_data = input_data
        self._lstm_output_val = output_data
        return

    def __combine_x_and_yt_and_fx(self, x, t, yt):
        """ Input type:
            (1) x : array[batch_size,state_dim] 
            (2) t : scalar
            (3) yt : array[batch_size,1]
            Output type: array[batch_size,2*state_dim+1]. """
        batch_size, _, _, _ = self.__get_dim()
        if t > 0:
            fx = self._compute_next_mean(t, x)
        else:
            fx = self._init_mean(x)
        return np.concatenate([x, yt[:,:], fx], axis=1)

    def __combine_x_and_yt_and_fx_particles(self, x, t, yt):
        """ Input type:
            (1) x : array[batch_size,n_particle,state_dim] 
            (2) t : scalar
            (3) yt : array[batch_size,1]
            Output type: array[batch_size,n_particle,2*state_dim+1]. """
        batch_size, _, n_particle, _ = self.__get_dim()
        array_yt = np.tile(np.expand_dims(yt, axis=1), (1,n_particle,1))
        if t > 0:
            fx = self._compute_next_mean(t,x)
        else:
            fx = self._init_mean(x)
        return np.concatenate([x, array_yt, fx], axis=2)

    def __sample(self, t, x_prev_and_yt_and_fx):
        """ Input type:
            (1) t : scalar
            (2) x_prev_and_yt_and_fx : array[batch_size,2*state_dim+1] 
            Output type: 
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size]. 
            (5) temp_mean : array[batch_size,state_dim]. mean before incorporating h.
            (6) temp_log_var : array[batch_size]. logvar before incorporating h."""
        batch_size, n_step, _, state_dim = self.__get_dim()

        # compute h_mean, h_log_var
        if t < n_step - 1:
            lstm_output = self._lstm_output_val[:,t+1,:]
            dummy = np.zeros(shape=[batch_size,state_dim])
            _, h_mean, h_log_var = self.__apply_h(dummy, lstm_output) # first argument is a dummy: doesn't affect h_mean, h_log_var
        else:
            h_mean = np.zeros(shape=[batch_size,state_dim])
            h_log_var = 10 * np.ones(shape=[batch_size]) # something big, so that h_(T-1) close to 0.

        # compute bootstrap info
        x_prev = x_prev_and_yt_and_fx[:,0:state_dim]
        fx = x_prev_and_yt_and_fx[:,(state_dim+1):(2*state_dim+1)]
        y = x_prev_and_yt_and_fx[:,state_dim:state_dim+1]
        
        # get bootstrap mean and bootstrap log_var
        bootstrap_mean = fx # mean of p(.|x_(t-1))
        if t == 0:
            bootstrap_log_var = self._init_log_var() + np.zeros(shape=[batch_size], dtype=np.float32)
        else:
            bootstrap_log_var = np.log(self._var_v) + np.zeros(shape=[batch_size], dtype=np.float32) 
        # log_var of p(.|x_(t-1)). [batch_size]        

        if not self._use_dnnq:
            # denormalise y's
            mean_yt = self.mean_y[t,0]
            std_yt = self.std_y[t,0]
            y_denorm = y * std_yt + mean_yt

            obs_mean = y_denorm / self._phi_w # mean of p(y_t|.)
            obs_log_var = np.log(self._var_w[t]/(self._phi_w)**2) * np.ones_like(h_log_var, dtype=np.float32) # log_var of p(y_t|.)
            temp_mean, temp_log_var = multiply_gaussian(bootstrap_mean, bootstrap_log_var, obs_mean, obs_log_var)
        else:
            input = self._dnn_q_input
            output = self._dnn_q_output
            feed_dict = {input: y}
            _, _, output_mean, output_log_var = self._sess.run(output, feed_dict=feed_dict)
            temp_mean, temp_log_var = multiply_gaussian(bootstrap_mean, bootstrap_log_var, output_mean, output_log_var)
        
        q_mean, q_log_var = multiply_gaussian(temp_mean, temp_log_var, h_mean, h_log_var)

        # Generate samples from the proposal distribution.
        # the following is valid only for spherical variance
        sample = np.random.normal(q_mean, np.expand_dims(np.exp(0.5*q_log_var),-1), size=q_mean.shape)
        log_pdf = - np_compute_neg_log_normal_pdf(state_dim, sample, q_mean, q_log_var)

        return sample, log_pdf, q_mean, q_log_var, temp_mean, temp_log_var

    def __sample_particles(self, t, x_prev_and_yt_and_fx):
        """ Input type:
            (1) t : scalar
            (2) x_prev_and_yt_and_fx : array[batch_size,n_particle,2*state_dim+1] 
            Output type: 
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]. """
        batch_size, n_step, n_particle, state_dim = self.__get_dim()
        lstm_size = self._lstm_size 
        num_entries = batch_size * n_particle

        # compute h_mean, h_log_var
        if t < n_step - 1:
            lstm_output = self._lstm_output_val[:,t+1,:]
            lstm_output_expanded = np.expand_dims(lstm_output, axis=1)
            lstm_output_tiled = np.tile(lstm_output_expanded, (1,n_particle,1))
            lstm_output_reshaped = np.reshape(lstm_output_tiled, (num_entries,lstm_size))
            dummy = np.zeros(shape=[num_entries,state_dim])
            _, h_mean, h_log_var = self.__apply_h(dummy, lstm_output_reshaped) # first argument is a dummy: doesn't affect h_mean, h_log_var
        else:
            h_mean = np.zeros(shape=[num_entries, state_dim])
            h_log_var = 10 * np.ones(shape=[num_entries])

        # compute bootstrap info
        x_prev = x_prev_and_yt_and_fx[:,:,0:state_dim]
        x_prev = np.reshape(x_prev, (num_entries,state_dim))
        fx = x_prev_and_yt_and_fx[:,:,(state_dim+1):(2*state_dim+1)]
        fx = np.reshape(fx, (num_entries,state_dim))
        y = x_prev_and_yt_and_fx[:,:,state_dim:state_dim+1]
        y = np.reshape(y,(num_entries,1))
        
        # get bootstrap mean and bootstrap log_var
        bootstrap_mean = fx # mean of p(.|x_(t-1))
        if t == 0:
            bootstrap_log_var = self._init_log_var() + np.zeros(shape=[num_entries], dtype=np.float32)
        else:
            bootstrap_log_var = np.log(self._var_v) + np.zeros(shape=[num_entries], dtype=np.float32) 
        # log_var of p(.|x_(t-1)). [num_entries]   

        if not self._use_dnnq:
            # denormalise y's
            mean_yt = self.mean_y[t,0]
            std_yt = self.std_y[t,0]
            y_denorm = y * std_yt + mean_yt

            obs_mean = y_denorm / self._phi_w # mean of p(y_t|.)
            obs_log_var = np.log(self._var_w[t]/(self._phi_w)**2) * np.ones_like(h_log_var, dtype=np.float32) # log_var of p(y_t|.)
            temp_mean, temp_log_var = multiply_gaussian(bootstrap_mean, bootstrap_log_var, obs_mean, obs_log_var)
        else:
            input = self._dnn_q_input
            output = self._dnn_q_output
            feed_dict = {input: y}
            _, _, output_mean, output_log_var = self._sess.run(output, feed_dict=feed_dict)
            temp_mean, temp_log_var = multiply_gaussian(bootstrap_mean, bootstrap_log_var, output_mean, output_log_var)
        
        q_mean, q_log_var = multiply_gaussian(temp_mean, temp_log_var, h_mean, h_log_var)
        
        # Generate samples from the proposal distribution.
        # the following is valid only for spherical variance
        sample = np.random.normal(q_mean, np.expand_dims(np.exp(0.5*q_log_var),-1), size=q_mean.shape)
        log_pdf = - np_compute_neg_log_normal_pdf(state_dim, sample, q_mean, q_log_var)
        
        # reshape back to original dimensions
        sample = np.reshape(sample,(batch_size, n_particle, state_dim))
        log_pdf = np.reshape(log_pdf,(batch_size, n_particle))

        return sample, log_pdf

    def sample_next(self, t, x_prev):
        """ Sample from a learnt proposal distribution. 
            Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,state_dim]
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size]. 
            (5) temp_mean : array[batch_size,state_dim]. mean before incorporating h.
            (6) temp_log_var : array[batch_size]. logvar before incorporating h."""
        x_prev_and_yt_and_fx_prev = self.__combine_x_and_yt_and_fx(x_prev, t, self._input_data[:,t,:])
        return self.__sample(t, x_prev_and_yt_and_fx_prev)

    def sample_next_particles(self, t, x_prev):
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]. """
        x_prev_and_yt_and_fx_prev = self.__combine_x_and_yt_and_fx_particles(x_prev, t, self._input_data[:,t,:])
        return self.__sample_particles(t, x_prev_and_yt_and_fx_prev)
        
    def sample_init(self):
        """ Sample for the 0-th step using a learnt proposal distribution.
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size].
            (5) temp_mean : array[batch_size,state_dim]. mean before incorporating h.
            (6) temp_log_var : array[batch_size]. logvar before incorporating h. """
        x_prev = np.zeros(shape=[self._batch_size, self._state_dim])
        x_prev_and_yt_and_fx_prev = self.__combine_x_and_yt_and_fx(x_prev, 0, self._input_data[:,0,:])
        return self.__sample(0, x_prev_and_yt_and_fx_prev)

    def sample_init_particles(self):
        """ Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]. """
        x_prev = np.zeros(shape=[self._batch_size,self._n_particle,self._state_dim])
        x_prev_and_yt_and_fx_prev = self.__combine_x_and_yt_and_fx_particles(x_prev, 0, self._input_data[:,0,:])
        return self.__sample_particles(0, x_prev_and_yt_and_fx_prev)

    def __apply_h(self, x, lstm_output):
        node_input1 = self._dnn_h_input1
        node_input2 = self._dnn_h_input2
        node_output = self._dnn_h_output
        feed_dict = {node_input1: x, node_input2: lstm_output}
        h, h_mean, h_log_var = self._sess.run(node_output, feed_dict=feed_dict)
        return [h, h_mean, h_log_var]

    def _compute_h(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,state_dim]
            (2) t : scalar
            Output type: array[batch_size]. """
        lstm_output = self._lstm_output_val[:,t+1,:]
        h, _, _ = self.__apply_h(x, lstm_output)
        return h

    def _compute_h_particles(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) t : scalar
            Output type: array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.__get_dim()
        lstm_size = self._lstm_size 
        num_entries = batch_size * n_particle

        lstm_output = self._lstm_output_val[:,t+1,:]
        lstm_output_expanded = np.expand_dims(lstm_output, axis=1)
        lstm_output_tiled = np.tile(lstm_output_expanded, (1,n_particle,1))
        lstm_output_reshaped = np.reshape(lstm_output_tiled, (num_entries,lstm_size))

        x_reshaped = np.reshape(x, (num_entries,state_dim))

        output, _, _ = self.__apply_h(x_reshaped, lstm_output_reshaped)
        output_unreshaped = np.reshape(output, (batch_size,n_particle))
        return output_unreshaped

    def _compute_h_batch(self, x, t, b):
        """ Compute h_t(x) for arbitrary x and input from batch b. 
            Input type:
            (1) x : array[a,state_dim]
            (2) t : scalar
            (3) b : scalar
            Output type: array[a]. """
        node_input1 = self._dnn_h_input1
        node_input2 = self._dnn_h_input2
        node_output = self._dnn_h_output
        num_x = x.shape[0]
        lstm_output = self._lstm_output_val[b,t+1,:]
        lstm_output_rep = np.tile(lstm_output,(num_x,1))
        feed_dict = {node_input1: x, node_input2: lstm_output_rep}
        h, _, _ = self._sess.run(node_output, feed_dict=feed_dict)
        return h

    def compute_h(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,state_dim]
            (2) t : scalar
            Output type: array[batch_size]. """
        if (t == -1):
            batch_size, _, _, _ = self.__get_dim()
            return np.zeros(shape=[batch_size]) 
        else:
            return self._compute_h(x, t)

    def compute_h_particles(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) t : scalar
            Output type: array[batch_size,n_particle]. """
        if (t == -1):
            batch_size, _, n_particle, _ = self.__get_dim()
            return np.zeros(shape=[batch_size,n_particle])
        else:
            return self._compute_h_particles(x, t)

    def compute_h_batch(self, x, t, b):
        """ Compute h_t(x) for arbitrary x and input from batch b. 
            Input type:
            (1) x : array[a,state_dim]
            (2) t : scalar
            (3) b : scalar
            Output type: array[a]. """
        if (t == -1):
            num_x = x.shape[0]
            return np.zeros(shape=[num_x])
        else:
            return self._compute_h_batch(x, t, b)

    def compute_h_diff_next(self, x_prev, x, t):
        """ Compute h_t(x) - h_(t-1)(x_prev). 
            Input type:
            (1) x_prev : array[batch_size,state_dim]
            (2) x : array[batch_size,state_dim]
            (3) t : scalar
            Output type: array[batch_size]. """
        h1 = self.compute_h(x_prev, t-1)
        if (t < self._n_step-1):
            h2 = self.compute_h(x, t)
            return h2-h1
        else:
            return -h1

    def compute_h_diff_next_particles(self, x_prev, x, t):
        """ Compute h_t(x) - h_(t-1)(x_prev). 
            Input type:
            (1) x_prev : array[batch_size,n_particle,state_dim]
            (2) x : array[batch_size,n_particle,state_dim]
            (3) t : scalar
            Output type: array[batch_size,n_particle]. """
        h1 = self.compute_h_particles(x_prev, t-1)
        if (t < self._n_step-1):
            h2 = self.compute_h_particles(x, t)
            return h2-h1
        else:
            return -h1

    def compute_h_diff_init(self, x):
        """ Compute h_0(x). 
            Input type:
            (1) x : array[batch_size,state_dim]
            Output type: array[batch_size]. """
        x_prev = np.zeros(shape=[self._batch_size, self._state_dim])
        return self.compute_h_diff_next(x_prev, x, 0)

    def compute_h_diff_init_particles(self, x):
        """ Compute h_0(x). 
            Input type:
            (1) x : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        x_prev = np.zeros(shape=[self._batch_size, self._n_particle, self._state_dim])
        return self.compute_h_diff_next_particles(x_prev, x, 0)

    def __generate_input_smc_rev(self, rollout, weight, ancestry):
        batch_size, n_step, n_particle, state_dim = rollout.shape
        input_data = self._input_data
        shape = [batch_size, n_step, n_particle, 3*state_dim+4]

        input_smc = np.zeros(shape=shape)
        input_smc[:,:,:,0:state_dim] = rollout[:,:,:,:]
        for b in xrange(batch_size):
            for t in xrange(1,n_step):
                for i in xrange(n_particle): 
                    j = ancestry[b,t-1,i]
                    ancestor = rollout[b,t-1,j,:]
                    input_smc[b,t,i,(state_dim+1):(2*state_dim+1)] = ancestor
                    input_smc[b,t,i,(2*state_dim+4):(3*state_dim+4)] = self._compute_next_mean(t,ancestor)
        for t in xrange(n_step): 
            for i in xrange(n_particle):
                input_smc[:,t,i,state_dim] = t
                input_smc[:,t,i,2*state_dim+1] = input_data[:,t,0]
        input_smc[:,:,:,2*state_dim+2] = weight[:,:,:,0]

        weight_entropy = np.exp(weight)*weight
        weight_entropy_sum = np.zeros(shape=[batch_size,n_step])
        for b in xrange(batch_size):
            for t in xrange(n_step):
                weight_entropy_sum[b,t] = np.log(-np.sum(weight_entropy[b,t,:,0]))
        for i in xrange(n_particle): 
            input_smc[:,:,i,2*state_dim+3] = weight_entropy_sum[:,:]
        return np.flip(input_smc, axis=1)

    def __generate_input_lstm_rev(self, input_data):
        input_data_rev = np.flip(input_data, axis=1)
        return input_data_rev

    def train(self, iter_num, rollout, weight, input_data, ancestry):
        weight_expanded = np.expand_dims(weight, axis=3)
        input_smc_rev = self.__generate_input_smc_rev(rollout,weight_expanded,ancestry)
        input_data_rev = self.__generate_input_lstm_rev(input_data)

        loss_input_rev = self._loss_input_rev
        input_rev = self._input_rev
        feed_dict = {input_rev: input_data_rev, loss_input_rev: input_smc_rev}

        minimize = self._minimize 
        self._sess.run(minimize, feed_dict=feed_dict) 
        #loss_val = self._sess.run(self._loss_output_rev, feed_dict=feed_dict) 
        #print "(Iter, ProxyLoss): (%d, %.3f)" % (iter_num, loss_val)
        return

    def estimate_objective(self, weight):
        batch_size, _, n_particle = weight.shape
        weight_entropy = np.exp(weight)*weight
        weight_entropy_mean = np.sum(weight_entropy,axis=2) / n_particle # array[batch_size,n_step]
        weight_entropy_mean_t = np.sum(weight_entropy_mean, axis=0) / batch_size # array[n_step]
        weight_entropy_mean_t += np.log(n_particle)/n_particle
        return weight_entropy_mean_t


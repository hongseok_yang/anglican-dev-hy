import numpy as np
import tensorflow as tf
import scipy.stats

from exper import Exper
import util

def compute_normal_log_pdf(x, mean, var): 
    """ Input type:
        (1) x : array[a1,...,an,b]
        (2) mean : array[a1,...,an,b]
        (3) var : scalar
        Output type: array[a1,...,an]. """
    dim = x.shape[-1]
    distance = np.sum((x - mean) ** 2, axis=-1)
    return - distance / (2. * var) - 0.5 * dim * np.log(2. * np.pi * var)

""" Class for storing info about performing experiments with a simple high-dimensional nonlinear ssm model. """

class ExperNLSSM(Exper):

    def __init__(self):
        """ Parameters for our algorithm """
        self.batch_size = 10 # size of a minibatch
        self.n_epoch = 10000 # number of epochs
        self.n_particle = 100 # number of particles in smc 
        self.state_dim = 10 # dimension of state 
        self.n_iter = 1 # number of gradient updates 
        self.adapt_threshold = 0.5 # fraction of ESS below which we resample

        """ Parameters for a state-space model used in our experiment """ 
        self.n_step = 100  # number of steps in the model 
        self.var_v = 10. # variance for Gaussian transition 
        self.var_w = 1. # variance for Gaussian likelihood

        """ GPU number """
        self.gpu = 6 # gpu number to use
        self.linear = False
        return

    def compute_next_mean(self, t, x): 
        """ Input type:
            (1) t : scalar
            (2) x : array[a1,...,an,state_dim]
            Output type: array[a1,...,an,state_dim]. """
        return x 

    def tf_compute_next_mean(self, t, x): 
        """ Input type:
            (1) t : scalar
            (2) x : tensorflow_array[a1,...,an,state_dim]
            Output type: tensorflow_array[a1,...,an,state_dim]. """
        return x 

    def log_lkhd(self, t, y, x): 
        """ Input type: 
            (1) t : scalar
            (2) y : scalar
            (3) x : array[state_dim]
            Output type: scalar. """
        mean = np.sum(x) 
        return compute_normal_log_pdf(np.array([y]), np.array([mean]), self.var_w)

    def log_lkhd_particles(self, t, y, x): 
        """ Compute log likelihood of particles.  
            Input type: 
            (1) t : scalar
            (2) y : array of shape [batch_size]
            (3) x : array of shape [batch_size,n_particle,state_dim]
            Output type:
            (1) array of shape [batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = np.sum(x, axis=2) 
        mean_expanded = np.expand_dims(mean, axis=2)
        y_tiled = np.tile(np.expand_dims(y,axis=1), (1,n_particle)) 
        y_expanded = np.expand_dims(y_tiled, axis=2)
        return compute_normal_log_pdf(y_expanded, mean_expanded, self.var_w)

    def _log_init_prob(self, x): 
        """ Input type: 
            (1) x : array[state_dim] 
            Output type: scalar. """
        mean = np.zeros(shape=[state_dim])
        return compute_normal_log_pdf(x, mean, 5.)

    def _log_init_prob_particles(self, x): 
        """ Input type: 
            (1) x : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.get_dim()
        mean = np.zeros(shape=[batch_size,n_particle,state_dim])
        return compute_normal_log_pdf(x, mean, 5.)

    def _log_trans_prob(self, t, x_prev, x_curr): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[state_dim]
            (3) x_curr : array[state_dim]
            Output type: scalar. """
        mean = self.compute_next_mean(t, x_prev)
        return compute_normal_log_pdf(x_curr, mean, self.var_v)

    def _log_trans_prob_particles(self, t, x_prev, x_curr): 
        """ Input type: 
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            (3) x_curr : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        mean = self.compute_next_mean(t, x_prev)
        return compute_normal_log_pdf(x_curr, mean, self.var_v)

    def _sample_init(self): 
        """ Output type:
            (1) x : array[batch_size,state_dim]
            (2) mean : array[batch_size,state_dim]
            (3) log_var : array[batch_size]. """
        batch_size, _, _, state_dim = self.get_dim()
        mean = np.zeros(shape=[batch_size,state_dim])
        log_var = np.zeros(shape=[batch_size,state_dim]) + np.log(5.)
        return np.random.normal(mean, np.sqrt(5.)), mean, log_var        

    def _sample_init_particles(self): 
        """ Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) mean : array[batch_size,n_particle,state_dim]
            (3) log_var : array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.get_dim()
        mean = np.zeros(shape=[batch_size,n_particle,state_dim])
        log_var = np.zeros(shape=[batch_size,n_particle]) + np.log(5.)
        return np.random.normal(mean, np.sqrt(5.)), mean, log_var        

    def _sample_next(self, t, x_prev): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,state_dim]
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) mean : array[batch_size,state_dim]
            (3) log_var : array[batch_size]. """
        batch_size, _, _, _ = self.get_dim()
        mean = self.compute_next_mean(t, x_prev)
        log_var = np.zeros(shape=[batch_size]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var

    def _sample_next_particles(self, t, x_prev): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) mean : array[batch_size,n_particle,state_dim]
            (3) log_var : array[batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = self.compute_next_mean(t, x_prev)
        log_var = np.zeros(shape=[batch_size,n_particle]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var

    def _sample_obs(self,x):
        """ Input type:
            (1) x : array[a1,...,an,state_dim]
            Output type: array[a1,...,an,1]. """
        mean = np.sum(x, axis=-1) 
        obs = np.random.normal(mean, np.sqrt(self.var_w))
        return np.expand_dims(obs, axis=-1)

    def get_dim(self):
        return (self.batch_size, self.n_step, self.n_particle, self.state_dim)

    def plot(self, fixed_xs_ys, oracle, n_times_wanted, epoch_cur, iter_cur, dir_name):
        return



class ExperPrior(ExperNLSSM):
    def __init__(self):
        super(ExperPrior, self).__init__()
        return


class ExperLSTM(ExperNLSSM):
    def __init__(self):
        super(ExperLSTM, self).__init__()

        """ Parameters for our algorithm """
        self.lstm_size = 100 # num_units of h,c and output of lstm 
        self.dnn_sizes = [100,100,100] # list of layer sizes for dnn 
        self.nonlinearity = tf.tanh # tf.nn.relu
        self.max_grad_norm = 5. 
        self.learning_rate = 0.001 # default for adam 
        self.h_size = 0. # amplifier for h
        return


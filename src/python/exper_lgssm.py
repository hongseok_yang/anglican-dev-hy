import numpy as np
import scipy.stats
import tensorflow as tf
import pdb
from exper import Exper
import util

""" Utility functions """

def compute_normal_log_pdf(x, mean, var): 
    """ Input type:
        (A1) x : scalar
        (A2) mean : scalar 
        (A3) var : scalar
        or
        (B1) x : array[batch_size,n_particle]
        (B2) mean : array[batch_size,n_particle]
        (B3) var : scalar
        Output type: (A) scalar or (B) array[batch_size,n_particle]. """
    return - ((x - mean) ** 2) / (2. * var) - 0.5 * np.log(2. * np.pi * var)

""" Class for storing info about performing experiments with the linear gaussian ssm model
    q(x_{t+1}|x_t) = N(x_{t+1}; phi_v *x_t, var_v)
    p(y_t | x_t) = N(y_t; phi_w * x_t, var_w)   """
class ExperLGSSM(Exper):

    def __init__(self):
        """ Parameters for our algorithm """
        self.batch_size = 10 # size of a minibatch
        self.n_epoch = 500 # number of epochs
        self.n_particle = 100 # number of particles in smc 
        self.state_dim = 1 # dimension of state 
        self.n_iter = 1 # number of gradient updates
        self.adapt_threshold = 0.5 # fraction of ESS below which we resample 
        self.h_size = 1. # amplifier for h
        self.q_learning_rate = 0.0001
        self.h_learning_rate = 0.0001
        self.optimiser = 'Adam' # 'RMSProp' or 'Adam'
        self.init_q_std = 1. # initialisation of std of DNNQ output
        self.init_h = 10. # initialisation of std of log h(y_(t+1:T-1)|x_t)

        self.lstm_size = 100 # num_units of h,c and output of lstm 
        self.num_layers = 3
        self.dnn_sizes = [100]*self.num_layers # list of layer sizes for dnn 
        self.nonlinearity = [tf.tanh]*self.num_layers # list of nonlinearities for each layer of dnn
        self.max_grad_norm = 5. 

        """ Parameters for a state-space model used in our experiment """ 
        self.n_step = 5 # number of steps in the model 
        self.var_v = 0.1 # variance for Gaussian transition. Should be to 1.d.p. (for filename)
        self.var_w = np.array([5.**2]*(self.n_step-1) + [0.0001]) # variance for Gaussian likelihood
        self.phi_v = np.sqrt(1.)*np.ones(self.n_step) # linear coeff for mean of transition
        self.phi_w = 1. # linear coeff for mean of likelihood

        """ Parameters for objective """
        self.kl_weights = np.ones(self.n_step)

        """ Miscellaneous parameters """
        self.gpu = 0 # gpu number to use
        self.plot_every = 100 # number of epochs per plot
        self.n_times_wanted = 5 # number of time steps(subfigures) to include in one plot
        self.linear = False # used to determine whether to use gaussian parameterisation for h. Only relevant for l_b_n_d_no_t.
        self.use_dnnq = True # used for lstm1 to determine whether to use DNNQ or not
        self.use_layernorm = False # use layernorm for lstm
        self.init_bootstrap = False # add bootstrap mean and (log var - 2*log init_q_std) to DNNQ output mean and log var
        self.init_dnnq_output_scale = (1.)**2 # var of output of DNNQ / var of input to DNNQ, used for uniform initialisation of wieghts. If None, Xavier init is used. Only relevent if self.init_bootstrap=True
        self.init_dnnh_output_scale = None # var of output of DNNH / var of input to DNNH, used for uniform initialisation of wieghts. If None, Xavier init is used.

        return

    def find_phi_v(self, t):
        return self.phi_v[t]

    def compute_next_mean(self, t, x): 
        """ Compute mean of p(x_t|x_{t-1}=x).
            Input type:
            (A1) t : scalar
            (A2) x : scalar
            or
            (B1) t : scalar
            (B2) x : array[a1,...,an]
            Output type: (A) scalar or (B) array[a1,...,an]. """
        find_phi_vs = np.vectorize(self.find_phi_v)
        phi_vs = find_phi_vs(t)
        return phi_vs * x

    def tf_compute_next_mean(self, t, x):
        """ Same as above but in tf."""
        phi_v = tf.convert_to_tensor(self.phi_v, dtype=tf.float32)
        ts = tf.cast(t, tf.int32)
        next_mean = tf.gather(phi_v, ts)
        return next_mean * x

    def log_lkhd(self, t, y, x): 
        """ Input type: 
            (1) t : scalar
            (2) y : scalar 
            (3) x : array[1]
            Output type: scalar. """
        mean = self.phi_w * x[0]
        return compute_normal_log_pdf(y, mean, self.var_w[t])

    def log_lkhd_particles(self, t, y, x): 
        """ Compute log likelihood of particles. 
            Input type: 
            (1) t : scalar
            (2) y : array[batch_size]
            (3) x : array[batch_size,n_particle,1]
            Output type:
            (1) array[batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = np.reshape(self.phi_w * x, (batch_size,n_particle))
        y_expanded = np.tile(np.expand_dims(y,axis=1), (1,n_particle)) 
        return compute_normal_log_pdf(y_expanded, mean, self.var_w[t])

    def _log_init_prob(self, x): 
        """ Input type: 
            (1) x : array[1]
            Output type: scalar. """
        return compute_normal_log_pdf(x[0], 0., self.var_v)

    def _log_init_prob_particles(self, x): 
        """ Input type: 
            (1) x : array[batch_size,n_particle,1]
            Output type: array[batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = np.zeros(shape=[batch_size,n_particle])
        return compute_normal_log_pdf(x[:,:,0], mean, self.var_v)

    def _log_trans_prob(self, t, x_prev, x_curr): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[1]
            (3) x_curr : array[1]
            Output type: scalar. """
        mean = self.compute_next_mean(t, x_prev[0])
        return compute_normal_log_pdf(x_curr[0], mean, self.var_v)

    def _log_trans_prob_particles(self, t, x_prev, x_curr): 
        """ Input type: 
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,1]
            (3) x_curr : array[batch_size,n_particle,1]
            Output type: array[batch_size,n_particle]. """
        mean = self.compute_next_mean(t, x_prev[:,:,0])
        return compute_normal_log_pdf(x_curr[:,:,0], mean, self.var_v)

    def _sample_init(self): 
        """ Output type:
            (1) x : array[batch_size,1]
            (2) mean : array[batch_size,1]
            (3) log_var : array[batch_size]. """
        batch_size, _, _, _ = self.get_dim()
        mean = np.zeros(shape=[batch_size,1])
        log_var = np.zeros(shape=[batch_size]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var        

    def _sample_init_particles(self): 
        """ Output type: 
            (1) x : array[batch_size,n_particle,1]
            (2) mean : array[batch_size,n_particle,1]
            (3) log_var : array[batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = np.zeros(shape=[batch_size,n_particle,1])
        log_var = np.zeros(shape=[batch_size,n_particle]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var        

    def _sample_next(self, t, x_prev): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,1]
            Output type:
            (1) x : array[batch_size,1]
            (2) mean : array[batch_size,1]
            (3) log_var : array[batch_size]. """
        batch_size, _, _, _ = self.get_dim()
        mean = self.compute_next_mean(t, x_prev)
        log_var = np.zeros(shape=[batch_size]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var

    def _sample_next_particles(self, t, x_prev): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,1]
            Output type:
            (1) x : array[batch_size,n_particle,1]
            (2) mean : array[batch_size,n_particle,1]
            (3) log_var : array[batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = self.compute_next_mean(t, x_prev)
        log_var = np.zeros(shape=[batch_size,n_particle]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var

    def _sample_obs(self, t, x):
        """ Sample y from p(y_t|x_t).
            Input type:
            (1) t : scalar
            (2) x : array[batch_size,1]
            Output type: array[batch_size,1] """
        mean = self.phi_w * x
        return np.random.normal(mean, np.sqrt(self.var_w[t]))

    def get_dim(self):
        return self.batch_size, self.n_step, self.n_particle, self.state_dim

    def plot(self, fixed_xs_ys, oracle, epoch_cur, iter_cur, dir_name):
        if epoch_cur%(self.plot_every) == 0:
            util.plot_result_lgssm(fixed_xs_ys, self, oracle, self.n_times_wanted, epoch_cur, iter_cur, dir_name)
        return

    def future_lkhd(self, input_data):
        """ Compute c_t,mu_t,prec_t for t=0:T-1 where 
            p(y_{t+1:T-1}|x_t) := c_t * exp(-0.5*prec_t*(mu_t-x_t)^2). 
            Also output the marginal lkhd p(y_{0:T-1}).
            Defining c_(T-1) = 1, mu_(T-1) = 0, prec_(T-1) = 0 simplifies recursion.
            input_data is an array shape [batch_size,n_step], 
            so want to output c,mu,prec all arrays of same shape [batch_size,n_step]. 
            The marginal lkhd is an array shape [batch_size] """
        T = self.n_step
        phi_v = self.phi_v
        phi_w = self.phi_w        
        prec_v = 1./self.var_v
        prec_w = 1./self.var_w
        logc = np.zeros((self.batch_size,T), dtype = np.float32)
        mu = np.zeros((self.batch_size,T), dtype = np.float32)
        prec = np.zeros((self.batch_size,T), dtype = np.float32)
        
        assert input_data.shape[1] == T, "input_data argument not of the right shape"
        assert T > 1, "n_step must be greater than 1"
        log_marginal_lkhd = 0 # temporary assignment
        for t in xrange(T-2,-2,-1):
            prev_prec = prec[:, t+1]
            prev_mu = mu[:,t+1]
            prev_logc = logc[:,t+1]
            prev_y = input_data[:,t+1]
            f = 0.5 * (prev_prec + phi_w**2*prec_w[t+1] + prec_v)
            g = prev_mu * prev_prec + phi_w * prev_y * prec_w[t+1]
            diff_2f_prec_v = 2*f - prec_v
            if t >= 0:
                if t == (T-1):
                    pdb.set_trace()
                mu[:,t] = g / (phi_v[t+1] * diff_2f_prec_v)
                prec[:,t] = phi_v[t+1]**2 * prec_v * diff_2f_prec_v /(2*f)
                logc[:,t] = prev_logc + 0.5 * np.log(prec_v) + 0.5 * np.log(prec_w[t+1]) - np.log(2*np.sqrt(np.pi * f)) + ( - 0.5 * prev_mu**2 * prev_prec - 0.5 * prev_y**2 * prec_w[t+1] + 0.5* g**2 / diff_2f_prec_v )
            else:
                log_marginal_lkhd =  prev_logc + 0.5 * np.log(prec_v) + 0.5 * np.log(prec_w[t+1]) - np.log(2*np.sqrt(np.pi * f)) + ( - 0.5 * prev_mu**2 * prev_prec - 0.5 * prev_y**2 * prec_w[t+1] + 0.25 * g**2/f )
        return mu, prec, logc, log_marginal_lkhd

    def optimal_proposal(self, t, x_prev, y_t, mu_t, prec_t):
        """ Computes mean and precision of optimal proposal p(x_t|x_{t-1},y_{t:T-1}) 
            x_prev is x_{t-1}, a scalar or array
            y_t is a scalar or array
            mu_t, prec_t are entries or cols of outputs of future_lkhd 
            So output mu_opt, prec_opt can be scalars or arrays depending on input args """
        T = self.n_step
        phi_v = self.phi_v[t]
        phi_w = self.phi_w        
        prec_v = 1./self.var_v
        prec_w = 1./self.var_w
        prec_opt = prec_v + phi_w**2 * prec_w[t] + prec_t
        mu_opt = (phi_v * x_prev * prec_v + phi_w * y_t * prec_w[t] + mu_t * prec_t) / prec_opt
        return mu_opt, prec_opt

    def suboptimal_proposal(self, t, x_prev, y_t):
        """ Computes mean and precision of optimal proposal p(x_t|x_{t-1},y_t) 
            x_prev is x_{t-1}, a scalar or array
            y_t is a scalar or array
            So output mu_subopt, prec_subopt can be scalars or arrays depending on input args """
        if np.isscalar(x_prev):
            zero = 0
        else:
            zero = np.zeros_like(x_prev)
        return self.optimal_proposal(t, x_prev, y_t, zero, zero)        

class ExperPrior(ExperLGSSM):
    def __init__(self):
        super(ExperPrior, self).__init__()
        self.n_iter = 30 # number of gradient updates 
        return


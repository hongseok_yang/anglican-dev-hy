import numpy as np
from scipy.misc import logsumexp
import tensorflow as tf

from util import np_compute_neg_log_normal_pdf
from util import tf_compute_neg_log_normal_pdf
from util import tf_stack_along_col

""" Utility functions. """

def data_type(): 
    return tf.float32

class Oracle(object):

    def __init__(self, sess, exper):
        self._sess = sess # tensorflow session
        self._learning_rate = exper.learning_rate
        self._dnn_sizes = exper.dnn_sizes
        self._batch_size = exper.batch_size
        self._n_iter = exper.n_iter
        self._n_particle = exper.n_particle
        self._n_step = exper.n_step
        self._h_size = exper.h_size
        self._state_dim = exper.state_dim
        self._compute_next_mean = exper.compute_next_mean
        self._var_v = exper.var_v
        return

    def __build_dnn(self, input, num_outputs, base_name):
        dnn_hidden = input
        for d in xrange(len(self._dnn_sizes)): 
            scope_name = base_name + 'Layer' + str(d+1) 
            dnn_hidden = tf.contrib.layers.fully_connected(
                            inputs=dnn_hidden, 
                            num_outputs=self._dnn_sizes[d], 
                            activation_fn=tf.tanh,
                            reuse=True, 
                            scope=scope_name) 
        scope_name = base_name + 'FinalLayer'
        bias_init_value = np.array([0. if i < num_outputs else 5. for i in xrange(num_outputs)])
        bias_init = tf.constant_initializer(bias_init_value)
        dnn_output = tf.contrib.layers.fully_connected(
                        inputs=dnn_hidden, 
                        num_outputs=num_outputs, 
                        activation_fn=None, 
                        biases_initializer = bias_init,
                        reuse=True, 
                        scope=scope_name)
        return dnn_output

    def __build_(self):
        batch_size = self._batch_size
        n_step = self._n_step
        state_dim = self._state_dim
        shape = [batch_size, n_step, 1] # shape of tensor that stores observations
        input_rev = tf.placeholder(shape=shape, dtype=data_type()) # minibatch of observations
        input_rev_reshaped = tf.reshape(input_rev,shape = [batch_size*n_step,1])
        output_dnn_q_rev = self.__build_dnn(input_rev_reshaped, state_dim+1, 'DNNQ') # tensor [batch_size*n_step,state_dim+1]
        output_dnn_h_rev = self.__build_dnn(input_rev_reshaped[batch_size:batch_size*n_step], state_dim+1, 'DNNH') # tensor [batch_size*(n_step-1),state_dim+1]
        
        self._input_rev = input_rev
        self._output_dnn_q_rev = output_dnn_q_rev
        self._output_dnn_h_rev = output_dnn_h_rev
        return

    def __build_loss(self):
        batch_size = self._batch_size
        state_dim = self._state_dim
        h_size = self._h_size
        n_step = self._n_step
        n_particle = self._n_particle
        n_summands_q = batch_size * n_step * n_particle
        n_summands_h = batch_size * (n_step - 1) * n_particle

        """ input_rev_smc0 and input_rev_smc1 placeholders store information about a particular 
            smc run over a minibatch. The seven elements in the array are: 
            (1) x[i,T-1-t] 
            (2) T-1-t
            (3) x[i,T-1-(t-1)] 
            (4) T-1-t
            (5) log_w[i,T-1-t] 
            (6) log_w[i,T-1-(t+1)] 
            (7) log(sum_i (- w[i,T-1-t] * log_w[i,T-1-t])) 
            where T is n_step. """

        n_elem = 2 * state_dim + 5
        input_rev_smc0 = tf.placeholder(shape=[batch_size,n_step,n_particle,n_elem], dtype=data_type())
        input_rev_smc1 = tf.reshape(input_rev_smc0, [n_summands_q,n_elem])

        input_rev_smc2 = input_rev_smc0[:,1:n_step,:,:]
        input_rev_smc3 = tf.reshape(input_rev_smc2, [n_summands_h,n_elem])

        x_q = input_rev_smc1[:,0:state_dim]
        log_w_q = input_rev_smc1[:,(2*state_dim+2)]

        x_h = input_rev_smc3[:,0:state_dim]
        log_w_h = input_rev_smc3[:,(2*state_dim+2)]
        log_w_next_h = input_rev_smc3[:,(2*state_dim+3)]
        log_neg_w_sum_h = input_rev_smc3[:,(2*state_dim+4)]

        output_rev_q0 = self._output_dnn_q_rev
        output_rev_q1 = tf.expand_dims(output_rev_q0, axis =1)
        output_rev_q2 = tf.tile(output_rev_q1, [1,n_particle,1])
        output_rev_q3 = tf.reshape(output_rev_q2, [n_summands_q, state_dim+1])
        output_mean = output_rev_q3[:,0:state_dim]
        output_log_var = output_rev_q3[:,state_dim]

        x_q_prev = input_rev_smc1[:,state_dim+1:2*state_dim+1]
        ts0 = input_rev_smc1[:,state_dim]
        ts = tf_stack_along_col(ts0,state_dim)
        bootstrap_mean = self._compute_next_mean(ts,x_q_prev)
        bootstrap_log_var = tf.log(self._var_v)*tf.ones(shape=output_log_var.shape,dtype=data_type())
        concat_log_var = tf.stack([bootstrap_log_var, output_log_var],axis = 1)
        log_sum_var = tf.reduce_logsumexp(concat_log_var,axis=1)
        log_var = bootstrap_log_var + output_log_var - log_sum_var
        mu = bootstrap_mean * tf_stack_along_col(tf.exp(output_log_var - log_sum_var), state_dim) + output_mean * tf_stack_along_col(tf.exp(bootstrap_log_var - log_sum_var), state_dim)

        output_rev_h0 = self._output_dnn_h_rev
        output_rev_h1 = tf.expand_dims(output_rev_h0, axis =1)
        output_rev_h2 = tf.tile(output_rev_h1, [1,n_particle,1])
        output_rev_h3 = tf.reshape(output_rev_h2, [n_summands_h, state_dim+1])

        h_mu = output_rev_h3[:,0:state_dim]
        h_log_var = output_rev_h3[:,state_dim]

        log_q_term = tf.exp(log_w_q) * tf_compute_neg_log_normal_pdf(state_dim, x_q, mu, log_var)
        h_term = h_size * tf.exp(- tf_compute_neg_log_normal_pdf(state_dim, x_h, h_mu, h_log_var))
        gamma_t_term = (tf.exp(log_w_h) - tf.exp(log_w_next_h)) * h_term 
        gamma_t_next_term = tf.exp(log_w_h) * (log_w_h + tf.exp(log_neg_w_sum_h)) * h_term 

        self._loss_input_rev = input_rev_smc0
        self._loss_output_rev = tf.reduce_sum(log_q_term) + tf.reduce_sum(gamma_t_term + gamma_t_next_term)
        return

    def __build_optimizer(self):
        optimizer = tf.train.AdamOptimizer(self._learning_rate)
        minimize = optimizer.minimize(self._loss_output_rev)
        self._minimize = minimize
        return

    def build_graph(self):
        self.__build_()
        self.__build_loss()
        self.__build_optimizer()
        return

    def feed_data(self, input_data):
        """ Run the set of n_step DNNQ and DNNH's with shared weights on a given input data, 
            and store the output in the internal state of this object. 
            input_data is a numpy array of shape [batch_size,n_step,1], 
            and the output is stored in the _dnn_output_val_q/h field of this object. """
        batch_size = self._batch_size
        n_step = self._n_step
        state_dim = self._state_dim

        input_rev = self._input_rev 
        input_data_rev = np.flip(input_data, axis=1)

        output_rev_q = self._output_dnn_q_rev
        output_data_rev_q = self._sess.run(output_rev_q, feed_dict={input_rev: input_data_rev}) #[batch_size*n_step,state_dim+1]
        output_data_rev_q_reshaped = np.reshape(output_data_rev_q, (batch_size, n_step, state_dim + 1))
        output_data_q = np.flip(output_data_rev_q_reshaped, axis=1)
        self._dnn_output_val_q = output_data_q

        output_rev_h = self._output_dnn_h_rev
        output_data_rev_h = self._sess.run(output_rev_h, feed_dict={input_rev: input_data_rev}) #[batch_size*(n_step-1),state_dim+1]
        output_data_rev_h_reshaped = np.reshape(output_data_rev_h, (batch_size, n_step-1, state_dim + 1))
        output_data_h = np.flip(output_data_rev_h_reshaped, axis=1)
        self._dnn_output_val_h = output_data_h
        return 

    def __sample(self, t, x_prev):
        state_dim = self._state_dim

        # Run the dnn for q to get parameters of the proposal distribution.
        dnn_output = self._dnn_output_val_q[:,t,:] # array shape [batch_size,state_dim+1]
        output_mean = dnn_output[:,0:state_dim]
        output_log_var = dnn_output[:,state_dim]
        output_log_var = np.expand_dims(output_log_var, axis = 1)

        bootstrap_mean = self._compute_next_mean(t,x_prev)
        bootstrap_log_var = np.log(self._var_v)*np.ones(shape=output_log_var.shape)
        log_sum_var = np.logaddexp(bootstrap_log_var,output_log_var)
        log_var = bootstrap_log_var + output_log_var - log_sum_var
        mean = bootstrap_mean * np.tile(np.exp(output_log_var - log_sum_var),state_dim) + output_mean*np.tile(np.exp(bootstrap_log_var - log_sum_var),state_dim)
   
        # Generate a minibatch of samples from the proposal distribution.
        sample = np.zeros(shape=[self._batch_size,state_dim])
        for b in xrange(self._batch_size):
            for d in xrange(self._state_dim): 
                sample[b,d] = np.random.normal(mean[b,d], np.sqrt(np.exp(log_var[b,0])))
        log_pdf = - np_compute_neg_log_normal_pdf(state_dim, sample, mean, log_var[:,0])
        return sample, log_pdf, mean, np.squeeze(log_var, axis = (1,))

    def sample_next(self, t, x_prev):
        """ Sample from a learnt proposal distribution. 
            x_prev is a numpy array of shape [batch_size,state_dim], and stores the 
            states in the previous step. t is the current step. The 
            results are two numpy arrays of shape [batch_size,state_dim] and [batch_size,1]. """
        return self.__sample(t, x_prev)
        
    def sample_init(self):
        """ Sample for the 0-th step using a learnt proposal distribution. 
            The results are numpy arrays of shape [batch_size,state_dim] and [batch_size,1]. """
        x_prev = np.zeros(shape=[self._batch_size, self._state_dim])
        return self.__sample(0, x_prev)

    def compute_h_diff_next(self, x_prev, x, t):
        """ Compute h_(t+1)(x) - h_t(x_prev). x_prev and x are numpy arrays 
            of shape [batch_size,state_dim], and stores the states in the previous 
            and the current steps. t is the current step. The 
            result is a numpy array of shape [batch_size]. """
        batch_size = self._batch_size
        n_step = self._n_step
        state_dim = self._state_dim
        h_size = self._h_size

        if t > 0:
            dnn_output1 = self._dnn_output_val_h[:,t-1,:]
            h_mean1 = dnn_output1[:,0:state_dim]
            h_log_var1 = dnn_output1[:,state_dim]
            log_pdf1 = - np_compute_neg_log_normal_pdf(state_dim, x_prev, h_mean1, h_log_var1)
            h1 = h_size * np.exp(log_pdf1)
        else:
            h1 = 0

        if (t < n_step-1):
            dnn_output2 = self._dnn_output_val_h[:,t,:]
            h_mean2 = dnn_output2[:,0:state_dim]
            h_log_var2 = dnn_output2[:,state_dim]
            log_pdf2 = - np_compute_neg_log_normal_pdf(state_dim, x, h_mean2, h_log_var2)
            h2 = h_size * np.exp(log_pdf2)
        else:
            h2 = 0
        return h2-h1

    def compute_h_diff_init(self, x):
        x_prev = np.zeros(shape=[self._batch_size,self._state_dim])
        return self.compute_h_diff_next(x_prev, x, 0)

    def __generate_input_smc_rev(self, rollout, weight, ancestry):
        batch_size = self._batch_size
        n_step = self._n_step
        n_particle = self._n_particle
        state_dim = self._state_dim
        shape = [batch_size,n_step,n_particle,2*state_dim+5]

        input_smc = np.zeros(shape=shape)
        input_smc[:,:,:,0:state_dim] = rollout[:,:,:,:]
        for b in xrange(batch_size):
            for t in xrange(1,n_step):
                for i in xrange(n_particle): 
                    j = ancestry[b,t-1,i]
                    input_smc[b,t,i,(state_dim+1):(2*state_dim+1)] = rollout[b,t-1,j,:]
        for t in xrange(n_step): 
            input_smc[:,t,:,state_dim] = t 
            input_smc[:,t,:,2*state_dim+1] = t
        input_smc[:,:,:,2*state_dim+2] = weight[:,:,:,0]
        input_smc[:,0:(n_step-1),:,2*state_dim+3] = weight[:,1:n_step,:,0]

        f = np.vectorize(lambda w: w + np.log(-w))
        weight_entropy = f(weight)
        weight_entropy_sum = np.zeros(shape=[batch_size,n_step])
        for b in xrange(batch_size):
            for t in xrange(n_step):
                weight_entropy_sum[b,t] = logsumexp(weight_entropy[b,t,:,0])
        for i in xrange(n_particle): 
            input_smc[:,:,i,2*state_dim+4] = weight_entropy_sum[:,:]
        return np.flip(input_smc, axis=1)

    def __generate_input_rev(self, input_data):
        input_data_rev = np.flip(input_data, axis=1)
        return input_data_rev

    def train(self, iter_num, rollout, weight, input_data, ancestry):
        weight_expanded = np.expand_dims(weight, axis=3)
        input_smc_rev = self.__generate_input_smc_rev(rollout,weight_expanded,ancestry)
        input_data_rev = self.__generate_input_rev(input_data)

        loss_input_rev = self._loss_input_rev
        input_rev = self._input_rev
        feed_dict = {input_rev: input_data_rev, loss_input_rev: input_smc_rev}

        minimize = self._minimize 
        self._sess.run(minimize, feed_dict=feed_dict) 
        loss_val = self._sess.run(self._loss_output_rev, feed_dict=feed_dict) 
        print "(Iter, ProxyLoss): (%d, %.3f)" % (iter_num, loss_val)
        return

    def estimate_objective(self, weight):
        batch_size = self._batch_size
        n_particle = self._n_particle

        logsumexp_neg = logsumexp(weight[:,:,:] + np.log(- weight[:,:,:]),axis = 2)
        logsumexp_neg = logsumexp(logsumexp_neg, axis = 0)
        logsumexp_neg_normalised = logsumexp_neg - np.log(n_particle) - np.log(batch_size)

        return -np.exp(logsumexp_neg_normalised)

import tensorflow as tf
import ssm
#import linear_ssm as ssm
import numpy as np
import time
from sklearn import preprocessing

from tensorflow.python import debug as tf_debug # need for tensorflow debugger tfdbg

### Configuration parameters for Tensorflow
flags = tf.flags
flags.DEFINE_bool("use_fp16", False,
                  "Train using 16-bit floats instead of 32bit floats")
FLAGS = flags.FLAGS


### Utility functions
def data_type():
  return tf.float16 if FLAGS.use_fp16 else tf.float32

def replace_none_with_zero(l):
    return [0 if i==None else i for i in l]

### generates summaries for tf graph
def variable_summaries(var, name):
    """Attach a lot of summaries to a Tensor."""
    with tf.name_scope('summaries'): # name_scope used to visualise variables
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean/' + name, mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('sttdev/' + name, stddev)
        tf.summary.scalar('max/' + name, tf.reduce_max(var))
        tf.summary.scalar('min/' + name, tf.reduce_min(var))
        #tf.summary.histogram(name, var)

### Configuration parameters for model used in experiment
class Config:
    lstm_hidden_size = 100 # num_units of h,c and output of lstm
    max_grad_norm = 5
    learning_rate = 0.001 # default for adam
    num_iter = 100 # number of gradient updates
    n_input = 100 # length of input sequence T. Need to be > 1.
    n_particles = 100 # number of particles in smc
    minibatch_size = 50 
    dnn_layer_sizes = [100,100,100] # list of layer sizes for dnn
    resample = False 

class smcmtModel(object):
    """ SMC with moving targets model. 
        Parameterises the intermediate proposals and targets using LSTM with DNN on top.
        Only generate sequences of small length st we can use full rollout
        This is a basic model with no resampling."""

    def __create_instance(self, name, ind, obj):
        """ Create instance "self.nameind" and bind it to obj 
            type(name): str
            type(ind): int
            type(obj): any """
        exec("self." + name + str(ind) + " = obj")
        return

    def get_instance(self, name, ind):
        """ Return instance "self.nameind" """
        exec("instance = self." + name + str(ind))
        return instance 
    
    def get_dict(self, name, ind, val):
        """ Return dict {self.nameind: val} """
        exec("dictionary = {self." + name + str(ind) + ": val}")
        return dictionary

    def run_lstm(self, input_tensor):
        """ Define and run LSTM on given input data and return outuput as a list of length self.T """        
        
        # define LSTM of length = self.lstm_size
        lstm = tf.contrib.rnn.BasicLSTMCell(num_units = self.lstm_size)
        # alternatively, can use LSTM with dropout: lstm = tf.contrib.rnn.DropoutWrapper(lstm,output_keep_prob = config.keep_prob)
        # and/or stacked LSTMs: lstm = tf.contrib.rnn.MultiRNNCell([lstm] * config.num_layers)

        # LSTM memory initial state. Tensor with zeros of shape [m,self.lstm_size]
        self.initial_state = lstm.zero_state(m,dtype=data_type()) 

        # transform tensor into list of T tensors of shape [m,1]
        input_data = tf.unstack(tf.expand_dims(input_tensor,axis=2),axis=1)
        input_data.reverse() # reverse data as we want a backwards LSTM

        # generate outputs,  a list of T tensors of shape [m,self.lstm_size]
        outputs, _ = tf.contrib.legacy_seq2seq.rnn_decoder(input_data,self.initial_state,lstm, scope = "LSTM") 
        outputs.reverse() # reverse outputs so that we recover the correct indexing
        return outputs

    def __run_dnn(self):
        """ Generate mu_t, log_var_t, h_{t-1} using DNN with inputs: self.outputs[t] and x_{t-1}
            Put placeholder (tensor shape [m,n]) on each x_t for t=0,...,T-1 
            For x, need to create t different variables so that we only need to feed in 
            the t'th x variable to evaluate mu_t,log_var_t,h_{t-1}.
            Can't use list for x since the feed argument for partial_run_setup needs to be
            a list of graph elements and can't be a list of lists/dictionaries. """
        T = self.T; m = self.m; n = self.n        
        for t in xrange(1,T):
            # bring in x_{t-1} from outside tf using placeholder
            self.__create_instance("x",t-1,tf.placeholder(dtype = data_type(), shape=[self.m,self.n]))
 
            # concatenate x_{t-1} and outputs_t to form the input to dnn
            # note we want dnn to map [x_{t-1}[:,i],outputs[t]] to [mu_t[:,i],log_var_t[:,i],h_{t-1}[:,i]]. 
            # So first reshape x_{t-1}, treating it as a minibatch size m*n.
            x_input = tf.reshape(self.get_instance("x",t-1), shape = [m*n,1])
            # Also tile n copies of outputs[t] 
            lstm_output = tf.tile(self.outputs[t],[n,1]) # tensor shape [m*n,lstm.size]
            
            ## initialise dnn inputs
            dnn_hidden = tf.concat([x_input,lstm_output],axis = 1) # tensor shape [m*n, 1+lstm.size]

            ## recursively apply fully_connected relu layer to dnn_hiden
            for i in xrange(len(self.dnn_size)):
                dnn_hidden = tf.contrib.layers.fully_connected(dnn_hidden, num_outputs = self.dnn_size[i], reuse = True, scope = ("DNNlayer"+str(i+1)), activation_fn = tf.tanh)
                #dnn_hidden = tf.contrib.layers.batch_norm(dnn_hidden, center = True, scale = True, is_training = True, scope = ("BNlayer"+str(i+1)))
                #dnn_hidden = tf.sigmoid(dnn_hidden)

            ## obtain final dnn output
            dnn_output = tf.contrib.layers.fully_connected(inputs = dnn_hidden, num_outputs = 3, activation_fn = None, reuse = True, scope = "DNNfinalLayer")
            #biases_initializer =  tf.constant_initializer(value = 5))               
            
            # assign self.mu*, self.log_var*, self.h* to dnn_output
            self.__create_instance("mu",t,tf.reshape(dnn_output[:,0],[m,n])) # tensor shape [m,n]                
            self.__create_instance("log_var",t,tf.reshape(dnn_output[:,1],[m,n])) # tensor shape [m,n]        
            self.__create_instance("h",t-1,tf.reshape(dnn_output[:,2],[m,n])) # tensor shape [m,n]

        # assign self.x(T-1) to be a placeholder. Need this to compute weights later
        self.__create_instance("x",T-1,tf.placeholder(dtype = data_type(), shape=[m,n]))
        return

    def __form_tensor(self):
        """ for tensors x, mu, log_var, h, log q from self.x*, self.mu*, self.log_var*,self.h*
        each of shape [m,n,T].
        Note q[k,i,t] = Normal(mu[k,i,t],exp(log_var[k,i,t])).pdf(x[k,i,t]) """
        T = self.T; m = self.m; n = self.n

        x = tf.stack([self.get_instance("x",t) for t in xrange(T)], axis = -1)
        mu = tf.stack([self.get_instance("mu",t) if t > 0 else tf.zeros([m,n], dtype = data_type()) for t in xrange(T)], axis = -1)
        log_var = tf.stack([self.get_instance("log_var",t) if t > 0 else tf.log(5.0)*tf.ones([m,n], dtype = data_type()) for t in xrange(T)], axis = -1)
        h = tf.stack([self.get_instance("h",t) if t < T-1 else tf.zeros([m,n], dtype = data_type()) for t in xrange(T)], axis = -1)
        var = tf.exp(log_var)
        log_q = -0.5 * tf.log(2 * np.pi * var) - (x-mu)**2/(2*var)
        variable_summaries(x,"x")
        variable_summaries(mu,"mu")
        variable_summaries(log_var,"log_var")
        variable_summaries(h,"h")
        variable_summaries(log_q,"log_q")
        return [x,mu,log_var,h,log_q]

    def __compute_log_norm_weights(self, x, h, log_q):
        """ Compute log normalised weights from inputs, x, h, log_q """    
        T = self.T; m = self.m; n = self.n
        self.input_data_stack = tf.stack([self.input for i in xrange(n)], axis = 1) # replicate self.input (tensor shape [m,T]) and stack to form tensor shape [m,n,T]. 
        # Not sure if this is the most efficient way. tf.tile(tf.exapnd_dims(self.input,axis = 1),[1,n,1]) could be better.
        
        # initialise list
        log_weights = [None]*T 
        for t in xrange(T):
            #log_weights[t] = ssm.lin_gauss_pdf(x[:,:,t],x[:,:,t-1],var_v,phi_v) + ssm.lin_gauss_pdf(self.input_data_stack[:,:,t],x[:,:,t],var_w,phi_w) + h[:,:,t] - h[:,:,t-1] - log_q[:,:,t]
            log_weights[t] = ssm.log_trans(x[:,:,t],t,x[:,:,t-1],var_v) + ssm.log_lkhd(self.input_data_stack[:,:,t],x[:,:,t],var_w) + h[:,:,t] - h[:,:,t-1] - log_q[:,:,t] # note this is fine even for t=0 since log_trans(x_t,t,x,var_v)=log_trans(x_t,t) for t=0 and h[:,:,t-1] = h[:,:,-1]=zeros([m,n])
        log_weights = tf.stack(log_weights, axis = 2) # convert list to tensor shape [m,n,T]        
        variable_summaries(log_weights,"log_weights")
        log_sum_weights = tf.reduce_logsumexp(log_weights, axis = 1) # log of weight sums across i=0,...,n-1
        return log_weights - tf.stack([log_sum_weights for i in range(n)], axis = 1) # tensor shape [m,n,T]. tf.tile(tf.expand_dims(log_sum_weights,axis = 2),[1,1,T]) could be better.

    def compute_average_ess(self, log_norm_weight):
        """ Compute average ESS across all batches and time steps """
        T = self.T; m = self.m; n = self.n
        log_ess_tb = -tf.reduce_logsumexp(log_norm_weight * 2, axis = 1)
        log_ess = tf.reduce_logsumexp(log_ess_tb) - np.log(m) - np.log(T)
        return tf.exp(log_ess)
    
    def __compute_loss(self, h, log_q):
        """ Construct objective that gives the gradients we want """
        T = self.T; m = self.m; n = self.n
        self.log_W = tf.placeholder(dtype = data_type(), shape=[m,n,T]) # log normalised weights
        log_W = self.log_W
        log_W_shift_erase = tf.concat([tf.slice(log_W,[0,0,1],[m,n,T-1]),tf.expand_dims(tf.zeros([m,n]),axis=2)], axis = 2) # log_W_shift_erase = [log_W[:,:,1:T-1],zeros[m,n]]
        W = tf.exp(log_W)
        return tf.reduce_sum( W * ( - log_q + h*(1+log_W) ) - tf.exp(log_W_shift_erase)*h ) - tf.reduce_sum( tf.reduce_sum(W*h,1) * tf.reduce_sum(W*log_W,1) )

    def compute_KL(self, h, log_q, x):
        """ Compute [avg(KL(gamma_t || gamma_{t-1}*q_t)) for t in range(T)] and its sum
            where avg is taken across mini-batch """
        log_W = self.log_normalised_weights
        W = tf.exp(log_W)
        kl_list = [tf.reduce_mean(W[:,:,t]*log_W[:,:,t]) for t in xrange(self.T)]
        return kl_list

    def __optimize(self, max_grad_norm):    
        tvars = tf.trainable_variables()
        grads, _ = tf.clip_by_global_norm(tf.gradients(self.loss, tvars), max_grad_norm)
        optimizer = tf.train.AdamOptimizer(config.learning_rate)
        gradients = zip(grads, tvars)
        return optimizer.apply_gradients(gradients)

    def __init__(self, config):
        # config consists of model & optimisation hyperparameters

        """ Define model hyperparameters """
        self.lstm_size = config.lstm_hidden_size 
        self.dnn_size = config.dnn_layer_sizes 
        self.n = n =config.n_particles
        self.T = T = config.n_input
        self.m = m = config.minibatch_size
        self.input = tf.placeholder(dtype = data_type(),shape=[m,T]) # y^{0:m-1}_{0:T-1}
        self.resample = config.resample

        """ Define LSTM and generate its outputs """        
        self.outputs = self.run_lstm(self.input)
        
        """ Define and run DNN that maps particles and LSTM outputs 
            to parameters used to generate particles at next time step """
        self.__run_dnn() # creates instances self.x* (placeholders), self.mu*, self.log_var*,self.h* (tensors) as side effect
        
        """ Form tensors x, mu, log_var, h, log_q from the instances created above """
        x, mu, log_var, h, log_q = self.__form_tensor()

        """ Compute weights and normalise """
        self.log_normalised_weights = self.__compute_log_norm_weights(x, h, log_q)
        variable_summaries(self.log_normalised_weights,"log_normalised_weights")
        
        """ Compute average ESS """
        self.ess = self.compute_average_ess(self.log_normalised_weights)        

        """ Compute list of KL """
        self.kl_list = self.compute_KL(h, log_q, x)
        
        """ Construct loss for optimiser """
        self.loss = self.__compute_loss(h, log_q)

            
        # apply optimiser to gradients
        self.train_op = self.__optimize(config.max_grad_norm)

        # merge summaries
        self.merged = tf.summary.merge_all()

        with tf.control_dependencies([self.train_op, self.merged]):
            self.dummy = tf.constant(0) # use this as fetch argument for partial_run instead of self.train_op (c.f. https://github.com/tensorflow/tensorflow/issues/1899)
        
        """
        # compute gradients of mu,log_var,h using list comprehension. Above is more efficient. Left in case we want to check gradients.
        # grad_mu,grad_log_var, grad_h are all tensors of shape [m,n,T]
        grad_mu = [[[tf.clip_by_global_norm(tf.gradients(self.mu[t][k,i],tvars), config.max_grad_norm)[0] if t > 0 else tf.constant(0) for t in xrange(T)] for i in xrange(n)] for k in xrange(m)]
        grad_mu = tf.convert_to_tensor(grad_mu)
        grad_log_var = [[[tf.clip_by_global_norm(tf.gradients(self.log_var[t][k,i],tvars), config.max_grad_norm)[0] if t > 0 else tf.constant(0) for t in xrange(T)] for i in xrange(n)] for k in xrange(m)]
        grad_log_var = tf.convert_to_tensor(grad_log_var)
        grad_h = [[[tf.clip_by_global_norm(tf.gradients(self.h[t][k,i],tvars), config.max_grad_norm)[0] if t < T-1 else tf.constant(0) for t in xrange(T)] for i in xrange(n)] for k in xrange(m)]
        grad_h = tf.convert_to_tensor(grad_h)
        """

def run(session, model, data, eval_op = None, summary = False):
    """ Runs model on given data using session. Trains parameters of model if eval_op=model.train_op
        Otherwise do all computations with fixed parameters. """
    start_time = time.time()
    assert data.shape[0] == model.m
    m = model.m; n = model.n; T = model.T
    
    """ use partial_run to compute x's, weights, loss and gradient updates """
    # define fetches and feeds for partial_run_step
    fetches = [model.outputs] +  [[model.get_instance("mu", t),model.get_instance("log_var", t), model.get_instance("h", t-1)] for t in xrange(1,T)] + [model.log_normalised_weights, model.ess, model.kl_list, model.loss]
    feeds = [model.input] + [model.get_instance("x", t) for t in xrange(T)]
    
    # train parameters or fix parameters depending on value of eval_op
    if eval_op is not None:
        fetches.append(model.dummy)
        feeds.append(model.log_W)  
    if summary:
        fetches.append(model.merged)      

    # setup partial_run    
    setup = session.partial_run_setup(fetches, feeds)

    # get outputs of LSTM
    _ = session.partial_run(setup, model.outputs, feed_dict={model.input: data}) 

    # generate rollout
    rollout = np.zeros(shape=[m,n,T], dtype = np.float32)
    x = ssm.sample_q(0)*np.ones((m,n), dtype=np.float32) # x_0
    for t in xrange(1,T):
        # resample
        #if model.resample:
            ### to resample, need to keep track of weights.
        mu, log_var, h = session.partial_run(setup, [model.get_instance("mu", t), model.get_instance("log_var", t), model.get_instance("h", t-1)], feed_dict = model.get_dict("x", t-1, x))
        x = np.random.normal(mu, np.exp(0.5*log_var)+1e-6)
    
    # compute normalised_weights from the generated x's    
    log_normalised_weights = session.partial_run(setup, model.log_normalised_weights, feed_dict = model.get_dict("x", T-1, x))

    # compute average ESS and average KL
    ess_avg, kl_list = session.partial_run(setup, [model.ess,model.kl_list])

    # compute Kl list

    # output loss and train parameters
    if eval_op is not None:
        loss, _ = session.partial_run(setup, [model.loss, model.dummy], feed_dict = {model.log_W: log_normalised_weights})

    end_time = time.time()
    iter_time = (end_time - start_time)/m # computation time per mini-batch

    # return summary if summary=True
    if summary:
        my_summary = session.partial_run(setup, model.merged)
        return loss, ess_avg, kl_list, iter_time, my_summary
    else:
        return loss, ess_avg, kl_list, iter_time
        
""" main """
# define model hyp    
config = Config()

# generate data
var_v = 10 # variance for Gaussian transition
var_w = 1 # variance for Gaussian likelihood
phi_v = phi_w = 1 # coefficient for mean of Gaussian transition and likelihood
summary = False # generate summary if True
T = config.n_input
m = config.minibatch_size
#y = preprocessing.scale(y) # centre and whiten data

# run_epoch
tfconfig = tf.ConfigProto()
tfconfig.gpu_options.visible_device_list = "" # just use /gpu:0
with tf.Session(config=tfconfig) as sess:
    model = smcmtModel(config) # initialise model and variables
    print([var.name for var in tf.trainable_variables()])
    if summary:    
        train_writer = tf.summary.FileWriter('/homes/hkim/clojure/anglican-dev-hy/src/python/summaries',sess.graph)
    #sess = tf_debug.LocalCLIDebugWrapperSession(sess)
    #sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)
    sess.run(tf.global_variables_initializer()) # initialise variables 
    for iter in range(config.num_iter):
        #y = np.array([ssm.generate(var_v,var_w,phi_v,phi_w,T)[1] for i in xrange(m)]) # array shape [m,T]        
        y = np.array([ssm.generate(var_v,var_w,T)[1] for i in xrange(m)]) # array shape [m,T]
        if summary:        
            loss, ess_avg, kl_list, iter_time, summary = run(sess, model, y, eval_op = model.train_op, summary = summary)
            train_writer.add_summary(summary,iter)
        else:
            loss, ess_avg, kl_list, iter_time = run(sess, model, y, eval_op = model.train_op, summary = summary) 
        print("Iter: %d " % iter),
        print("Time per sample obs: %.3f sec " % iter_time),
        print("Loss: %.3f " % loss),
        print("Average ESS: %.1f " % ess_avg),
        print("KL_sum: %.3f " % sum(kl_list))
        #print("KL_list:")
        #print(kl_list)


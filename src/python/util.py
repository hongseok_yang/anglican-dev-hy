import numpy as np
#%matplotlib inline
import matplotlib
import matplotlib.pyplot as plt
import random
from scipy.stats import norm
from scipy.misc import logsumexp
import sys
import tensorflow as tf
from time import time

plt.switch_backend('agg')
plt.style.use('ggplot')

import ssm

""" Utility Functions. """
# doublechecked for HD case
def np_compute_neg_log_normal_pdf(state_dim, x, mean, log_var): 
    """ Compute the log pdf of a normal distribution. 
        Input type:
        (1) x: array[a,state_dim]
        (2) mean: array[a,state_dim]
        (3) log_var: array[a]
        Output type: array[a]. """ 
    mean_term = np.sum((x - mean)**2, axis=1) / (2. * np.exp(log_var)) 
    normalisation_term = state_dim * 0.5 * (log_var + np.log(2 * np.pi))  
    return normalisation_term + mean_term

# doublechecked for HD case
def tf_compute_neg_log_normal_pdf(state_dim, x, mean, log_var): 
    """ Construct a tensorflow node that computes the log pdf of a normal 
        distribution.
        Input type:
        (1) x: tensor[a,state_dim]
        (2) mean: tensor[a,state_dim]
        (3) log_var: tensor[a]
        Output type: tensor[a]. """ 
    mean_term = tf.reduce_sum((x - mean)**2, axis=1) / (2. * tf.exp(log_var)) 
    normalisation_term = state_dim * 0.5 * (log_var + tf.log(2 * np.pi))  
    return normalisation_term + mean_term

def tf_stack_along_col(x, num_rep):
    """ Stack num_rep copies of 1d tensor x along cols to create 2d tensor.
        Input type:
        (1) x: tensor[a]
        (2) num_rep: scalar
        Output type: tensor[a,num_rep]. """
    x1 = tf.expand_dims(x, axis = 1)
    x2 = tf.tile(x1, [1,num_rep])
    return x2

def tf_multiply_gaussian(mu1, logvar1, mu2, logvar2):
    """ Output the mean and variance of the product of two gaussian pdfs
        Input type:
        (1) mu1: mean of pdf1. array[a,state_dim]
        (2) logvar1: log variance of pdf1. array[a]
        (3) mu2: mean of pdf2. array[a,state_dim]
        (4) logvar2: log variance of pdf2. array[a]
        Output type:        
        (1) mu: mean of pdf1*pdf2. array[a,state_dim]
        (2) logvar: log variance of pdf1*pdf2. array[a]. """
    log_sum_var = tf.reduce_logsumexp(tf.stack([logvar1,logvar2], axis=-1), axis=-1)
    logvar = logvar1 + logvar2 - log_sum_var
    mu = mu1 * tf.expand_dims(tf.exp(logvar - logvar1),-1) + mu2 * tf.expand_dims(tf.exp(logvar - logvar2),-1) # uses broadcasting
    return mu, logvar

def multiply_gaussian(mu1, logvar1, mu2, logvar2):
    """ output the mean and variance of the product of two gaussian pdfs
        Input type:
        (1) mu1: mean of pdf1. array[a,state_dim]
        (2) logvar1: log variance of pdf1. array[a]
        (3) mu2: mean of pdf2. array[a,state_dim]
        (4) logvar2: log variance of pdf2. array[a]
        Output type:        
        (1) mu: mean of pdf1*pdf2. array[a,state_dim]
        (2) logvar: log variance of pdf1*pdf2. array[a]
    """
    log_sum_var = logsumexp(np.stack([logvar1,logvar2], axis=-1), axis=-1)
    logvar = logvar1 + logvar2 - log_sum_var
    mu = mu1 * np.expand_dims(np.exp(logvar - logvar1),-1) + mu2 * np.expand_dims(np.exp(logvar - logvar2),-1) # uses broadcasting
    return mu, logvar

def normalise(input, mean, std):
    """ Normalise input with mean and std 
        Input type:
        (1) input: [batch_size,n_step,a]    
        (2) mean: [n_step,a]
        (3) std: [n_step,a]
        Output type: [batch_size,n_step,a]
    """
    norm_input = input - np.expand_dims(mean, axis=0) / np.expand_dims(std, axis=0) # uses broadcasting
    return norm_input

def denormalise(input, mean, std):
    """ Denormalise input that was normalised with mean and std 
        Input type:
        (1) input: [batch_size,n_step,a]    
        (2) mean: [n_step,a]
        (3) std: [n_step,a]
        Output type: [batch_size,n_step,a]
    """
    denorm_input = input * np.expand_dims(std, axis=0) + np.expand_dims(mean, axis=0) # uses broadcasting
    return denorm_input

def tf_denormalise(input, mean, std):
    """ Denormalise input that was normalised with mean and std 
        Input type:
        (1) input: [batch_size,n_step,a]    
        (2) mean: [n_step,a]
        (3) std: [n_step,a]
        Output type: [batch_size,n_step,a]
    """
    denorm_input = input * tf.expand_dims(std, axis=0) + tf.expand_dims(mean, axis=0) # uses broadcasting
    return denorm_input

def compute_average_ess(weight):
    """ Compute ESS averaged across batch.
        Input type: array[batch_size,n_step,n_particle]
        Output type: array[n_step]. """ 
    shape = weight.shape
    batch_size = shape[0]
    n_step = shape[1]
    log_ess = -logsumexp(2*weight, axis=2) # array[batch_size,n_step]
    log_ess = logsumexp(log_ess, axis=0) - np.log(batch_size) # log mean_ess over batch. array[n_step] 
    return np.exp(log_ess)

def compute_average_lml(marginal):
    """ Compute log mean marginal likelihood estimate, 
        where mean is taken over batch. 
        Input type: array[batch_size]
        Output type: scalar. """
    batch_size = marginal.shape
    log_mean_lml = logsumexp(marginal) - np.log(batch_size)
    return log_mean_lml

def compute_average_latent_rmse(rollout, weight, ancestry, array_xs):
    """ Compute RMSE (mean taken over time) of sum_i w^i_t * x_t for predicting 
        samples x*_t taken from the model. Then RMSE is averaged over batch.
        Input type:
        (1) rollout: array[batch_size,n_step,n_particle,state_dim]
        (2) weight: array[batch_size,n_step,n_particle]
        (3) ancestry: array[batch_size,n_step,n_particle]
        (4) array_xs: array[batch_size,n_step,state_dim]
        Output type: scalar
    """
    batch_size, n_step, n_particle, state_dim = rollout.shape
    norm_weight = np.exp(weight)
    extended_weight = np.expand_dims(norm_weight, axis=3)
    tiled_weight = np.tile(extended_weight, (1,1,1,state_dim))
    weighed_rollout = rollout * tiled_weight
    post_mean = np.sum(weighed_rollout, axis=2) 
    se = np.sum((array_xs-post_mean)**2, axis=2)
    mse = np.mean(se, axis=1) # mean across time
    rmse = np.sqrt(mse)
    mean_rmse = np.mean(rmse) # mean across batch
    return mean_rmse

def compute_path_diversity(ancestry):
    """ Compute number of offspring of a given particle.
        Input type: array[batch_size,n_step,n_particle]
        Output type: array[batch_size,n_step,n_particle] """
    batch_size, n_step, n_particle = ancestry.shape
    count = np.zeros(shape=ancestry.shape, dtype=int) 
    for b in range(batch_size):
        for i in range(n_particle):
            count[b,n_step-1,ancestry[b,n_step-1,i]] += 1
    for t in range(n_step-2,-1,-1): 
        for b in range(batch_size): 
            for i in range(n_particle):
                j = ancestry[b,t,i]
                count[b,t,j] += count[b,t+1,i]
    return count

# doublechecked for HD case
def compute_ce(oracle, fixed_xs):
    """ Compute MC estimate of E_p(y_(0:T-1)) [CE(p(x_(0:T-1)|y_(0:T-1))||q(x_(0:T-1)))]
        with the fixed samples.
        i.e. -(1/n_particle) * sum_i log q(x^i_(0:T-1))
        where  x^i_(0:T-1), y^i_(0:T-1) ~ p(x_(0:T-1),y_(0:T-1)).
        Output type: 
        (1) estimate: estimate of the above. scalar.
        (2) ce_estimate_array: -(1/n_particle) * sum_i log q(x^i_t | x^i_(t-1)) for t=0:T-1. array[n_step]"""
    batch_size, n_step, state_dim = fixed_xs.shape
    estimate = 0
    ce_estimate_array = []
    for t in range(n_step):
        if t == 0:
            _, _, q_mean, q_log_var, _, _ = oracle.sample_init()
        else:
            _, _, q_mean, q_log_var, _, _ = oracle.sample_next(t,fixed_xs[:,t-1,:])
        neg_log_density = np_compute_neg_log_normal_pdf(state_dim, fixed_xs[:,t,:], q_mean, q_log_var)
        neg_log_q_t = np.sum(neg_log_density)/ batch_size
        ce_estimate_array.append(neg_log_q_t)
    ce_estimate_array = np.array(ce_estimate_array)
    estimate = np.sum(ce_estimate_array)
    return estimate, ce_estimate_array

def compute_degree_degeneracy(ancestry):
    (batch_size, n_step, n_particle) = ancestry.shape
    count = compute_path_diversity(ancestry)
    n_fst_deg = float(np.count_nonzero(count[:,0,:])) / batch_size
    n_mid_deg = float(np.count_nonzero(count[:,n_step/2,:])) / batch_size
    n_half_deg = 2 * float(np.count_nonzero(count[:,0:(n_step/2),:])) / (batch_size * n_step)
    return (n_fst_deg, n_mid_deg, n_half_deg)

def compute_h_effect(oracle, fixed_xs):
    """ Compute standard deviation of q before and after incorporating h with the fixed samples.
        Input type:
        (1) oracle: oracle object
        (2) fixed_xs: array[batch_size,n_step,state_dim]
        Output type: 
        (1) std_no_h: array[batch_size,n_step]
        (2) std_h: array[batch_size,n_step]"""
    batch_size, n_step, state_dim = fixed_xs.shape
    std_no_h = np.zeros(shape=[batch_size, n_step])
    std_h = np.zeros(shape=[batch_size, n_step])
    for t in range(n_step):
        if t == 0:
            _, _, _, q_log_var, _, temp_log_var = oracle.sample_init()
        else:
            _, _, _, q_log_var, _, temp_log_var = oracle.sample_next(t,fixed_xs[:,t-1,:])
        std_no_h[:,t] = np.exp(0.5*temp_log_var)
        std_h[:,t] = np.exp(0.5*q_log_var)
    return std_no_h, std_h    

def print_result(oracle, algo, rollout, weight, marginal, marginal_acc, ancestry, array_xs): 
    """ Print summary of diagnostics at given iteration """
    batch_size, n_step, n_particle, state_dim = rollout.shape
    objective_t = oracle.estimate_objective(weight)
    objective = np.sum(objective_t)
    average_ess_t = compute_average_ess(weight)
    average_ess = np.sum(average_ess_t) / n_step
    average_lml = compute_average_lml(marginal_acc)
    ce_estimate, ce_estimate_t = compute_ce(oracle,array_xs)
    average_rmse = compute_average_latent_rmse(rollout, weight, ancestry, array_xs)
    (n_fst_deg, n_mid_deg, n_half_deg) = compute_degree_degeneracy(ancestry)
    std_no_h_t, std_h_t = compute_h_effect(oracle,array_xs)
    print("Estimated Objective (Dodgy): %f"
          % objective)
    print("Effective Sample Size Averaged Over Time: %.2f out of %d"
          % (average_ess, algo.get_n_particle()))
    print("Effective Sample Size at Time 0: %.2f out of %d"
          % (average_ess_t[0], algo.get_n_particle())) 
    print("Min Effective Sample Size over Time: %.2f out of %d"
          % (min(average_ess_t), algo.get_n_particle()))
    print("Estimated log mean marginal likelihood across batch: %f"
          % average_lml)
    #print("Breakdown of log marginal for first batch across first five time stamps: (%f, %f, %f, %f, %f)" %tuple(marginal[0,0:5])) 
    print("Mean latent rmse across batch: %f"
          % average_rmse) 
    print("Estimate of CE between full posterior and q: %f"
          % ce_estimate)
    # print("Breakdown of CE across first time stamps: (%f, %f, %f, %f, %f, %f, %f, %f, %f, %f)" %tuple(ce_estimate_t)) 
    print("Batch-ave. # of resampling steps: %.2f out of %d"
          % (algo.get_average_n_resampling(), algo.get_n_step()))
    print("Batch-ave. # of different values of paths at 0-step, mid-step, 0:mid-steps: (%.2f, %.2f, %.2f)"
          % (n_fst_deg, n_mid_deg, n_half_deg))
    print("Log weights (Avg, Max, Med, Min): (%f, %f, %f, %f)"
          % (np.average(weight), np.max(weight), np.median(weight), np.min(weight))) 
    print("h diff (Max, Min): (%f, %f)"
          % (algo.h_diff_max, algo.h_diff_min)) 
    print("log-pi diff (Max, Min): (%f, %f)"
          % (algo.log_pi_ratio_max, algo.log_pi_ratio_min)) 
    print("log-q (Max, Min): (%f, %f)"
          % (algo.log_q_max, algo.log_q_min)) 
    # print("log marginals (Avg, Max, Med, Min): (%f, %f, %f, %f)" 
    #          % (np.average(marginal), np.max(marginal), 
    #              np.median(marginal), np.min(marginal)))
    # print("log marginals of last step (Avg, Max, Med, Min): (%f, %f, %f, %f)" 
    #         % (np.average(marginal[:,-1]), np.max(marginal[:,-1]), 
    #             np.median(marginal[:,-1]), np.min(marginal[:,-1])))
    return objective_t, average_ess_t, average_lml, ce_estimate, average_rmse, ce_estimate_t, std_no_h_t, std_h_t

def plot_gaussian(ax, mu, var, grid, label):
    """ plot gaussian pdf and return the values of pdf at grid points """
    std = np.sqrt(var)
    pdf_values = norm.pdf(grid,mu,std)
    ax.plot(grid, pdf_values, label = label)
    return pdf_values

def plot_result_general(fixed_xs_ys, exper, oracle, n_times_wanted, epoch, iter, dir): 
    """ plot lkhd, prior, filtering, proposal for nlssm 
        for particles at n_times_wanted evenly space time steps """
    var_v = exper.var_v
    var_w = exper.var_w
    fixed_xs, fixed_ys = fixed_xs_ys
    fixed_ys = denormalise(fixed_ys, oracle.mean_y, oracle.std_y)
    batch_size, n_step, state_dim = fixed_xs.shape
    n_times = min(n_step, n_times_wanted)
    num_pts = min(4,batch_size)
    mini_batch = range(num_pts)
    # b = batch_size/2
    time_step = n_step / n_times
    time_range = range(0,n_step,time_step)
    #time_range = range(0,5)

    n_grid = 100
    grid_range = np.max(fixed_xs)-np.min(fixed_xs)
    grid_max = np.max(fixed_xs) + 0.2*grid_range
    grid_min = np.min(fixed_xs) - 0.2*grid_range
    grid = np.linspace(grid_min, grid_max, n_grid)

    f,a = plt.subplots(num_pts, n_times, figsize = (25,5*num_pts))
    for idx,ax in enumerate(a.flatten()):
        t = time_range[idx%n_times]
        b = mini_batch[idx/n_times]
        if t > 0:
            x_prev = fixed_xs[:,t-1,:]
            prior_mean = exper.compute_next_mean(t,x_prev[b,:])
        else:
            x_prev = np.zeros(shape=[batch_size, state_dim])
            prior_mean = exper.init_mean(x_prev)[0,:] # array[state_dim]
        y = fixed_ys[:,t,0]
        """ Get mean and log_var of q_t(.|test_x_prev). """
        _, _, test_mean, test_log_var, _, _ = oracle.sample_next(t,x_prev)
        lkhd_vals = np.exp(exper.log_lkhd_lifted(t, y[b], np.expand_dims(grid,axis=1)))
        ax.plot(grid,lkhd_vals, label = "lkhd=p(y_t|x_t)")

        prior_vals = plot_gaussian(ax, prior_mean, var_v, grid, "prior=p(x_t|x_{t-1})")
        post_vals = prior_vals * lkhd_vals
        norm_const = (grid_max - grid_min)*(2*np.sum(post_vals)-post_vals[0]-post_vals[-1])/(2*(n_grid-1))
        ax.plot(grid,post_vals/norm_const, label = "one-step posterior c*p(x_t|x_{t-1},y_t)")
        _ = plot_gaussian(ax, test_mean[b,:], np.exp(test_log_var[b]), grid, "proposal")
        h_vals = oracle.compute_h_batch(np.expand_dims(grid,axis=1),t,b)
        flkhd_approx = np.exp(h_vals)
        ax.plot(grid,flkhd_approx, label = "exp(h_t(x_t;y_{t+1:T}))")
        ax.set_title("densities for particle at time %d" %t)
        ax.set_xlim([grid_min, grid_max])
        ax.set_ylim([0, 1])
        if idx == 0:
            ax.legend(loc='center left', bbox_to_anchor=(-1, 0.5), 
                ncol=1, fancybox=True, shadow=True)
        f.suptitle('iter %d of epoch %d' % (iter, epoch))
        plt.savefig(dir + '/epoch%d_%d' % (epoch, iter)) 
    return

def plot_result_lgssm(fixed_xs_ys, exper, oracle, n_times_wanted, epoch, iter, dir):
    """ plot filtering, smoothing, proposal, future lkhd, exp(h) for lgssm 
        for particles at n_times_wanted evenly space time steps """
    fixed_xs, fixed_ys = fixed_xs_ys
    fixed_ys = denormalise(fixed_ys, oracle.mean_y, oracle.std_y)
    batch_size, n_step, state_dim = fixed_xs.shape
    n_times = min(n_step, n_times_wanted)
    num_pts = min(4,batch_size)
    mini_batch = range(num_pts)
    #b = batch_size / 2 # choose data pt at middle of batch
    time_step = n_step / n_times
    time_range = range(0,n_step,time_step)
    input_data = fixed_ys[:,:,0]
    flkhd_mu, flkhd_prec, _, _ = exper.future_lkhd(input_data)

    n_grid = 100
    grid_range = np.max(fixed_xs)-np.min(fixed_xs)
    #grid_max = np.max(fixed_xs) + 0.2*grid_range
    #grid_min = np.min(fixed_xs) - 0.2*grid_range
    #grid = np.linspace(grid_min, grid_max, n_grid)
    f,a = plt.subplots(num_pts, n_times, figsize = (25,5*num_pts))
    for idx, ax in enumerate(a.flatten()):
        t = time_range[idx%n_times]
        b = mini_batch[idx/n_times]
        if t > 0:
            x_prev = fixed_xs[:,t-1,:]
        else:
            x_prev = np.zeros(shape=[batch_size, state_dim])
        y = fixed_ys[:,t,0]
        _, _, prop_mean, prop_log_var, _, _ = oracle.sample_next(t,x_prev)
        #lkhd_vals = np.exp(exper.log_lkhd_lifted(t, y[b], np.expand_dims(grid,axis=1)))
        #ax.plot(grid,lkhd_vals, label = "lkhd p(y_t|x_t)")
        #_ = plot_gaussian(ax, exper.compute_next_mean(t, x_prev[b,:]), exper.var_v, grid, "prior p(x_t|x_{t-1})")
        mu_flkhd = flkhd_mu[b,t] # 0 for t = T-1
        prec_flkhd = flkhd_prec[b,t] # 0 for t = T-1
        mu_subopt, prec_subopt = exper.suboptimal_proposal(t,x_prev[b,:],y[b])
        mu_opt, prec_opt = exper.optimal_proposal(t,x_prev[b,:],y[b],mu_flkhd,prec_flkhd)
        grid_min = min([mu_opt - 4./np.sqrt(prec_opt),mu_subopt - 4./np.sqrt(prec_subopt)])
        grid_max = max([mu_opt + 4./np.sqrt(prec_opt),mu_subopt + 4./np.sqrt(prec_subopt)])
        if t < n_step - 1:
            grid_min = min([grid_min, mu_flkhd - 4./np.sqrt(prec_flkhd)])
            grid_max = max([grid_max, mu_flkhd + 4./np.sqrt(prec_flkhd)])
        grid = np.linspace(grid_min, grid_max, n_grid)
        _ = plot_gaussian(ax, mu_subopt, 1./prec_subopt, grid, "one-step posterior p(x_t|x_{t-1},y_t)")
        _ = plot_gaussian(ax, mu_opt, 1./prec_opt, grid, "optimal proposal p(x_t|x_{t-1},y_{t:T})")
        _ = plot_gaussian(ax, prop_mean[b,:], np.exp(prop_log_var[b]), grid, "proposal q(x_t|x_{t-1},y_{t:T})")
        if t < n_step - 1:        
            _ = plot_gaussian(ax, flkhd_mu[b,t],1./flkhd_prec[b,t],grid, "future lkhd p(y_{t+1:T}|x_t)")
            flkhd_max_density = np.sqrt(flkhd_prec[b,t]/(2*np.pi)) # max value of future lkhd
            h_vals = oracle.compute_h_batch(np.expand_dims(grid,axis=1),t,b)
            #h_vals = h_vals + np.log(flkhd_max_density) - np.amax(h_vals) # renormalise h (in log space) so that it has the same max as flkhd
        else:
            ax.plot(grid,np.zeros(shape=grid.shape))
            h_vals = np.zeros(shape=grid.shape)        
        flkhd_approx = np.exp(h_vals)
        #norm_const = (grid_max-grid_min)*(2*np.sum(flkhd_approx)-flkhd_approx[0]-flkhd_approx[-1])/(2*(n_grid-1))
        #flkhd_approx_norm = flkhd_approx/norm_const
        ax.plot(grid,flkhd_approx, label = "exp(h_t(x_t;y_{t+1:T}))")
        ax.set_title("densities for particle at time %d" %t)
        
        y_max = max(np.sqrt([prec_opt,prec_subopt])/np.sqrt(2*np.pi))*1.1
        ax.set_xlim([grid_min, grid_max])
        ax.set_ylim([0, y_max])
        if idx == 0:
            #print("filtering/smoothing variance ratio: %f" %(prec_opt/prec_subopt))
            ax.legend(loc='center left', bbox_to_anchor=(-1, 0.5),
          ncol=1, fancybox=True, shadow=True)
    f.suptitle('iter %d of epoch %d' % (iter, epoch))
    plt.savefig(dir + '/lgssm_epoch%d_%d' % (epoch, iter))  
    return

class OutputWriter:

    def __init__(self, stdout, file_name):
        self.stdout = stdout
        self.logfile = file(file_name, 'w')
        return

    def write(self, text):
        self.stdout.write(text)
        self.logfile.write(text)
        return

    def close(self):
        self.stdout.close()
        self.logfile.close()
        return

def open_write_output(file_name):
    """ Write output of python script to file """
    out_file = '../../resource/output/' + file_name + '.txt'
    #print("##### Output_file_name = " + file_name + ".txt #####")
    writer = OutputWriter(sys.stdout, out_file)
    sys.stdout = writer
    return file_name

def extract_from_output(file_name, line_mod, skip_line):
    """ extract numbers at line numbers line_mod (mod skip_line) """
    file = open(file_name,"r")
    ess_list = []
    line_no = 1
    for line in file:
        if line_no % skip_line == line_mod:
            for t in line.split():
                try:
                    if float(t)%100 != 0:
                        ess_list.append(float(t))
                except ValueError:
                    pass
        line_no = line_no + 1
    file.close()
    return ess_list

def plot_t(array, y_label, dir_name):
    """ for each col of array, plot across the rows"""
    num_cols = array.shape[1]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    colormap = matplotlib.cm.gist_rainbow
    ax.set_color_cycle([colormap(i) for i in np.linspace(0,1,num_cols)])
    for col in xrange(num_cols):
        ax.plot(array[:,col], label='t='+str(col),alpha=0.5, linewidth=0.5)
    ax.set_y_label = y_label
    ax.legend(loc='best')
    plt.savefig(dir_name+'_'+y_label+'.png')
    return

def evaluation_plot(string, plot_all_iter, labels, start_iter, ylim=None, exact=None, file_name=None, figsize=(12,9), kl_line_number=10, skip_line=19):
    """ string: takes one of three values ['ess','ml','min_ess','ce','kl','rmse'].
            'log_ml' produces box_plots for log_ml from start_iter.
            'rmse' prints mean and std of latent rmse for final 100 iter.
        plot_all_iter: boolean. If True plots ml/ce across all iter. If False makes boxplot of ml
                       from start iter
        labels: The output files should be stored in output directory with
                file name matching labels
        start_iter: first iter for box plots
        ylim: limits of y axis
        exact: (optional) exact value of log mean ml
        kl_line_number: line number for kl value
        skip_line_number: number of lines of output per iteration
        """

    if string == 'kl':
        line_number = kl_line_number
    elif string == 'ess':
        line_number = kl_line_number + 1
    elif string == 'min_ess':
        line_number = kl_line_number + 3
    elif string == 'log_ml':
        line_number = kl_line_number + 4
    elif string == 'rmse':
        line_number = kl_line_number + 5
    elif string == 'ce':
        line_number = kl_line_number + 6

    n_plot = len(labels)
    data=[None]*n_plot
    for i in range(n_plot):
        data[i] = extract_from_output("../../resource/output/"+labels[i]+".txt",line_number,skip_line)
    num_iter = len(data[0])

    fig = plt.figure()
    ax = fig.add_subplot(111)
    if string == 'log_ml' or string == 'ce' or string == 'kl':
        if plot_all_iter:
            colormap = matplotlib.cm.gist_rainbow
            ax.set_color_cycle([colormap(i) for i in np.linspace(0,1,n_plot)])
            ymin = min(data[0][100:-1])
            ymax = max(data[0][100:-1])
            for i in range(n_plot):
                ax.plot(data[i], label=labels[i], alpha=0.5, linewidth=1.0)
                ymin = min(ymin, min(data[i][100:-1]))
                ymax = max(ymax, max(data[i][100:-1]))
        else:
            n_plot = len(data)
            boxplot_data = [None]*n_plot
            for i in range(n_plot):
                boxplot_data[i] = data[i][start_iter:num_iter]
            ax.boxplot(boxplot_data)
            ax.set_xticklabels(labels)
        if ylim is not None:
            plt.ylim(ylim)
        if exact is not None:
            ax.axhline(y=exact, label='exact')
        ax.legend(loc = 'best')


    if string == 'rmse':
        if plot_all_iter:
            colormap = matplotlib.cm.gist_rainbow
            ax.set_color_cycle([colormap(i) for i in np.linspace(0,1,n_plot)])
            for i in range(n_plot):
                ax.plot(data[i], label=labels[i], alpha=0.5, linewidth=1.0)
            ax.legend(loc = 'best')
        else:
            n_plot = len(data)
            boxplot_data = [None]*n_plot
            for i in range(n_plot):
                rmse_data = data[i][num_iter-100:num_iter]
                mean = np.mean(rmse_data)
                std = np.std(rmse_data)
                label = labels[i]
                print("%s RMSE: mean %f, std: %f" %(label, mean, std))

    if string == 'ess' or string == 'min_ess':
        colormap = matplotlib.cm.gist_rainbow
        ax.set_color_cycle([colormap(i) for i in np.linspace(0,1,n_plot)])
        for i in range(n_plot):
            ax.plot(data[i], label = labels[i], alpha=0.5, linewidth=1.0)
        ax.legend(loc = 'best')

    ax.set_ylabel(string)
    if file_name is not None:
        fig.set_size_inches(figsize[0],figsize[1])
        plt.savefig("../../resource/pictures/"+file_name+".png")
    else:
        plt.show()
    return

def compute_threshold(p, var_v, T, prec=100000):
    """Compute the threshold c such that Prob(max_t |X_t| >= c) is close to p. 
    p is a probability close to 0. X_t = sum_{k=1}^t N(0,var_v). Estimate using prec * n samples from N(0,var_v).
    prec is a positive integer that determines the accuracy of the computation. The higher the more accurate.
    """
    n = int(round(prec * 1./(1-p))) 
    sample_sums = np.random.normal(0., np.sqrt(var_v), size=(n,T)) # array[n,T]

    # let entries be the partial sums across cols
    for i in range(1,T):
        sample_sums[:,i] += sample_sums[:,i-1]

    # compute max of the absolute value of partial sums
    max_sums = np.amax(np.absolute(sample_sums), axis=1) # array[n]

    # find prec th smallest among the n
    minimum = np.partition(max_sums, prec-1)[prec-1]

    # find (prec+1) th smallest among the n
    minimum2 = np.partition(max_sums, prec)[prec]

    return np.mean([minimum,minimum2])
    

import numpy as np
import matplotlib
import tensorflow as tf
from tensorflow.python import debug as tf_debug
import os
import time
from scipy.misc import logsumexp

import smc
import smc_opt
import util

import oracle_lookahead
import oracle_lookahead_bootstrap
import oracle_lookahead_bootstrap_new
import oracle_lookahead_bootstrap_new_denoise
import oracle_lookahead_bootstrap_new_denoise_no_t
import oracle_lookahead_bootstrap_new_halfnoise_no_t
import oracle_lookahead_no_h
import oracle_lookahead_no_h_no_t
import oracle_lookahead_no_lstm
import oracle_no_lstm_bootstrap
import oracle_prior
import oracle_nasmc
import oracle_nasmc_no_fx
import oracle_lstm_type1
import oracle_lstm_type1_no_fx
import oracle_lstm_type1_t
import oracle_lstm_type2
import oracle_lstm_type3

import exper_lgssm
import exper_lgssm_high
import exper_nlssm
import exper_nlssm_high
import exper_jump

""" Script that performs experiments. """ 

exper_lgssm_lstm = exper_lgssm.ExperLGSSM()
exper_lgssm_prior = exper_lgssm.ExperPrior()
exper_lgssm_lstm_high = exper_lgssm_high.ExperLGSSM()
exper_lgssm_prior_high = exper_lgssm_high.ExperPrior()

exper_nlssm_lstm = exper_nlssm.ExperNLSSM()
exper_nlssm_prior = exper_nlssm.ExperPrior()
exper_nlssm_lstm_high = exper_nlssm_high.ExperNLSSM()
exper_nlssm_prior_high = exper_nlssm_high.ExperPrior()

exper_jump_lstm = exper_jump.ExperJUMP()
exper_jump_prior = exper_jump.ExperPrior()

write = True
model = 'lgssm'
oracle_type = 'bootstrap'
is_high_dimensional = False
have_fixed_data_stored = True
plot = True
normalise = True
string = model
if model == 'lgssm':
    if is_high_dimensional:
        exper_prior = exper_lgssm_prior_high
        exper_lstm = exper_lgssm_lstm_high
    else:
        exper_prior = exper_lgssm_prior
        exper_lstm = exper_lgssm_lstm
    string += ('_varv%.1f_varw%.1f' %(exper_lstm.var_v, exper_lstm.var_w[0]))
elif model == 'nlssm':
    if is_high_dimensional:
        exper_prior = exper_nlssm_prior_high 
        exper_lstm = exper_nlssm_lstm_high
    else:
        exper_prior = exper_nlssm_prior
        exper_lstm = exper_nlssm_lstm
    string += ('_varv%.1f_varw%.1f' %(exper_lstm.var_v, exper_lstm.var_w[0]))
elif model == 'jump':
    exper_prior = exper_jump_prior
    exper_lstm = exper_jump_lstm
    string += ('_varv%.1f_varw%.2f_thresh%.1f' %(exper_lstm.var_v, exper_lstm.var_w[0], exper_lstm.threshold))

exper = exper_lstm

#config = tf.ConfigProto(); config.gpu_options.visible_device_list = str(exper.gpu) # just use /gpu:0
config = tf.ConfigProto(device_count = {'GPU': 0}) # only use cpu's
sess = tf.Session(config = config) 
# sess = tf_debug.LocalCLIDebugWrapperSession(sess) 
# sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)

oracle_prior = oracle_prior.Oracle(sess, exper_prior)
#oracle_lkhd = oracle_lookahead.Oracle(sess, exper_lstm)
oracle_lstm1 = oracle_lstm_type1.Oracle(sess, exper_lstm)
#oracle_lstm1_no_fx = oracle_lstm_type1_no_fx.Oracle(sess, exper_lstm)
#oracle_lstm1_t = oracle_lstm_type1_t.Oracle(sess, exper_lstm)
oracle_lstm2 = oracle_lstm_type2.Oracle(sess, exper_lstm)
#oracle_lstm3 = oracle_lstm_type3.Oracle(sess, exper_lstm)
#oracle_lkhd_bootstrap = oracle_lookahead_bootstrap.Oracle(sess, exper_lstm)
#oracle_lkhd_bootstrap_new = oracle_lookahead_bootstrap_new.Oracle(sess, exper_lstm)
#oracle_lkhd_bootstrap_new_denoise = oracle_lookahead_bootstrap_new_denoise.Oracle(sess, exper_lstm)
#oracle_lkhd_bootstrap_new_denoise_no_t = oracle_lookahead_bootstrap_new_denoise_no_t.Oracle(sess, exper_lstm)
#oracle_lkhd_bootstrap_new_halfnoise_no_t = oracle_lookahead_bootstrap_new_halfnoise_no_t.Oracle(sess, exper_lstm)
#oracle_lkhd_no_h = oracle_lookahead_no_h.Oracle(sess, exper_lstm)
#oracle_lkhd_no_h_no_t = oracle_lookahead_no_h_no_t.Oracle(sess, exper_lstm)
oracle_nasmc = oracle_nasmc.Oracle(sess, exper_lstm)
#oracle_nasmc_no_fx = oracle_nasmc_no_fx.Oracle(sess, exper_lstm)
#oracle_lkhd_no_h_no_t = oracle_lookahead_no_h_no_t.Oracle(sess, exper_lstm)
#oracle_no_lstm1 = oracle_no_lstm_bootstrap.Oracle(sess, exper_lstm)
#oracle_no_lstm2 = oracle_lookahead_no_lstm.Oracle(sess, exper_lstm)

if exper.init_bootstrap:
    string += '_binit'
    string += ('scale%.3f' %(np.sqrt(exper.init_dnnq_output_scale)))

if exper.use_layernorm:
    string += '_ln'

if oracle_type == 'bootstrap':
    oracle = oracle_prior
elif oracle_type == 'nasmc':
    oracle = oracle_nasmc
elif oracle_type == 'lstm1':
    oracle = oracle_lstm1
    if exper.h_learning_rate != exper.q_learning_rate:
        string += ('_hlr%.5f' %exper.h_learning_rate)

file_name = (string + '_'+ oracle_type +'_%dd_m%d_T%d_dnn%d_epoch%d' \
             % (exper.state_dim,exper.batch_size,exper.n_step,exper.dnn_sizes[0],exper.n_epoch))
output_file_name = '../../resource/output/' + file_name + '.txt'
# avoid overwriting:
while os.path.isfile(output_file_name):
    file_name += '+'
    output_file_name = '../../resource/output/' + file_name + '.txt'

dir_name = '../../resource/pictures/' + file_name    
    

if plot and not is_high_dimensional:
    os.system("mkdir -p " + dir_name) # make directory for storing plots

algo = smc_opt.SMC(exper, oracle)

n_step = int(exper.n_step)
batch_size = exper.batch_size
state_dim = exper.state_dim

if write:
    util.open_write_output(file_name) # write output to file ../../resource/output/file_name.txt

print('##### start preprocessing steps #####')
if have_fixed_data_stored:
    var_v = exper.var_v
    var_w = exper.var_w[0]
    if model == 'lgssm':
        if is_high_dimensional:
            filename = ('../../resource/data/lgssm_d%d_m%d_T%d_varv%.1f_varw%.1f_phiv%.1f_phiw%.1f_test_data.npz' \
                        % (state_dim,batch_size,n_step,var_v,var_w,exper.phi_v,exper.phi_w[0]))
        else:
            filename = ('../../resource/data/lgssm_d%d_m%d_T%d_varv%.1f_varw%.1f_phiv%.1f_phiw%.1f_test_data.npz' \
                        % (state_dim,batch_size,n_step,var_v,var_w,exper.phi_v[0],exper.phi_w))
    elif model == 'nlssm':
        filename = ('../../resource/data/'+ model +'_d%d_m%d_T%d_varv%.1f_varw%.1f_test_data.npz' \
                    % (state_dim,batch_size,n_step,var_v,var_w))
    elif model == 'jump':
        filename = ('../../resource/data/'+ model +'_d%d_m%d_T%d_varv%.1f_varw%.2f_thresh%.1f_test_data.npz' \
                    % (state_dim,batch_size,n_step,var_v,var_w,exper.threshold))
    if os.path.isfile(filename):
        print('Loading a test file from '+filename)
        test_data = np.load(filename)
        fixed_xs = test_data['x']
        fixed_ys = test_data['y']
    else:
        print('Generating and saving a test file: '+filename)
        fixed_xs, fixed_ys = exper.generate_minibatch()
        np.savez(filename,x=fixed_xs,y=fixed_ys)
else:
    print('Generating a test file')
    fixed_xs, fixed_ys = exper.generate_minibatch()

if normalise:
    mean_x = np.mean(fixed_xs, axis=0) # array[n_step, state_dim]
    std_x = np.std(fixed_xs, axis=0) # array[n_step, state_dim]
    mean_y = np.mean(fixed_ys, axis=0) # array[n_step, 1]
    std_y = np.std(fixed_ys, axis=0) # array[n_step, 1]
else:
    mean_x = np.zeros([n_step, state_dim])
    std_x = np.ones([n_step, state_dim])
    mean_y = np.zeros([n_step, 1])
    std_y = np.ones([n_step, 1])

#fixed_xs = util.normalise(fixed_xs, mean_x, std_x)
fixed_ys_norm = util.normalise(fixed_ys, mean_y, std_y)

fixed_xs_ys = (fixed_xs, fixed_ys)
fixed_xs_ys_norm = (fixed_xs, fixed_ys_norm)

# store mean_y, std_y in oracle
oracle.mean_y = mean_y
oracle.std_y = std_y

oracle.build_graph()
init = tf.global_variables_initializer()
sess.run(init)
matplotlib.rcParams.update({'figure.max_open_warning': 0}) # suppress warning of > 20 figures

if (model=='lgssm') and not is_high_dimensional:
    _,_,_,log_marginal_lkhd = exper.future_lkhd(fixed_ys[:,:,0])
    log_mean_marginal = logsumexp(log_marginal_lkhd)-np.log(exper.batch_size)
    print("True log mean marginal likelihood across batch: %f" %log_mean_marginal)

print("##### batch_size=%d, n_step=%d, n_particle=%d, q_learning_rate=%f, h_learning_rate=%f #####" \
      % (exper.batch_size, exper.n_step, exper.n_particle, exper.q_learning_rate, exper.h_learning_rate))
objective = [None]*(exper.n_epoch*exper.n_iter)
ess = [None]*(exper.n_epoch*exper.n_iter)
log_ml = [None]*(exper.n_epoch*exper.n_iter)
ce = [None]*(exper.n_epoch*exper.n_iter)
ce_t = [None]*(exper.n_epoch*exper.n_iter)
rmse = [None]*(exper.n_epoch*exper.n_iter)
std_no_h_t = [None]*(exper.n_epoch*exper.n_iter)
std_h_t = [None]*(exper.n_epoch*exper.n_iter)
idx = 0
for epoch in xrange(exper.n_epoch):
    print "### Epoch: %d ###" % epoch
    _, array_ys = exper.generate_minibatch()
    array_ys_norm = util.normalise(array_ys, mean_y, std_y) # normalised array_ys
    for i in xrange(exper.n_iter):         
        print("### Iter %d of Epoch %d ###" % (i, epoch))
        oracle.feed_data(array_ys_norm)  
        smc_result = algo.run(array_ys)
        (rollout, weight, _, _, ancestry) = smc_result
        oracle.train(i, rollout, weight, array_ys_norm, ancestry)
        oracle.feed_data(fixed_ys_norm)
        if plot and (model=='lgssm'):
            exper.plot(fixed_xs_ys, oracle, epoch, i, dir_name)
        rollout, weight, marginal, marginal_mean_acc, ancestry = algo.run(fixed_ys)
        objective[idx], ess[idx], log_ml[idx], ce[idx], rmse[idx], ce_t[idx], std_no_h_t[idx], std_h_t[idx] = util.print_result(
            oracle, algo, rollout, weight, marginal, marginal_mean_acc, ancestry, fixed_xs)
        idx += 1
objective = np.array(objective) # array[n_iter*n_epoch,n_step]
ess = np.array(ess) # array[n_iter*n_epoch,n_step]
ce_t = np.array(ce_t) # array[n_iter*n_epoch,n_step]
std_no_h_t = np.array(std_no_h_t) # array[n_iter*n_epoch,batch_size,n_step]
std_h_t = np.array(std_h_t) # array[n_iter*n_epoch,batch_size,n_step]
#util.plot_t(objective,'kl',dir_name)
util.plot_t(ess,'ess',dir_name)
util.plot_t(ce_t,'ce',dir_name)
util.plot_t(np.mean(std_no_h_t, axis=1),'std_no_h',dir_name)
util.plot_t(np.mean(std_h_t, axis=1),'std_h',dir_name)

sess.close()    

if write:
    print("##### Output_file_name = " + file_name + ".txt #####")
    

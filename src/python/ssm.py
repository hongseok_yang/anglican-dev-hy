import numpy as np
import tensorflow as tf
import scipy.stats

def compute_next_mean(x, t): 
    return 0.5 * x + 25. * x / (1. + x ** 2) + 8. * np.cos(1.2 * t)

def sample_q(t, x=0, var_v=1):
    """ Sample from a transition kernel q_t(.|x) for t = 0, 1, ..., T-1. """
    if t > 0:
        return compute_next_mean(x, t) + np.random.normal(0., np.sqrt(var_v))
    else:
        return np.random.normal(0., np.sqrt(5.))        

def generate(var_v, var_w, T):
    """ Generate T data points from a non-linear state space model. """
    state = [None]*T # pre-allocate list
    obs = [None]*T
    cur = 0 # temporary state
    for t in xrange(T):
        cur = sample_q(cur,t,var_v) # current state
        state[t] = cur
        obs[t] = 0.05*cur*cur + np.random.normal(0, np.sqrt(var_w))
    return state, obs   


def np_log_trans(x_t, t, x, var_v):
    """ Compute the log transition probability log(f(x_t|x)).
        x_t and x and numpy arrays, and t and var_v are scalar 
        values. The result is a numpy array. """
    if t > 0:    
        mean = compute_next_mean(x, t)
        return -0.5 * np.log(2. * np.pi * var_v) - ((x_t - mean) ** 2) / (2. * var_v)
    else:
        return -0.5 * np.log(2. * np.pi * 5.) - (x_t  ** 2) / 10.

def np_log_lkhd(y, x, var_w):
    """ Compute the log likelihood log(p(y|x)), 
        y and x and numpy arrays, and var_w is a scalar value.
        The result is a numpy array. """
    mean = 0.05 * (x ** 2)
    return -0.5 * np.log(2. * np.pi * var_w) - ((y - mean) ** 2) / (2. * var_w)

def log_trans(x_t, t, x, var_v): 
    """ Build a tensorflow computation graph that computes the log 
        transition probabiliy log(f(x_t|x)), and return a tensorflow 
        object that denotes the result of this computaiton. """
    if t > 0:    
        mean = compute_next_mean(x, t)
        var = var_v
    else:
        mean = tf.zeros(tf.shape(x))
        var = 5
    return -0.5 * tf.log(2. * np.pi * var) - ((x_t - mean) ** 2) / (2. * var)
    
def log_lkhd(y,x,var_w):
    """ Build a tensorflow computation graph that computes the log 
        likelihood log(p(y|x)), and return a tensorflow 
        object that denotes the result of this computaiton. """
    mean = 0.05 * (x ** 2)
    var = var_w
    return -0.5 * tf.log(2. * np.pi * var) - ((y - mean) ** 2) / (2. * var)


class ExperNonlinearSSM(object):

    def __init__(self):
        """ Parameters for our algorithm """
        self.lstm_size = 100 # num_units of h,c and output of lstm 
        self.dnn_sizes = [100,100,100] # list of layer sizes for dnn 
        self.max_grad_norm = 5 
        self.learning_rate = 0.001 # default for adam 
        self.batch_size = 10 # size of minibatch 
        self.n_iter = 30 # number of gradient updates 
        self.n_epoch = 100 # number of epochs
        self.n_particle = 100 # number of particles in smc 
        self.state_dim = 1 # dimension of state 
        self.h_size = 1. # amplifier for h

        """ Parameters for a state-space model used in our experiment """ 
        self.n_step = 30 # number of steps in the model 
        self.var_v = 10 # variance for Gaussian transition 
        self.var_w = 10 # variance for Gaussian likelihood
        return

    def __generate(self):
        return ssm.generate(self.var_v, self.var_w, self.n_step) 

    def __log_lkhd(self, y, x): 
        return ssm.np_log_lkhd(y, x, self.var_w) 

    def __log_init_prob(self, x): 
        return ssm.np_log_trans(x, 0, 0, self.var_v)

    def __log_trans_prob(self, t, x_prev, x_curr): 
        return ssm.np_log_trans(x_curr, t, x_prev, self.var_v)        

    def get_dim(self):
        return self.batch_size, self.n_step, self.n_particle, self.state_dim

    def generate_minibatch(self):
        """ Generate a minibatch from a joint model. The minibatch includes
            both sequences of hiden states and sequences of observations. 
            The result is a pair of two arrays of the following shapes: 
            [batch_size,n_step,state_dim] and [batch_size,n_step,1]. """
        batch_size, _, _, _ = self.get_dim()
        list_xs_ys = [self.__generate() for _ in xrange(batch_size)]
        list_xs = map(lambda xy: xy[0], list_xs_ys) 
        list_ys = map(lambda xy: xy[1], list_xs_ys) 
        array_xs = np.expand_dims(np.array(list_xs), axis=2)
        array_ys = np.expand_dims(np.array(list_ys), axis=2)
        return array_xs, array_ys

    def __compute_log_pi_ratio(self, t, x, ys, compute_log_trans): 
        batch_size, _, _, _ = self.get_dim() 
        log_ratio = np.zeros(shape=[batch_size]) 
        for b in xrange(batch_size): 
            l = self.__log_lkhd(ys[b,t,0], x[b,0])
            k = compute_log_trans(b, t, x[b,0])
        log_ratio[b] = l + k 
        return log_ratio

    def compute_log_pi_ratio_init(self, x, ys): 
        """ Return log(pi_0(x) / 1) where pi_0 is an unnormalised target density that 
            incorporates no observations. x is an numpy array of shape 
            [batch_size,state_dim], and y an array of shape [batch_size,n_step,1].  
            The result is another array of the same shape. """ 
        compute_log_trans = lambda b, t, v_cur: self.__log_init_prob(x[b,0])
        log_ratio = self.__compute_log_pi_ratio(0, x, ys, compute_log_trans)
        return log_ratio

    def compute_log_pi_ratio_next(self, t, x_prev, x, ys): 
        """ Compute log(pi_t(x) / pi_(t-1)(x_prev)) where 
            pi_t and pi_(t-1) are unnormalised target densities.  
            x_prev and x are numpy arrays of shape [batch_size,state_dim], 
            and ys is an array of shape [batch_size,n_step,1].  
            The function does this log-ratio computation for the ith 
            entries of these arrays for each i, and returns an 
            array of shape [batch_size,1]. """ 
        assert (t > 0) 
        compute_log_trans = lambda b, t, v_cur: self.__log_trans_prob(t, x_prev[b,0], v_cur)
        log_ratio = self.__compute_log_pi_ratio(t, x, ys, compute_log_trans)
        return log_ratio



import numpy as np
import tensorflow as tf
import scipy.stats

from exper import Exper
import util

def compute_normal_log_pdf(x, mean, var): 
    """ Input type:
        (1) x : array[a1,...,an,b]
        (2) mean : array[a1,...,an,b]
        (3) var : scalar
        Output type: array[a1,...,an]. """
    dim = x.shape[-1]
    distance = np.sum((x - mean) ** 2, axis=-1)
    return - distance / (2. * var) - 0.5 * dim * np.log(2. * np.pi * var)

class ExperNLSSM(Exper):
    """ Class for storing info about performing experiments with a simple high-dimensional        
    nonlinear ssm model. """

    def __init__(self):
        """ Parameters for our algorithm """
        self.batch_size = 10 # size of a minibatch
        self.n_epoch = 1000 # number of epochs
        self.n_particle = 100 # number of particles in smc 
        self.state_dim = 10 # dimension of state 
        self.n_iter = 1 # number of gradient updates 
        self.adapt_threshold = 0.5 # fraction of ESS below which we resample
        self.h_size = 0. # amplifier for h
        self.q_learning_rate = 0.0001
        self.h_learning_rate = 0.0001
        self.optimiser = 'Adam' # 'RMSProp' or 'Adam'
        self.init_q_std = 1. # initialisation of std of q(x_t|x_(t-1))
        self.init_h = 10. # initialisation of h(y_(t+1:T-1)|x_t)

        self.lstm_size = 100 # num_units of h,c and output of lstm 
        self.dnn_sizes = [100,100,100] # list of layer sizes for dnn 
        self.nonlinearity = [tf.nn.elu, tf.nn.elu, tf.tanh]
        self.max_grad_norm = 5. 

        """ Parameters for a state-space model used in our experiment """ 
        self.n_step = 10  # number of steps in the model 
        self.var_v = 10. # variance for Gaussian transition 
        self.var_w = 1.*np.ones(self.n_step) # variance for Gaussian likelihood

        """ Parameters for objective """
        self.kl_weights = np.ones(self.n_step)

        """ Miscellaneous parameters """
        self.gpu = 6 # gpu number to use
        self.linear = False # used to determine whether to use gaussian parameterisation for h. Only relevant for l_b_n_d_no_t.
        self.use_dnnq = True # used for lstm1 to determine whether to use DNNQ or not - MUST be True for SSM with high dimensional x.
        self.init_bootstrap = True # add bootstrap mean and (log var - 2*log init_q_std) to DNNQ output mean and log var
        self.init_dnnq_output_scale = (0.05)**2 # var of output of DNNQ / var of input to DNNQ, used for uniform initialisation of wieghts. If None, Xavier init is used. Only relevent if self.init_bootstrap=True
        self.init_dnnh_output_scale = None # var of output of DNNH / var of input to DNNH, used for uniform initialisation of wieghts. If None, Xavier init is used.
        return

    def compute_next_mean(self, t, x): 
        """ Input type:
            (1) t : scalar
            (2) x : array[a1,...,an,state_dim]
            Output type: array[a1,...,an,state_dim]. """
        return 0.5 * x + 25. * x / (1. + x ** 2) + 8. * np.cos(1.2 * t)

    def tf_compute_next_mean(self, t, x): 
        """ Input type:
            (1) t : scalar
            (2) x : tensorflow_array[a1,...,an,state_dim]
            Output type: tensorflow_array[a1,...,an,state_dim]. """
        return 0.5 * x + 25. * x / (1. + x ** 2) + 8. * tf.cos(1.2 * t)

    def log_lkhd(self, t, y, x): 
        """ Input type: 
            (1) t : scalar
            (2) y : scalar
            (3) x : array[state_dim]
            Output type: scalar. """
        mean = 0.05 * (np.sum(x) ** 2)
        return compute_normal_log_pdf(np.array([y]), np.array([mean]), self.var_w[t])

    def log_lkhd_particles(self, t, y, x): 
        """ Compute log likelihood of particles.  
            Input type: 
            (1) t : scalar
            (2) y : array of shape [batch_size]
            (3) x : array of shape [batch_size,n_particle,state_dim]
            Output type:
            (1) array of shape [batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = 0.05 * (np.sum(x, axis=2) ** 2)
        mean_expanded = np.expand_dims(mean, axis=2)
        y_tiled = np.tile(np.expand_dims(y,axis=1), (1,n_particle)) 
        y_expanded = np.expand_dims(y_tiled, axis=2)
        return compute_normal_log_pdf(y_expanded, mean_expanded, self.var_w[t])

    def _log_init_prob(self, x): 
        """ Input type: 
            (1) x : array[state_dim] 
            Output type: scalar. """
        batch_size, _, n_particle, state_dim = self.get_dim()
        mean = np.zeros(shape=[state_dim])
        return compute_normal_log_pdf(x, mean, 5.)

    def _log_init_prob_particles(self, x): 
        """ Input type: 
            (1) x : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.get_dim()
        mean = np.zeros(shape=[batch_size,n_particle,state_dim])
        return compute_normal_log_pdf(x, mean, 5.)

    def _log_trans_prob(self, t, x_prev, x_curr): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[state_dim]
            (3) x_curr : array[state_dim]
            Output type: scalar. """
        mean = self.compute_next_mean(t, x_prev)
        return compute_normal_log_pdf(x_curr, mean, self.var_v)

    def _log_trans_prob_particles(self, t, x_prev, x_curr): 
        """ Input type: 
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            (3) x_curr : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        mean = self.compute_next_mean(t, x_prev)
        return compute_normal_log_pdf(x_curr, mean, self.var_v)

    def _sample_init(self): 
        """ Output type:
            (1) x : array[batch_size,state_dim]
            (2) mean : array[batch_size,state_dim]
            (3) log_var : array[batch_size]. """
        batch_size, _, _, state_dim = self.get_dim()
        mean = np.zeros(shape=[batch_size,state_dim])
        log_var = np.zeros(shape=[batch_size,state_dim]) + np.log(5.)
        return np.random.normal(mean, np.sqrt(5.)), mean, log_var        

    def _sample_init_particles(self): 
        """ Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) mean : array[batch_size,n_particle,state_dim]
            (3) log_var : array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.get_dim()
        mean = np.zeros(shape=[batch_size,n_particle,state_dim])
        log_var = np.zeros(shape=[batch_size,n_particle]) + np.log(5.)
        return np.random.normal(mean, np.sqrt(5.)), mean, log_var        

    def _sample_next(self, t, x_prev): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,state_dim]
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) mean : array[batch_size,state_dim]
            (3) log_var : array[batch_size]. """
        batch_size, _, _, _ = self.get_dim()
        mean = self.compute_next_mean(t, x_prev)
        log_var = np.zeros(shape=[batch_size]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var

    def _sample_next_particles(self, t, x_prev): 
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) mean : array[batch_size,n_particle,state_dim]
            (3) log_var : array[batch_size,n_particle]. """
        batch_size, _, n_particle, _ = self.get_dim()
        mean = self.compute_next_mean(t, x_prev)
        log_var = np.zeros(shape=[batch_size,n_particle]) + np.log(self.var_v)
        return np.random.normal(mean, np.sqrt(self.var_v)), mean, log_var

    def _sample_obs(self, t, x):
        """ Input type:
            (1) x : array[a1,...,an,state_dim]
            Output type: array[a1,...,an,1]. """
        mean = 0.05 * (np.sum(x, axis=-1) ** 2)
        obs = np.random.normal(mean, np.sqrt(self.var_w[t]))
        return np.expand_dims(obs, axis=-1)

    def get_dim(self):
        return (self.batch_size, self.n_step, self.n_particle, self.state_dim)

    def plot(self, fixed_xs_ys, oracle, epoch_cur, iter_cur, dir_name):
        return

class ExperPrior(ExperNLSSM):
    def __init__(self):
        super(ExperPrior, self).__init__()
        return


import numpy as np

class Oracle(object):
    def __init__(self, sess, exper):
        self._batch_size = exper.batch_size
        self._n_particle = exper.n_particle
        self._n_step = exper.n_step
        self._state_dim = exper.state_dim
        self._exper = exper
        return

    def build_graph(self):
        return

    def feed_data(self, input_data):
        return 

    def sample_next(self, t, x_prev):
        """ Sample from a proposal distribution.  
            Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,state_dim]
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size].
            (5) mean : array[batch_size,state_dim]
            (6) log_var : array[batch_size]. 
            5-6 are necesary for compatibility."""
        x, x_log_pdf, mean, log_var = self._exper.sample_prior_next(t, x_prev)
        return x, x_log_pdf, mean, log_var, mean, log_var

    def sample_next_particles(self, t, x_prev):
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]. """
        x, x_log_pdf, _, _ = self._exper.sample_prior_next_particles(t, x_prev)
        return x, x_log_pdf
        
    def sample_init(self):
        """ Sample for the 0-th step using a learnt proposal distribution.
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size].
            (5) mean : array[batch_size,state_dim]
            (6) log_var : array[batch_size]. 
            5-6 are necesary for compatibility."""
        x, x_log_pdf, mean, log_var = self._exper.sample_prior_init()
        return x, x_log_pdf, mean, log_var, mean, log_var

    def sample_init_particles(self):
        """ Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]. """
        x, x_log_pdf, _, _ = self._exper.sample_prior_init_particles()
        return x, x_log_pdf

    def compute_h(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,state_dim]
            (2) t : scalar
            Output type: array[batch_size]. """
        return np.zeros(shape=[self._batch_size])

    def compute_h_particles(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) t : scalar
            Output type: array[batch_size,n_particle]. """
        return np.zeros(shape=[self._batch_size, self._n_particle])

    def compute_h_batch(self, x, t, b):
        """ Compute h_t(x) for arbitrary x and input from batch b. 
            Input type:
            (1) x : array[a,state_dim]
            (2) t : scalar
            (3) b : scalar
            Output type: array[a]. """
        size = x.shape[0]
        return np.zeros(shape=[size])

    def compute_h_diff_next(self, x_prev, x, t):
        """ Compute h_t(x) - h_(t-1)(x_prev). 
            Input type:
            (1) x_prev : array[batch_size,state_dim]
            (2) x : array[batch_size,state_dim]
            (3) t : scalar
            Output type: array[batch_size]. """
        return np.zeros(shape=[self._batch_size])

    def compute_h_diff_next_particles(self, x_prev, x, t):
        """ Compute h_t(x) - h_(t-1)(x_prev). 
            Input type:
            (1) x_prev : array[batch_size,n_particle,state_dim]
            (2) x : array[batch_size,n_particle,state_dim]
            (3) t : scalar
            Output type: array[batch_size,n_particle]. """
        return np.zeros(shape=[self._batch_size, self._n_particle])

    def compute_h_diff_init(self, x):
        """ Compute h_0(x). 
            Input type:
            (1) x : array[batch_size,state_dim]
            Output type: array[batch_size]. """
        return np.zeros(shape=[self._batch_size])

    def compute_h_diff_init_particles(self, x):
        """ Compute h_0(x). 
            Input type:
            (1) x : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        return np.zeros(shape=[self._batch_size, self._n_particle])

    def train(self, iter_num, rollout, weight, input_data, ancestry):
        return 

    def estimate_objective(self, weight): 
        batch_size, _, n_particle = weight.shape
        weight_entropy = np.exp(weight)*weight
        weight_entropy_mean = np.sum(weight_entropy,axis=2) / n_particle # array[batch_size,n_step]
        weight_entropy_mean_t = np.sum(weight_entropy_mean, axis=0) / batch_size # array[n_step]
        weight_entropy_mean_t += np.log(n_particle)/n_particle
        return weight_entropy_mean_t

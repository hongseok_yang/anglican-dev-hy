import numpy as np
from scipy.misc import logsumexp 
import tensorflow as tf

from util import np_compute_neg_log_normal_pdf
from util import tf_compute_neg_log_normal_pdf
from util import tf_stack_along_col

""" Utility functions. """

def data_type(): 
    return tf.float32

class Oracle(object):

    def __init__(self, sess, exper):
        self._sess = sess # tensorflow session
        self._lstm_size = exper.lstm_size
        self._learning_rate = exper.q_learning_rate
        self._optimiser = exper.optimiser
        self._nonlinearity = exper.nonlinearity
        self._max_grad_norm = exper.max_grad_norm
        self._dnn_sizes = exper.dnn_sizes
        self._batch_size = exper.batch_size
        self._n_iter = exper.n_iter
        self._n_particle = exper.n_particle
        self._n_step = exper.n_step
        self._state_dim = exper.state_dim
        self._compute_next_mean = exper.compute_next_mean
        self._init_mean = exper.init_mean
        self._init_log_var = exper.init_log_var
        self._var_v = exper.var_v
        self._init_q_std = exper.init_q_std
        self._init_bootstrap = exper.init_bootstrap
        self._init_dnnq_output_scale=exper.init_dnnq_output_scale
        self._use_layernorm = exper.use_layernorm
        return

    def __get_dim(self):
        return (self._batch_size, self._n_step, self._n_particle, self._state_dim)

    def __build_lstm(self):
        shape = [None, self._n_step, 1] # shape of tensor that stores observations
        input = tf.placeholder(shape=shape, dtype=data_type()) # minibatch of observations
        if self._use_layernorm:
            lstm = tf.contrib.rnn.LayerNormBasicLSTMCell(num_units=self._lstm_size)
        else:
            lstm = tf.contrib.rnn.LSTMCell(num_units=self._lstm_size)
        output, _ = tf.nn.dynamic_rnn(lstm, input, dtype=data_type())

        self._input = input
        self._output = output
        return

    def __build_dnn(self, input_list, num_outputs, base_name, reuse, bias_init, weight_init_scale):
        """ Construct fully connected DNN.
        Inputs:
        (1) input_list: a list of inputs to DNN which are concatenated along axis=1. Each element is an array[batch_size,.]
        (2) num_outputs: dimensionality of output of DNN. scalar.
        (3) base_name: name for the DNN layers. string.
        (4) reuse: whether to reuse an already created DNN. boolean
        (5) bias_init: numpy array of biases for output layer. array[num_outputs]
        (6) weight_init_scale: scale of weights of last layer, sampled from Unif[-L,L] where L=sqrt(3*scale)/fan_in. scalar. If None, use Xavier init.
        Outputs: output of DNN. array[batch_size,num_outputs]
        """
        dnn_hidden = tf.concat(input_list, axis=1) 
        for d in xrange(len(self._dnn_sizes)): 
            scope_name = base_name + 'Layer' + str(d+1) 
            dnn_hidden = tf.contrib.layers.fully_connected(
                            inputs=dnn_hidden, 
                            num_outputs=self._dnn_sizes[d], 
                            activation_fn=self._nonlinearity[d],
                            reuse=reuse, 
                            scope=scope_name) 
        scope_name = base_name + 'FinalLayer'
        bias_initializer = tf.constant_initializer(bias_init)
        if weight_init_scale is None:
            weights_initializer = tf.contrib.layers.xavier_initializer()
        else:
            #fan_in = self._dnn_sizes[-1]
            #lim = np.sqrt(3 * weight_init_scale / fan_in)
            #weight_init = weight_init_scale * np.random.uniform(-lim, lim, size=[num_outputs,fan_in])
            #weights_initializer = tf.constant_initializer(weight_init)
            weights_initializer = tf.variance_scaling_initializer(
                                     scale=weight_init_scale, 
                                     distribution='uniform')
        dnn_output = tf.contrib.layers.fully_connected(
                        inputs=dnn_hidden, 
                        num_outputs=num_outputs, 
                        activation_fn=None, 
                        biases_initializer=bias_initializer,
                        weights_initializer=weights_initializer,
                        reuse=reuse, 
                        scope=scope_name)
        return dnn_output

    def __build_dnn_q_with_inputs(self, input_x_fx, input_lstm, reuse):
        """
        Inputs:
        (1) input_x_fx: x_(t-1),f(x_(t-1)) (latter is mean of state transition from x_(t-1) to x_t). array[batch_size,2*state_dim]
        (2) reuse: whether to reuse learned variables at test time. Boolean.
        Outputs:
        (1) sample: sample from gaussian q(x_t|x_(t-1)). array[batch_size,state_dim]
        (2) log_pdf: log pdf of sample. array[batch_size]
        (3) mean: mean of q(x_t|x_(t-1)). array[batch_size,state_dim]
        (4) log_var: log_var of q(x_t|x_(t-1)). array[batch_size]
        """
        state_dim = self._state_dim
        num_outputs = state_dim + 1
        scope_name = 'DNNQ'
        init_log_q_var = 2*np.log(self._init_q_std)
        bias_init = np.array([0.]*state_dim + [init_log_q_var])
        weight_init_scale = self._init_dnnq_output_scale
        output = self.__build_dnn([input_x_fx, input_lstm], num_outputs, scope_name, reuse, bias_init, weight_init_scale)
        # output = self.__build_dnn([input1], num_outputs, scope_name)
        mean = output[:,0:state_dim]
        log_var = output[:,state_dim]
        std = tf.sqrt(tf.exp(log_var))
        std = tf_stack_along_col(std, state_dim)
        dist = tf.contrib.distributions.MultivariateNormalDiag(mean, std)
        sample = dist.sample()
        log_pdf = dist.log_prob(sample)
        return [sample, log_pdf, mean, log_var]

    def __build_dnn_q(self):
        lstm_size = self._lstm_size 
        state_dim = self._state_dim
        input1 = tf.placeholder(shape=[None,2*state_dim], dtype=data_type())
        input2 = tf.placeholder(shape=[None,lstm_size], dtype=data_type())
        self._dnn_q_input1 = input1
        self._dnn_q_input2 = input2
        self._dnn_q_output = self.__build_dnn_q_with_inputs(input1, input2, None)
        return 

    def __build_loss(self):
        batch_size = self._batch_size
        lstm_size = self._lstm_size
        state_dim = self._state_dim
        n_step = self._n_step
        n_particle = self._n_particle
        n_summands_long = batch_size * n_step * n_particle
        n_summands_init = batch_size 

        """ The input_smc0 placeholder stores information about a particular 
            smc run over a minibatch. For all 0 <= t < n_step and 0 <= i < n_particle,
            input_smc0[b,t,i,:] is an array containing the following four elements:
            (1) x^i_t -- state of particle i at step t. [:,:,:,0:state_dim]
            (2) x^(a_(t-1)^i)_(t-1) -- state of ancestor of x^i_t. [:,:,:,state_dim:2*state_dim]
            (3) log (w^i_t) -- log weight of the i-th particle at step t. [:,:,:,2*state_dim]
            (4) f(x^(a_(t-1)^i)_(t-1)) -- mean of transition p(.|x^(a_(t-1)^i)_(t-1)). [:,:,:,(2*state_dim+1):(3*state_dim+1)] """

        n_elem = 3 * state_dim + 1
        input_smc0 = tf.placeholder(shape=[batch_size,n_step,n_particle,n_elem], dtype=data_type())
        input_smc1 = tf.reshape(input_smc0, [n_summands_long,n_elem])

        output_lstm0 = self._output
        output_lstm1 = tf.expand_dims(output_lstm0, axis=2)
        output_lstm2 = tf.tile(output_lstm1, [1,1,n_particle,1])
        output_lstm3 = tf.reshape(output_lstm2, [n_summands_long,lstm_size])

        x_long = input_smc1[:,0:state_dim]
        log_w_long = input_smc1[:,2*state_dim]

        fx_prev = input_smc1[:,(2*state_dim+1):(3*state_dim+1)]
        bootstrap_mean = fx_prev # mean of p(.|x^t_(t-1)) t=0, ..., T-1. [n_summands_long,state_dim]
        bootstrap_log_var_short_unreshaped = tf.log(self._var_v) * tf.ones(shape=[batch_size,n_step-1,n_particle])
        bootstrap_log_var_init_unreshaped = self._init_log_var() * tf.ones(shape=[batch_size,1,n_particle])
        bootstrap_log_var_unreshaped = tf.concat([bootstrap_log_var_init_unreshaped,bootstrap_log_var_short_unreshaped], axis = 1)
        bootstrap_log_var = tf.reshape(bootstrap_log_var_unreshaped, shape = [n_summands_long]) # log_var of p(.|x^t_(t-1)) t=0,...,T-1. [n_summands_long]

        input_dnn_q = tf.concat([input_smc1[:,state_dim:(2*state_dim)],input_smc1[:,(2*state_dim+1):(3*state_dim+1)]], axis = 1)
        _, _, q_mean, q_log_var = self.__build_dnn_q_with_inputs(input_dnn_q, output_lstm3, True)
        if self._init_bootstrap:
            q_mean += bootstrap_mean
            q_log_var += bootstrap_log_var - 2 * np.log(self._init_q_std)

        log_q_term = tf.exp(log_w_long) * tf_compute_neg_log_normal_pdf(state_dim, x_long, q_mean, q_log_var)

        self._loss_input = input_smc0
        self._loss_output = tf.reduce_sum(log_q_term)
        return

    def __build_optimizer(self):
        minimize = tf.contrib.layers.optimize_loss(
                    loss = self._loss_output,
                    global_step = tf.contrib.framework.get_global_step(),
                    learning_rate = self._learning_rate,
                    optimizer = self._optimiser,
                    clip_gradients = self._max_grad_norm)
        self._minimize = minimize
        return

    def build_graph(self):
        self.__build_lstm()
        self.__build_dnn_q()
        self.__build_loss()
        self.__build_optimizer()
        return

    def feed_data(self, input_data):
        """ Run the lstm on a given input data, and store the output of the lstm in the
            internal state of this object. input_data is a numpy array of shape 
            [batch_size,n_step,1], and the output is stored in the _lstm_output_val field
            of this object. """
        input = self._input
        output = self._output
        output_data = self._sess.run(output, feed_dict={input: input_data})
        self._input_data = input_data
        self._lstm_output_val = output_data
        return 

    def __combine_x_and_fx(self, x, t):
        """ Extend a numpy array x of shape [batch_size,state_dim] with fx = mean of p(.|x).
            The result is an array of size [batch_size,2*state_dim]."""
        if t > 0:
            fx = self._compute_next_mean(t, x)
        else:
            fx = self._init_mean(x)
        return np.concatenate([x, fx], axis=1)

    def __combine_x_and_fx_particles(self, x, t):
        """ Input type:
            (1) x : array[batch_size,n_particle,state_dim] 
            (2) t : scalar
            Output type: array[batch_size,n_particle,2*state_dim]. """
        if t > 0:
            fx = self._compute_next_mean(t,x)
        else:
            fx = self._init_mean(x)
        return np.concatenate([x, fx], axis=2)

    def __sample(self, t, x_prev_and_fx):
        """ Input type:
            (1) t : scalar
            (2) x_prev_and_fx : array[batch_size,2*state_dim]
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size]. 
            (5) mean : array[batch_size,state_dim]
            (6) log_var : array[batch_size]. 
            5-6 are necesary for compatibility."""
        batch_size = self._batch_size
        state_dim = self._state_dim
        
        # get bootstrap mean and bootstrap log_var
        bootstrap_mean =  x_prev_and_fx[:,state_dim:(2*state_dim)] # mean of p(.|x_(t-1))
        if t == 0:
            bootstrap_log_var = self._init_log_var() + np.zeros(shape=[batch_size], dtype=np.float32)
        else:
            bootstrap_log_var = np.log(self._var_v) + np.zeros(shape=[batch_size], dtype=np.float32) 
        # log_var of p(.|x_(t-1)). [batch_size]        

        # Run the dnn for q to get parameters of the proposal distribution.
        node_input1 = self._dnn_q_input1
        node_input2 = self._dnn_q_input2
        node_output = self._dnn_q_output
        lstm_output = self._lstm_output_val[:,t,:]
        feed_dict = {node_input1: x_prev_and_fx, node_input2: lstm_output}
        _, _, q_mean, q_log_var = self._sess.run(node_output, feed_dict=feed_dict)
        if self._init_bootstrap:
            q_mean += bootstrap_mean
            q_log_var += bootstrap_log_var - 2 * np.log(self._init_q_std)

        # Generate samples from the proposal distribution.
        # the following is valid only for spherical variance
        sample = np.random.normal(q_mean, np.expand_dims(np.exp(0.5*q_log_var),-1), size=q_mean.shape)
        log_pdf = - np_compute_neg_log_normal_pdf(state_dim, sample, q_mean, q_log_var)

        return sample, log_pdf, q_mean, q_log_var, q_mean, q_log_var

    def __sample_particles(self, t, x_prev_and_fx):
        """ Input type:
            (1) t : scalar
            (2) x_prev_and_fx : array[batch_size,n_particle,2*state_dim] 
            Output type: 
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.__get_dim()
        lstm_size = self._lstm_size 
        num_entries = batch_size * n_particle

        # get bootstrap mean and bootstrap log_var
        bootstrap_mean =  x_prev_and_fx[:,:,state_dim:(2*state_dim)] # mean of p(.|x_(t-1))
        bootstrap_mean =  np.reshape(bootstrap_mean, (num_entries,state_dim))
        if t == 0:
            bootstrap_log_var = self._init_log_var() + np.zeros(shape=[num_entries], dtype=np.float32)
        else:
            bootstrap_log_var = np.log(self._var_v) + np.zeros(shape=[num_entries], dtype=np.float32) 
        # log_var of p(.|x_(t-1)). [num_entries]   

        lstm_output = self._lstm_output_val[:,t,:]
        lstm_output_expanded = np.expand_dims(lstm_output, axis=1)
        lstm_output_tiled = np.tile(lstm_output_expanded, (1,n_particle,1))

        lstm_output_reshaped = np.reshape(lstm_output_tiled, (num_entries,lstm_size))
        x_prev_and_fx_reshaped = np.reshape(x_prev_and_fx, (num_entries,2*state_dim))

        node_input1 = self._dnn_q_input1
        node_input2 = self._dnn_q_input2
        node_output = self._dnn_q_output
        feed_dict = {node_input1: x_prev_and_fx_reshaped, node_input2: lstm_output_reshaped}
        _, _, q_mean, q_log_var = self._sess.run(node_output, feed_dict=feed_dict)
        if self._init_bootstrap:
            q_mean += bootstrap_mean
            q_log_var += bootstrap_log_var - 2 * np.log(self._init_q_std)

        # Generate samples from the proposal distribution.
        # the following is valid only for spherical variance
        sample = np.random.normal(q_mean, np.expand_dims(np.exp(0.5*q_log_var),-1), size=q_mean.shape)
        log_pdf = - np_compute_neg_log_normal_pdf(state_dim, sample, q_mean, q_log_var)

        # reshape back to original dimensions
        sample = np.reshape(sample,(batch_size, n_particle, state_dim))
        log_pdf = np.reshape(log_pdf,(batch_size, n_particle))

        return sample, log_pdf

    def sample_next(self, t, x_prev):
        """ Sample from a learnt proposal distribution. 
            Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,state_dim]
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size]. 
            (5) mean : array[batch_size,state_dim]
            (6) log_var : array[batch_size]. 
            5-6 are necesary for compatibility."""
        x_prev_and_fx_prev = self.__combine_x_and_fx(x_prev, t)
        return self.__sample(t, x_prev_and_fx_prev)
    
    def sample_next_particles(self, t, x_prev):
        """ Input type:
            (1) t : scalar
            (2) x_prev : array[batch_size,n_particle,state_dim]
            Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]. """
        x_prev_and_fx_prev = self.__combine_x_and_fx_particles(x_prev, t)
        return self.__sample_particles(t, x_prev_and_fx_prev)
    
    def sample_init(self):
        """ Sample for the 0-th step using a learnt proposal distribution.
            Output type:
            (1) x : array[batch_size,state_dim]
            (2) x_log_pdf : array[batch_size]
            (3) mean : array[batch_size,state_dim]
            (4) log_var : array[batch_size]. 
            (5) mean : array[batch_size,state_dim]
            (6) log_var : array[batch_size]. 
            5-6 are necesary for compatibility."""
        x_prev = np.zeros(shape=[self._batch_size, self._state_dim])
        x_prev_and_fx_prev = self.__combine_x_and_fx(x_prev, 0)
        return self.__sample(0, x_prev_and_fx_prev)

    def sample_init_particles(self):
        """ Output type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) x_log_pdf : array[batch_size,n_particle]. """
        x_prev = np.zeros(shape=[self._batch_size, self._n_particle, self._state_dim])
        x_prev_and_fx_prev = self.__combine_x_and_fx_particles(x_prev, 0)
        return self.__sample_particles(0, x_prev_and_fx_prev)

    def compute_h(self, x, t):
        """ Compute h_t(x). x is a numpy array of shape [?,state_dim]
            and stores the states in the current steps. t is the current step. 
            The result is a numpy array of shape [?]. """
        h = np.zeros(shape = [x.shape[0]])
        return h

    def compute_h_particles(self, x, t):
        """ Compute h_t(x). 
            Input type:
            (1) x : array[batch_size,n_particle,state_dim]
            (2) t : scalar
            Output type: array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.__get_dim()
        h = np.zeros(shape = [batch_size,n_particle])
        return h

    def compute_h_batch(self, x, t, b):
        """ Compute h_t(x) for arbitrary x and input from batch b. 
            x is a numpy array of shape [?,state_dim]
            and stores the states for which we wish to evaluate h_t(.).
            y is a numpy array of shape [n_step] used to generate output of lstm.
            t is the current step. The result is a numpy array of shape [?]. """
        h = np.zeros(shape = [x.shape[0]])
        return h

    def compute_h_diff_next(self, x_prev, x, t):
        """ Compute h_t(x) - h_(t-1)(x_prev). x_prev and x are numpy arrays 
            of shape [batch_size,state_dim], and stores the states in the previous 
            and the current steps. t is the current step. The 
            result is a numpy array of shape [batch_size]. """
        h = np.zeros(shape = [x.shape[0]])
        return h

    def compute_h_diff_next_particles(self, x_prev, x, t):
        """ Compute h_t(x) - h_(t-1)(x_prev). 
            Input type:
            (1) x_prev : array[batch_size,n_particle,state_dim]
            (2) x : array[batch_size,n_particle,state_dim]
            (3) t : scalar
            Output type: array[batch_size,n_particle]. """
        batch_size, _, n_particle, state_dim = self.__get_dim()
        return np.zeros(shape = [batch_size,n_particle])

    def compute_h_diff_init(self, x):
        x_prev = np.zeros(shape=[self._batch_size, self._state_dim])
        return self.compute_h_diff_next(x_prev, x, 0)

    def compute_h_diff_init_particles(self, x):
        """ Compute h_0(x). 
            Input type:
            (1) x : array[batch_size,n_particle,state_dim]
            Output type: array[batch_size,n_particle]. """
        x_prev = np.zeros(shape=[self._batch_size, self._n_particle, self._state_dim])
        return self.compute_h_diff_next_particles(x_prev, x, 0)

    def __generate_input_smc(self, rollout, weight, ancestry):
        batch_size, n_step, n_particle, state_dim = rollout.shape
        input_data = self._input_data
        shape = [batch_size, n_step, n_particle, 3*state_dim+1]

        input_smc = np.zeros(shape=shape)
        input_smc[:,:,:,0:state_dim] = rollout[:,:,:,:]
        for b in xrange(batch_size):
            for t in xrange(0,n_step):
                for i in xrange(n_particle): 
                    if (t == 0):
                        ancestor = np.zeros(shape=[state_dim])
                        input_smc[b,0,i,(2*state_dim+1):(3*state_dim+1)] = self._compute_next_mean(t,ancestor)
                    else:
                        j = ancestry[b,t-1,i]
                        ancestor = rollout[b,t-1,j,:]
                        input_smc[b,t,i,state_dim:(2*state_dim)] = ancestor
                        input_smc[b,t,i,(2*state_dim+1):(3*state_dim+1)] = self._compute_next_mean(t,ancestor)
        input_smc[:,:,:,2*state_dim] = weight[:,:,:,0]

        return input_smc

    def train(self, iter_num, rollout, weight, input_data, ancestry):
        weight_expanded = np.expand_dims(weight, axis=3)
        input_smc = self.__generate_input_smc(rollout,weight_expanded,ancestry)

        loss_input = self._loss_input
        input = self._input
        feed_dict = {input: input_data, loss_input: input_smc}

        minimize = self._minimize 
        self._sess.run(minimize, feed_dict=feed_dict) 
        loss_val = self._sess.run(self._loss_output, feed_dict=feed_dict) 
        #print "(Iter, ProxyLoss): (%d, %.3f)" % (iter_num, loss_val)
        return

    def estimate_objective(self, weight):
        batch_size, _, n_particle = weight.shape
        weight_entropy = np.exp(weight)*weight
        weight_entropy_mean = np.sum(weight_entropy,axis=2) / n_particle # array[batch_size,n_step]
        weight_entropy_mean_t = np.sum(weight_entropy_mean, axis=0) / batch_size # array[n_step]
        weight_entropy_mean_t += np.log(n_particle)/n_particle
        return weight_entropy_mean_t


(ns anglican.ml-dists
  "Maximum-likelihood estimators for distributions"
  (:refer-clojure :exclude [rand rand-int rand-nth]) 
  (:use [anglican.runtime :refer :all]
        [anglican.gradients :only [digamma]]))

(defprotocol DistML
  (optimise-ml [dist log-weighted-values] 
    "optimises the parameters of a distribution using maximum likelihood 
    with respected to given log-weighted values"))

(extend-protocol DistML 
  anglican.runtime.normal-distribution 
  (optimise-ml [_ log-weighted-values]
    (let [weights (map #(exp (first %)) log-weighted-values) 
          sum-of-weights (sum weights)
          compute-weighted-average (fn [xs] 
            (/ (sum (map (partial *) weights xs)) sum-of-weights))
          values (map second log-weighted-values) 
          mean (compute-weighted-average values) 
          variance-values (map #(pow (- % mean) 2) values)
          variance (compute-weighted-average variance-values)
          sd (sqrt variance)
          dist (normal mean sd)]
      {:mean mean :sd sd :dist dist})))

(extend-protocol DistML
  anglican.runtime.bernoulli-distribution
  (optimise-ml [_ log-weighted-values]
    (let [weights(map #(exp (first %)) log-weighted-values)
          values (map second log-weighted-values) 
          corrected-values (map (partial *) weights values) 
          p (/ (sum corrected-values) (sum weights))
          dist (bernoulli p)]
      {:p p :dist dist})))

(extend-protocol DistML
  anglican.runtime.poisson-distribution
  (optimise-ml [_ log-weighted-values]
    (let [weights(map #(exp (first %)) log-weighted-values)
          values (map second log-weighted-values) 
          corrected-values (map (partial *) weights values) 
          lambda (/ (sum corrected-values) (sum weights))
          dist (poisson lambda)]
      {:lambda lambda :dist dist})))

(extend-protocol DistML
  anglican.runtime.exponential-distribution
  (optimise-ml [_ log-weighted-values]
    (let [weights(map #(exp (first %)) log-weighted-values)
          values (map second log-weighted-values)
          corrected-values (map (partial *) weights values)
          rate (/ (sum weights) (sum corrected-values))
          dist (exponential rate)]
      {:rate rate :dist dist})))


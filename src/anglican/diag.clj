(ns anglican.diag
  "Diagnostics for comparing different inference algorithms"
  (:refer-clojure :exclude [rand rand-int rand-nth])
  (:use [anglican.runtime :refer :all]))

(defn estimate-ml
  "compute estimates of marginal likelihood at each epoch"
  [all-samples]
  (let [f (fn [samples] 
            (let [log-weights (map :log-weight samples) 
                  log-importance-weights (map :log-importance-weight samples)
                  log-summands (map + log-weights log-importance-weights)] ;; gives a list of the log-summands in IS estimate of ml
              (mean (map exp log-summands))))] ;;get average of summands.
    (map f all-samples))) ;; list of ml estimates, one entry per epoch.

(defn ess
  "compute expected sample size (ess) at each epoch"
  [all-samples]
  (let [f (fn [samples]
            (let [log-weights (map :log-weight samples)                  
                  log-importance-weights (map :log-importance-weight samples)
                  log-summands (map + log-weights log-importance-weights)
                  summands (map exp log-summands) ;; list of final weights p(y|xi)p(xi)/q(xi)
                  square-sum-weights (square (sum summands)) ;; sum of final weights
                  sum-squared-weights (sum (map square summands))] ;; sum of squares of final weights 
              (/ square-sum-weights sum-squared-weights)))]
    (map f all-samples))) ;; list of ess, one entry per epoch

(ns anglican.ce
  "Inference algorithm based on the cross-entropy method and importance sampling"
  (:refer-clojure :exclude [rand rand-int rand-nth])
  (:require [anglican.ml-dists :refer :all])
  (:use [anglican.state :exclude [initial-state]]
        [anglican.runtime :only [observe* sample*]]
        anglican.inference
        clojure.pprint))

(derive ::algorithm-ce :anglican.inference/algorithm)
(derive ::algorithm-is :anglican.inference/algorithm)

;; Important datatypes used in the implementation.
;; 1. address: 
;;      [id count distribution-type]
;; 2. dist-obj:
;;      Anglican's distribution object
;; 3. dist-info: 
;;      {:param1 value, 
;;        ..., 
;;       :paramn value, 
;;       :dist dist-obj}
;; 4. proposal-map: 
;;      {address dist-info} 
;; 5. sample or state:
;;      {:log-weight value  (log likelihood weight = log p(y|x), i.e. sum of all log-lkhd from each obs),
;;       :predicts [[label value] ...] (samples drawn from the proposal-map), 
;;       :result value,
;;       :log-importance-weight value (log importance weight = log p(xi) - log q(xi) for current proposal q),
;;       ::log-weight-map {address value} (map of log likelihood weights; have one pair for each observe statement),
;;       ::proposal-map proposal-map (see 4. above for details),
;;       ::sample-values {address [[dist-obj value] ...]} (value is the sample drawn from dist-obj),
;;       ::choice-counts {id count} (have one id for each sample statement, and count is the number of times samples are generated from this program point),
;;       ::choice-last-id id}


;;;; Helper functions and values

(defn median 
  "compute the median of a collection" ;; maps, sets, vectors, lists are all collections
  [coll]
  (assert (and (coll? coll) (> (count coll) 0))
          "coll should be a non-empty collection")
  (let [sorted (sort coll)
        n (- (count sorted) 1)
        suffix (drop (quot n 2) sorted)] ;; lazy sequence of last n/2 items in sorted
    (if (even? n) 
      (first suffix)
      (/ (+ (first suffix) (second suffix)) 2))))

(defn mv-map
  "apply a function to every value of a map data structure"
  [f m]
  (into {} (for [[k v] m] [k (f v)]))) ;; for each (k,v) pair in m, create (k,f(v)) pair and put into map

(def neg-infty (- (/ 1.0 0.0))) ;; use def for functions returning same value each time, defn o/w.

;;;; Importance sampler with given proposal distributions

(def initial-state
  (into anglican.state/initial-state ;; map with keys: log-weight,predicts,result,mem,store
        {:log-importance-weight 0.0
         ::log-weight-map {} 
         ::proposal-map {}
         ::sample-values {} 
         ::choice-counts {}
         ::choice-last-id nil}))

(defn get-address-state-from-cpt
  "get the address (i.e. the tuple of program point, count and distribution type) 
  and the state from a checkpoint (i.e. obs or smp)"
  [cpt]
  (let [[choice-id state] (checkpoint-id cpt (:state cpt) ::choice-counts ::choice-last-id) ;; choice-id is a tuple [id number-of-previous-occurences], ::choice-counts and ::choice-last-id are keys in state
        dist-type (type (:dist cpt)) ;; get type of :dist key in cpt.
        address (conj choice-id dist-type)] ;; address is a vector [program_point count dist_type]
    [address state]))

(defmethod checkpoint [::algorithm-is anglican.trap.observe] [_ obs] ;; multimethod for checkpoint fn inherited from inference.clj. anglican.trap.observe is (type obs). Need in exec fn n inference.clj.
  "update the global log weight and the address-specific log weight based on observation"
  (let [[address state] (get-address-state-from-cpt obs)
        {dist :dist cont :cont value :value} obs ;; extract keys :dist, :cont, :value of obs into dist,cont,value.
        log-weight (observe* dist value) ;; observe returns log pdf of dist at value.
        new-state (-> state
                    (update-in [::log-weight-map address] 
                               #(if (nil? %) log-weight (+ % log-weight))) ;; update the value of key address in (state ::log-weight-map) to be log-weight if empty, or original value + log-weight o/w.
                    (add-log-weight log-weight))] ;; also add on log-weight to (state ::log-weight). note (-> a (f1 b) (f2 c)) equiv to (f2 (f1 a b) c). a.k.a. thread-first. ->> is thread-last.
    #(cont nil new-state))) ;; function that calls cont with arguments nil and new-state.

(defmethod checkpoint [::algorithm-is anglican.trap.sample] [_ smp]
  "sample a value from a proposal or a prior and record necessary information"
  (let [[address state] (get-address-state-from-cpt smp)
        {prior :dist cont :cont} smp
        proposal (get-in state [::proposal-map address :dist] prior) ;; prior is the not-found value. i.e. returns (:dist (address (::proposal-map state))), but if empty returns prior. 
        v (sample* proposal) ;; sample from proposal distrib.
        dist-v [proposal v] 
        delta-log-weight (- (observe* prior v) (observe* proposal v)) ;; log(prior(v)) - log(proposal(v))
        new-state (-> state
                    (update :log-importance-weight 
                            #(+ % delta-log-weight)) ;; increment log-importance weight (note log-importance weight is a some of factors, one for each address) 
                    (update-in [::sample-values address] 
                               #(if (nil? %) [dist-v] (conj % dist-v))))] ;; update sample-values for key address
    #(cont v new-state)))

(defmethod infer ::importance-proposal
  [_ prog value & {:keys [proposal-map] :or {proposal-map {}}}] ;; & followed by further argument :proposal-map to function. Defaults to empty-map if not provided.
  "perform importance sampling with given proposal distributions"
  (let [state (update initial-state ::proposal-map (fn [_] proposal-map))] ;; state = initial-state, but with proposal-map for ::proposal-map
    (letfn [(sample-seq [] ;; binds names to functions instead of expressions, and returns last fn  
              (lazy-seq ;; makes sample-seq into a lazy sequence via recursion
                (cons ;; concatenates an element to the beginning of a sequence
                  (:state (exec ::algorithm-is prog value state)) ;; state of one importance sample
                  (sample-seq))))]
      (sample-seq)))) ;; returns sample-seq, a lazy sequence of states

(defn generate-samples
  "generate posterior samples of a program using the importance-sampling algorithm"
  ([prog proposal-map number-of-samples]
   ;; The sampler uses distributions in proposal-map as proposal distributions.
   ;; If some address is not in the domain of proposal-map, it uses prior as proposal.
   (let [samples (infer ::importance-proposal prog nil :proposal-map proposal-map)]
     (take number-of-samples samples))) ;; list of the first number-of-samples elements from lazy sequence samples
  ([prog proposal-map]
   ;; This part does the same thing as above except that it generates a lazy infinite sequence of samples
   ;; and that it filters out samples with infinite log weights using inference/drop-invalid.
   (let [samples (infer ::importance-proposal prog nil :proposal-map proposal-map)
         valid-samples (drop-invalid samples)]
     valid-samples))) ;; return the generated lazy sequence.

;;;; Private functions and values

(defn anneal-weights
  "anneal of samples using the median-ceiling strategy"
  [samples threshold-map] ;; threshold-map is a map with key=address and value=threshold (scalar value).
  (let [log-weight-maps1 (map ::log-weight-map samples) ;; samples should be a list of states. So this gives a list of log-weight maps. 
        log-weight-maps2 (map (partial mv-map list) log-weight-maps1) 
        log-weight-map (apply merge-with concat log-weight-maps2) ;; log-weight-maps with values (log-weights) of the same key (address) are grouped together into a list 
        update-threshold (fn [cur-threshold-map [address log-weights]] 
                           (let [cur-threshold (get cur-threshold-map address neg-infty) ;; current threshold of sample corresponding to address. set to neg-infty if threshold empty.
                                 new-threshold (max (median log-weights) cur-threshold)] ;; new threshold of sample corresponding to address
                             (assoc cur-threshold-map address new-threshold))) ;; updates cur-threshold value of cur-threshold-map to new-threshold if address already exists, o/w concatenates {address new-threshold} to cur-threshold-map
        new-threshold-map (reduce update-threshold threshold-map log-weight-map) ;; updated threshold-map with the {address log-weight} pairs in log-weight-map
        ;; _ (println new-threshold-map "\n") ;; print threshold map (for debugging purposes)
        update-log-weight (fn [[k v]] [k (min (new-threshold-map k) v)]) ;; fn returning min of threshold at address k and value v.
        update-log-weight-map #(into {} (map update-log-weight %)) ;; fn with input log-weight-map, output log-weight-map with pairs {k v} replaced by {k min((new-threshold-map k) v)}
        update-sample #(update % ::log-weight-map update-log-weight-map) ;; fn with input list of states, output states whose log-weight maps have been updated as above line.
        new-samples (map update-sample samples)] ;; samples, whose states' log-weight maps have been updated as above line.
    [new-samples new-threshold-map])) ;; samples whose log-weights have been updated by min( new-threshold-map, sample:log-weights) where new-threshold-map = max(median(sample:log-weights),threshold-map)

(defn get-weight-dist-value-map
  "generate a map from addresses to vectors of weight, distribution object and value"
  [sample]
  (let [dist-value-map (::sample-values sample) ;; {address [[dist-obj value] ...]}
        log-weight (+ (:log-weight sample) (:log-importance-weight sample))] ;; log-weight = (:log-weight sample) + (:log-importance-weight sample)
    (mv-map #(map (partial into [log-weight]) %) dist-value-map))) ;; {address [[log-weight dist-obj value] ... ]} 

(defn optimise-parameters 
  "optimise parameters of proposal distributions based on samples"
  [samples proposal-map]
  ;; The function computes parameters of proposal distributions by effectively optimising 
  ;; an approximation of KL, which is computed by the importance-sampling estimator 
  ;; with samples. 
  ;; 1. It assumes that all the likelihoods in samples are annealed already,
  ;; so that it does not have to do anything special.
  ;; 2. If some proposal distribution in proposal-map is not used by any of samples, then
  ;; it is kept in the output. This is somewhat an arbitrary decision which may or may not
  ;; be correct in some sense. We should double-check this.
  ;; 3. The optimiser invokes the optimise-ml method implemented for each distribution.
  ;; This finds ml parameters of a given distribution with respect to a given collection of 
  ;; log-weighted samples.
  (let [weight-dist-value-map (apply merge-with concat (map get-weight-dist-value-map samples)) ;;[log-weight dist-obj value] vector of same address are grouped together into a list.
        optimise (fn [cur-proposal-map [addr weight-dist-values]] 
                   (let [[_ dist _] (first weight-dist-values) 
                         weight-values (map (fn [[w d v]] [w v]) weight-dist-values) ;; list ([w v] ... )
                         dist-info (optimise-ml dist weight-values)] ;; returns map of optimal parameters of dist in max step of ce. e.g. {:mean mean :sd sd :dist dist}
                     (assoc cur-proposal-map addr dist-info)))] ;; update proposal-map with new dist-info.
    (reduce optimise proposal-map weight-dist-value-map))) ;; update proposal-map for each address in weight-dist-value-map.

(defn update-proposal-map
  [samples proposal-map threshold-map]
  (let [[new-samples1 new-threshold-map] (anneal-weights samples threshold-map)
        compute-weight (fn [samp] 
                         (reduce + (vals (::log-weight-map samp)))) ;; sum all log-weights of log-weight-map of given sample.
        update-weight (fn [samp]
                        (assoc samp :log-weight (compute-weight samp))) ;; update log-weight of sample to be the sum of all log-weights of log-weight-map.
        new-samples2 (map update-weight new-samples1) ;; update log-weight of new-samples1 as above.
        new-proposal-map (optimise-parameters new-samples2 proposal-map)] ;; update proposal-map for each address in new-samples2
    [new-proposal-map new-threshold-map]))

(defn no-big-progress?
  "check whether an iteration in the cross-entropy method made enough progress"
  [old-threshold-map new-threshold-map]
  ;; (keys new-threshold-map) should include (keys old-threshold-map)
  (let [f (fn [k] 
            (<= (get new-threshold-map k neg-infty)
                (+ (get old-threshold-map k neg-infty) 1.0)))] ;; returns boolean. true if for address k, (new-threshold-map k) <= (old-threshold-map k) + 1.0
    (every? f (keys new-threshold-map)))) ;; boolean that returns true if there is no big progress for all addresses of new-threshold map

(defn compute-proposal-map
  "compute proposal distributions for the program using the cross-entropy method" 
  [prog number-of-samples max-epoch] 
  ;; number-of-samples is the number of samples used in each iteration of the method, and max-epoch is
  ;; the upper bound on the number of iterations.
  (loop [epoch 1 proposal-map {} threshold-map {::default neg-infty} all-samples []] ;; bindings at beginning of loop. all-samples is a vector whose entries are samples at each epoch.
    (if (< max-epoch epoch)
      [proposal-map all-samples] ;; if max-epoch < epoch, then return map of proposal-map and all-samples, else return the following:
      (let [samples (generate-samples prog proposal-map number-of-samples) ;; list of samples (states) from proposal-map.
            new-all-samples (conj all-samples samples) ;; concatenate samples to all-samples
            [new-proposal-map new-threshold-map] (update-proposal-map samples proposal-map threshold-map)] ;; update proposal-map using median thresholding then optimising CE.
        ;;(if (no-big-progress? threshold-map new-threshold-map)
        ;; (let [_ (println "No big progress after" epoch "epochs \n")]
        ;;  {:proposal-map new-proposal-map :all-samples new-all-samples}) ;; if no big progress return new-proposal-map and new-all-samples.
          (recur (inc epoch) new-proposal-map new-threshold-map new-all-samples))))) ;; else increase epoch by 1 and recur loop.


;;;; Public functions and values

(defmethod infer :ce [_ prog value
                        & {:keys [number-of-samples
                                  max-epoch]
                           :or {number-of-samples 100
                                max-epoch 10}}] ;; default values
  ;; This function generates samples by first finding good proposal distributions using 
  ;; a cross-entropy method and then generating samples via importance sampling with the 
  ;; found proposal distributions.
  (assert (>= number-of-samples 2)
          ":number-of-samples must be at least 2")
  (assert (>= max-epoch 1)
          ":max-epoch must be at least 1")
  (let [[proposal-map all-samples] (compute-proposal-map prog number-of-samples max-epoch) ;; temp = map of importance distribution obtained from ce method and vector of samples at each epoch.
        samples (generate-samples prog proposal-map)] ;; generate samples from this importance distribution.
    {:samples samples :all-samples all-samples})) ;; return samples and all-samples.

